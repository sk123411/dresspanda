package com.maestros.dresspanda.dashboard

import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import com.facebook.login.Login
import com.maestros.dresspanda.R
import com.maestros.dresspanda.databinding.FragmentMyAccountBinding
import com.maestros.dresspanda.databinding.SelectLanguageLayoutBinding
import com.maestros.dresspanda.home.HomeActivity
import com.maestros.dresspanda.home.profile.ProfileViewModel
import com.maestros.dresspanda.login.LoginViewModel
import com.maestros.dresspanda.util.Constant
import com.squareup.picasso.Picasso
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch


@AndroidEntryPoint
class MyAccountFragment : Fragment() {

    lateinit var binding:FragmentMyAccountBinding

    private val profileViewModel:ProfileViewModel by viewModels()
    private val loginViewModel:LoginViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = com.maestros.dresspanda.databinding.FragmentMyAccountBinding.inflate(inflater)
        loginViewModel.getUserDetails()




        binding.notificationLayout.setOnClickListener {
            Navigation.findNavController(it).navigate(R.id.notificationFragment)
        }

        binding.changePasswordLayout.setOnClickListener {
            Navigation.findNavController(it).navigate(R.id.changePasswordFragment)
        }

        binding.profileButton.setOnClickListener {
            Navigation.findNavController(it).navigate(R.id.profileFragment)


        }
        binding.ordersButton.setOnClickListener {
            Navigation.findNavController(it).navigate(R.id.ordersFragment)


        }

        binding.addressButton.setOnClickListener {
            Navigation.findNavController(it).navigate(R.id.addressList)
        }


        lifecycleScope.launch {

            loginViewModel.login.collect({user ->


                when(user) {


                    is LoginViewModel.LoginEvent.UserDetails -> {

                        profileViewModel.getProfile(user.userDetails.id)
                    }

                }

            })








        }

        lifecycleScope.launch {

            profileViewModel.register.collect({

                binding.profileName.setText(it.name)
                binding.number.setText(it.phone_number)
                Picasso.get().load(Constant.BASE_URL+it.image).into(binding.profileImage)

            })
        }


        binding.messageButton.setOnClickListener {

            Navigation.findNavController(it).navigate(R.id.chatFragment)
        }


        binding.changeLanguageBtn.setOnClickListener {

            val dialog = Dialog(requireActivity())
            val binding = SelectLanguageLayoutBinding.inflate(layoutInflater)
            dialog.setContentView(binding.root)
            dialog.show()



            binding.changeBtn.setOnClickListener {

                dialog.dismiss()
            }

        }






        return binding.root
    }


    override fun onResume() {
        super.onResume()

        HomeActivity.bottomNavigationView.menu.findItem(R.id.bottom_profile).setChecked(true)

    }



}