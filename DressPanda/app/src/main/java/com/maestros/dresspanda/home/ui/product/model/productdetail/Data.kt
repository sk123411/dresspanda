package com.maestros.dresspanda.home.ui.product.model.productdetail

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName("MRP")
    @Expose

    val MRP: String,
    @SerializedName("brand")
    @Expose

    val brand: String,
    @SerializedName("category_id")
    @Expose

    val category_id: String,
    @SerializedName("color")
    @Expose

    val color: String,
    @SerializedName("id")
    @Expose

    val id: String,
    @SerializedName("image")
    @Expose

    val image: String,
    @SerializedName("path")
    @Expose

    val path: String,
    @SerializedName("product_description")
    @Expose

    val product_description: String,
    @SerializedName("product_title")
    @Expose

    val product_title: String,
    @SerializedName("seller_id")
    @Expose

    val seller_id: String,
    @SerializedName("selling_price")
    @Expose

    val selling_price: String,
    @SerializedName("shop")
    @Expose

    val shop: Shop,
    @SerializedName("size")
    @Expose

    val size: String,
    @SerializedName("status")
    @Expose

    val status: String,
    @SerializedName("stock")
    @Expose

    val stock: String,
    @SerializedName("sub_category_id")
    @Expose

    val sub_category_id: String
)