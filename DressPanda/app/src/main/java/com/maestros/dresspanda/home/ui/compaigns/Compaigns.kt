package com.maestros.dresspanda.home.ui.compaigns

data class Compaigns(
    val compaignTitle:String?,
    val compaignDescription:String?,
    val compaignBackgroundColor:String?,
    val list: List<CompaignItem>
)
data class CompaignItem(var image:Int?, var title:String?)
