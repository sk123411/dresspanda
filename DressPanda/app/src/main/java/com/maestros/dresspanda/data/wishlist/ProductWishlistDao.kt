package com.maestros.dresspanda.data.wishlist

import androidx.room.*
import com.maestros.dresspanda.home.ui.wishlist.model.WishlistProduct

@Dao
interface ProductWishlistDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertProduct(product: WishlistProduct):Long

    @Delete
    suspend fun deleteProduct(product: WishlistProduct)


    @Query("SELECT * FROM wishlist")
    suspend fun getAllProducts():List<WishlistProduct>

    @Query("SELECT * FROM wishlist where id=:id")
    suspend fun getItemById(id:String):List<WishlistProduct>

    @Update
    suspend fun updateProduct(product: WishlistProduct)


    @Query("DELETE FROM wishlist")
    suspend fun deleteAll()


    suspend fun insertOrUpdate(product: WishlistProduct):Long{
        val item = getItemById(product.id)
        var successEvent:Long=0

        if(item.isEmpty()){
          successEvent = insertProduct(product)

        }else {
            updateProduct(product)
        }
        return successEvent
    }

    suspend fun isProductOnDB(product: WishlistProduct):Boolean {
        val item = getItemById(product.id)

        if (!item.isEmpty()) {
            return true
        }
        return false
    }




}