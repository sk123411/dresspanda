package com.maestros.dresspanda.login.model

data class SocialUser(var aouth_id:String?= "",
                      var aouth_provider:String?= "",
                      var firstName:String? = ""
                      , var lastName:String?="",
                      var email:String?="",
                      var profilePicture:String?="")