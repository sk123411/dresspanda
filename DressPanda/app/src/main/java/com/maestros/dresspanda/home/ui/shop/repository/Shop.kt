package com.maestros.dresspanda.home.ui.shop.repository

import com.maestros.dresspanda.home.ui.brand.BaseResponse
import com.maestros.dresspanda.home.ui.brand.HomeResponse
import com.maestros.dresspanda.home.ui.category.adapter.model.CategoryResponse
import com.maestros.dresspanda.home.ui.product.model.productdetail.ProductResponse
import com.maestros.dresspanda.seller.repository.model.SellerResponse
import com.maestros.dresspanda.util.Resource

interface Shop {

    suspend fun getShops(action:String?):Resource<List<SellerResponse>>
    suspend fun getProductsByShopID(action:String?, data:HashMap<String,String>):Resource<BaseResponse<ProductResponse>>
    suspend fun getCategoriesByShopID(action:String?, data:HashMap<String,String>):Resource<BaseResponse<CategoryResponse>>
    suspend fun getShopsByBrandID(action:String?,map:HashMap<String,String>): Resource<BaseResponse<SellerResponse>>


}