
package com.maestros.dresspanda.home.ui.wishlist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.maestros.dresspanda.data.wishlist.ProductWishlistDao
import com.maestros.dresspanda.home.ui.wishlist.model.WishlistProduct
import kotlinx.coroutines.launch

class WishlistViewModel @androidx.hilt.lifecycle.ViewModelInject constructor(
    val productWishlistDao: ProductWishlistDao
):ViewModel() {

    val productWishList_ = MutableLiveData<MutableList<WishlistProduct>>()

    val productWishList:LiveData<MutableList<WishlistProduct>>
        get() = productWishList_



    private val _text = MutableLiveData<String>().apply {
        value = "This is home Fragment"
    }
    val text: LiveData<String> = _text



    fun getWishlistProducts(){

        viewModelScope.launch {

            val myList: MutableList<WishlistProduct> = mutableListOf<WishlistProduct>()
            myList.addAll(productWishlistDao.getAllProducts())
            productWishList_.value = myList

        }
    }









}