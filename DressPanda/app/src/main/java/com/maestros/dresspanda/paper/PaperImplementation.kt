package com.maestros.dresspanda.paper

import android.content.Context
import javax.inject.Inject


class PaperImplementation constructor(
    val context:Context
):Paper{

    override fun initialize() {

        io.paperdb.Paper.init(context)
    }

}
