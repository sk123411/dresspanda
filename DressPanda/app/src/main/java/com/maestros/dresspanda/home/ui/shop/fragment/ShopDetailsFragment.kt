package com.maestros.dresspanda.home.ui.shop.fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.maestros.dresspanda.R
import com.maestros.dresspanda.databinding.FragmentShopDetailsBinding
import com.maestros.dresspanda.home.ui.category.adapter.model.CategoryResponse
import com.maestros.dresspanda.home.ui.home.HomeAdapter
import com.maestros.dresspanda.home.ui.product.adapter.ProductAdapter
import com.maestros.dresspanda.home.ui.shop.ShopViewModel
import com.maestros.dresspanda.home.ui.wishlist.model.WishlistShop
import com.maestros.dresspanda.login.LoginActivity
import com.maestros.dresspanda.seller.repository.model.SellerResponse
import com.maestros.dresspanda.util.Constant
import dagger.hilt.android.AndroidEntryPoint
import io.paperdb.Paper
import kotlinx.coroutines.launch

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ShopDetailsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
class ShopDetailsFragment : Fragment() {

    lateinit var binding: FragmentShopDetailsBinding

    val shopViewModel: ShopViewModel by viewModels()
     var wishlistShop: WishlistShop=   WishlistShop(
         id = "",
         city_id = "",
         category = "",
         pid = 1,
         path = "",
         pincode = "",
         password = "",
         brand ="",
         address = "",
         email = "",
         name ="",
         store_name = "",
         shop_title = "",
         Payment_method = "",
         store_image = "",
         aprove_status = "",
         mobile2 = "",
         result = "result",
         role = ""
     )
    lateinit var sellerResponse: SellerResponse
     var AddedwishlistShop: WishlistShop=  WishlistShop(
         id = "",
         city_id = "",
         category = "",
         pid = 1,
         path = "",
         pincode = "",
         password = "",
         brand ="",
         address = "",
         email = "",
         name ="",
         store_name = "",
         shop_title = "",
         Payment_method = "",
         store_image = "",
         aprove_status = "",
         mobile2 = "",
         result = "result",
         role = ""
     )

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentShopDetailsBinding.inflate(layoutInflater)

        Paper.init(activity)

        val isFromFollowed = arguments?.getBoolean(Constant.FROM_FOLLOWED_SHOPS)

        Log.d("ISFROMFOLLOWED", "::"+isFromFollowed)

        if (isFromFollowed!!) {

            wishlistShop = Paper.book().read<WishlistShop>(Constant.SHOP_WISHLIST_DETAILS)
            Log.d("ISFROMFOLLOWED", "::"+wishlistShop.toString())

            shopViewModel.isProductOnDB(wishlistShop)
            binding.shopTitle.text = wishlistShop.store_name
            shopViewModel.getCategoriesByShopId(wishlistShop.id)
            shopViewModel.getProductsByShopId(wishlistShop.id)

            setUpMenuPanelData(wishlistShop)

        } else {

            sellerResponse = Paper.book().read<SellerResponse>(Constant.SHOP_DETAILS)

            AddedwishlistShop =
                WishlistShop(
                    id = sellerResponse.id,
                    city_id = sellerResponse.city_id,
                    category = sellerResponse.category,
                    pid = sellerResponse.pid,
                    path = sellerResponse.path,
                    pincode = sellerResponse.pincode,
                    password = sellerResponse.password,
                    brand = sellerResponse.brand,
                    address = sellerResponse.address,
                    email = sellerResponse.email,
                    name = sellerResponse.name,
                    store_name = sellerResponse.store_name,
                    shop_title = sellerResponse.shop_title,
                    Payment_method = sellerResponse.Payment_method,
                    store_image = sellerResponse.store_image,
                    aprove_status = sellerResponse.aprove_status,
                    mobile2 = sellerResponse.mobile2,
                    result = "result",
                    role = sellerResponse.role
                )
            binding.shopTitle.text = sellerResponse.store_name

            shopViewModel.isProductOnDB(AddedwishlistShop)

            shopViewModel.getCategoriesByShopId(sellerResponse.id)
            shopViewModel.getProductsByShopId(sellerResponse.id)

            setUpMenuPanelData(AddedwishlistShop)
        }


        binding.sendMessageButtton.setOnClickListener {
            Paper.init(activity)


            if (isFromFollowed){



                if (!Constant.isUserLogged(requireActivity())!!){

                    startActivity(Intent(view?.context, LoginActivity::class.java).putExtra(Constant.FROM,"shop_detail"))


                }else {

                    Paper.book().write("SENDER_ID", wishlistShop.id)
                    Navigation.findNavController(it).navigate(R.id.sendChatMessageFragment)

                }




            }else {


                if (!Constant.isUserLogged(requireActivity())!!){

                    startActivity(Intent(view?.context, LoginActivity::class.java).putExtra(Constant.FROM,"shop_detail"))


                }else {

                    Paper.book().write("SENDER_ID", AddedwishlistShop.id)
                    Navigation.findNavController(it).navigate(R.id.sendChatMessageFragment)


                }

            }

        }


        lifecycleScope.launch {

            shopViewModel.shops.observe(viewLifecycleOwner, Observer {

                when (it) {

                    is ShopViewModel.ShopEvent.SuccessProducts -> {
                        activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.GONE
                        binding.productsList.apply {

                            layoutManager = GridLayoutManager(context, 2)
                            adapter = ProductAdapter(it.resultText)
                        }

                    }

                    is ShopViewModel.ShopEvent.SuccessCategories -> {
                        activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.GONE
                        binding.categoriesList.apply {

                            layoutManager =
                                LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                            adapter = HomeAdapter<CategoryResponse>(
                                it.resultText,
                                R.layout.lyt_category_item
                            )

                        }

                    }

                    is ShopViewModel.ShopEvent.ProductOnDb -> {

                        when (it.boolean) {
                            true -> {
                                binding.followButton.setText(resources.getString(R.string.unfollow))

                                binding.followButton.setOnClickListener {


                                    if (!Constant.isUserLogged(requireActivity())!!){

                                        startActivity(Intent(view?.context, LoginActivity::class.java).putExtra(Constant.FROM,"shop_detail"))


                                    }else {

                                        shopViewModel.unfollowShop(wishlistShop)
                                        binding.followButton.setText(resources.getString(R.string.follow))


                                    }


                                }



                            }
                            false -> {
                                binding.followButton.setText(resources.getString(R.string.follow))
                                binding.followButton.setOnClickListener {
                                    if (!Constant.isUserLogged(requireActivity())!!){

                                        startActivity(Intent(view?.context, LoginActivity::class.java).putExtra(Constant.FROM,"shop_detail"))


                                    }else {

                                        shopViewModel.unfollowShop(wishlistShop)
                                        binding.followButton.setText(resources.getString(R.string.follow))


                                    }
                                }



                            }
                        }
                    }

                    is ShopViewModel.ShopEvent.Failure -> {
                        Toast.makeText(context, "Server error occured", Toast.LENGTH_SHORT).show()
                    }


                    is ShopViewModel.ShopEvent.Loading -> {

                        activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.VISIBLE
                    }

                    else -> {

                    }


                }


            })





        }




        return binding.root
    }

    private fun setUpMenuPanelData(shop: Any?) {

        if(shop is WishlistShop){

            binding.locationRoot.setOnClickListener {
                showMessage(shop.address)
            }

            binding.callRoot.setOnClickListener {
                showMessage(shop.mobile2)
            }

            binding.termsLayout.setOnClickListener {
                showMessage("Terms will be available later")
            }

            binding.ReviewsRoot.setOnClickListener {
                showMessage("Reviews section will be added soon")
            }
        }else if (shop is SellerResponse) {

            binding.locationRoot.setOnClickListener {
                showMessage(shop.address)
            }

            binding.callRoot.setOnClickListener {

                startCall(shop.mobile2)
            }

            binding.termsLayout.setOnClickListener {
                showMessage("Terms will be available later")
            }

            binding.ReviewsRoot.setOnClickListener {
                showMessage("Reviews section will be added soon")
            }

        }

    }

    private fun startCall(number: String) {

        if (TextUtils.isDigitsOnly(number)){
            val intent = Intent(Intent.ACTION_CALL)
            intent.data = Uri.parse("tel:" + number)
            startActivity(intent)
        }else {
            showMessage("Number not available")

        }


    }

    private fun showMessage(s: String) {

        Toast.makeText(context,s,Toast.LENGTH_LONG).show()
    }


}