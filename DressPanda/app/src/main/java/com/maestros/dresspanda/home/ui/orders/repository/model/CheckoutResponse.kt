package com.maestros.dresspanda.home.ui.orders.repository.model


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.maestros.dresspanda.home.ui.product.model.productdetail.ProductResponse

data class CheckoutResponse(
    @SerializedName("address_id")
    @Expose
    val addressId: String,
    @SerializedName("checkout_date")
    @Expose
    val checkoutDate: String,
    @SerializedName("delivery_fee")
    @Expose
    val deliveryFee: String,
    @SerializedName("id")
    @Expose
    val id: String,
    @SerializedName("order_id")
    @Expose
    val orderId: String,
    @SerializedName("payment_type")
    @Expose
    val paymentType: String,
    @SerializedName("product_id")
    @Expose
    val productId: String,
    @SerializedName("product_price")
    @Expose
    val productPrice: String,
    @SerializedName("quantity")
    @Expose
    val quantity: String,
    @SerializedName("result")
    @Expose
    val result: String,
    @SerializedName("seller_id")
    @Expose
    val sellerId: String,
    @SerializedName("status")
    @Expose
    val status: String,
    @SerializedName("strtotime")
    @Expose
    val strtotime: String,
    @SerializedName("times")
    @Expose
    val times: String,
    @SerializedName("times_date")
    @Expose
    val timesDate: String,
    @SerializedName("total_amount")
    @Expose
    val totalAmount: String,
    @SerializedName("user_id")
    @Expose
    val userId: String,
    @SerializedName("product_detail")
    @Expose
    val products: List<ProductResponse>
)