package com.maestros.dresspanda.signup.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class Register(
    @SerializedName("email")
    @Expose
    val email: String?,
    @SerializedName("id")
    @Expose

    val id: String?,
    @SerializedName("name")
    @Expose

    val name: String?,
    @SerializedName("password")
    @Expose

    val password: String?,
    @SerializedName("path")
    @Expose
    val path: String?,
    @SerializedName("result")
    @Expose
    val result: String?,
    @SerializedName("aouth_id")
    @Expose
    val aouth_id: String?,
    @SerializedName("aouth_provider")
    @Expose

    val aouth_provider:String?,
    @SerializedName("DOB")
    @Expose
    val DOB:String?,
    @SerializedName("image")
    @Expose

    val image:String?,
    @SerializedName("gender")
    @Expose
    val gender:String?,
    @SerializedName("phone_number")
    @Expose
    val phone_number:String?,
    @SerializedName("regid")
    @Expose
    val regid:String?
)


