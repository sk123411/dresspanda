package com.maestros.dresspanda.home.ui.home.repository

import android.util.Log
import com.maestros.dresspanda.home.ui.brand.HomeResponse
import com.maestros.dresspanda.data.home.HomeBrandsDao
import com.maestros.dresspanda.home.ui.brand.BaseResponse
import com.maestros.dresspanda.home.ui.home.model.Slider
import com.maestros.dresspanda.home.ui.notification.Notification
import com.maestros.dresspanda.home.ui.product.model.productdetail.ProductResponse
import com.maestros.dresspanda.util.Resource
import retrofit2.Response
import javax.inject.Inject


class HomeImplementation @Inject constructor(val api: HomeAPI, val homeBrandsDao: HomeBrandsDao):
    Home {
    override suspend fun getBrands(action: String?): Resource<List<HomeResponse>> {


        return try {
            val response = api.getBrands(action)
            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())
                Resource.Success(result.list)
            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }


    }

    override suspend fun getProducts(action: String?): Resource<List<ProductResponse>> {


        return try {
            val response = api.getProducts(action)
            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result.list)

            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }


    }

    override suspend fun getSliderDataHome(action: String?): Resource<List<Slider>> {
//
//        val sliderData = HashMap<String,String>()
//        sliderData.put("Hannibal", "https://static2.hypable.com/wp-content/uploads/2013/12/hannibal-season-2-release-date.jpg");
//        sliderData.put("Big Bang Theory", "https://tvfiles.alphacoders.com/100/hdclearart-10.png");
//        sliderData.put("House of Cards", "https://cdn3.nflximg.net/images/3093/2043093.jpg");
//        sliderData.put("Game of Thrones", "https://images.boomsbeat.com/data/images/full/19640/game-of-thrones-season-4-jpg.jpg");
//
//        val sliderList = sliderData.map {
//            data -> Slider(data.key, data.value)
//        }.toList()
        return try {
            val response = api.getSliderDataHome(action)
            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result.list)

            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }

    }



}