package com.maestros.dresspanda.home.ui.product.model.productdetail

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.maestros.dresspanda.home.ui.product.model.productdetail.Data

data class ProductDetailResponse(
    @SerializedName("data")
    @Expose
    val `data`: Data,
    @SerializedName("status")
    @Expose
    val status: Boolean
)