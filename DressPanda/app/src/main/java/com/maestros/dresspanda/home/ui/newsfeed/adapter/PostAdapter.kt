package com.maestros.dresspanda.home.ui.newsfeed.adapter

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.maestros.dresspanda.R
import com.maestros.dresspanda.databinding.PostDetailLytBinding
import com.maestros.dresspanda.databinding.PostItemBinding
import com.maestros.dresspanda.home.ui.newsfeed.NewsFeedFragment
import com.maestros.dresspanda.home.ui.newsfeed.NewsViewModel
import com.maestros.dresspanda.home.ui.newsfeed.adapter.model.postlist.Post
import com.maestros.dresspanda.util.Constant
import com.maestros.dresspanda.util.Resource
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import javax.inject.Inject

class PostAdapter(
    val list: List<Post>,
    val newsFeedFragment: NewsFeedFragment,
    val newsViewModel: NewsViewModel
) : RecyclerView.Adapter<PostAdapter.ViewHolder>() {


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding = PostItemBinding.bind(view)


        fun bindData(post: Post, newsFeedFragment: NewsFeedFragment, newsViewModel: NewsViewModel) {
            binding.postUserName.text = post.userId
            binding.postTilte.text = post.title

            Picasso.get().load(Constant.BASE_URL + post.image)
                .into(binding.postImage)

            binding.likeCountText.text = post.likeCount.toString()
            binding.commentCountText.text = post.commentCount.toString()

            if (post.user_like_status == 1) {

                binding.postLikeButton.setImageResource(R.drawable.heart_full)

            } else {
                binding.postLikeButton.setImageResource(R.drawable.heart)

            }


            //Set image with picasso


            binding.postCommentButton.setOnClickListener {
                newsViewModel.showCommentOnPost(post.id)
                openPostDetail(post, newsFeedFragment)
            }

            binding.postLikeButton.setOnClickListener {

                newsFeedFragment.newsViewModel.addLikeOnPost(
                    Constant.getUserID(it.context),
                    post.id
                )
                binding.postLikeButton.setImageResource(R.drawable.wish_p)
                binding.likeCountText.text = (post.likeCount + 1).toString()

            }


            binding.postShareButton.setOnClickListener {

                Constant.shareImageFromURI(Constant.BASE_URL + post.image, post.title, it.context)


            }


        }


        private fun openPostDetail(post: Post, newsFeedFragment: NewsFeedFragment) {


            val bottomSheetDialog = BottomSheetDialog(newsFeedFragment.requireActivity())
            val binding =
                PostDetailLytBinding.inflate(LayoutInflater.from(newsFeedFragment.requireContext()))
            bottomSheetDialog.setContentView(binding.root)
            bottomSheetDialog.show()

            binding.postTilte.text = post.title
            binding.likeCountText.text = post.likeCount.toString()
            binding.commentCountText.text = post.commentCount.toString()

            Picasso.get().load(Constant.BASE_URL + post.image)
                .into(binding.postImage)


            if (post.user_like_status == 1) {

                binding.postLikeButton.setImageResource(R.drawable.heart_full)

            } else {
                binding.postLikeButton.setImageResource(R.drawable.heart)
            }


            newsFeedFragment.newsViewModel.newsEvent.observe(
                newsFeedFragment.viewLifecycleOwner,
                Observer {

                    when (it) {

                        is NewsViewModel.NewsEvent.SuccessGetPostComments -> {

                            binding.commentList.apply {

                                layoutManager = LinearLayoutManager(context)
                                adapter = PostCommentAdapter(it.resultText)
                            }

                        }

                        is NewsViewModel.NewsEvent.Failure -> {

                            Toast.makeText(
                                newsFeedFragment.requireActivity(),
                                it.errorText,
                                Toast.LENGTH_SHORT
                            ).show()

                        }

                        is NewsViewModel.NewsEvent.SuccessPostLike -> {

                            Toast.makeText(
                                newsFeedFragment.requireActivity(),
                                "Comment sent",
                                Toast.LENGTH_SHORT
                            ).show()
                            newsFeedFragment.newsViewModel.showCommentOnPost(postId = post.id)

                        }
                    }
                })



            binding.sendMessageButtton.setOnClickListener {


                if (!binding.messageEdit.text.toString().equals("")) {

                    newsFeedFragment.newsViewModel.addCommentOnPost(
                        userId = Constant.getUserID(it.context),
                        postId = post.id,
                        comment = binding.messageEdit.text.toString()
                    )

                    binding.messageEdit.setText("")
                }


            }


            binding.postLikeButton.setOnClickListener {

                newsFeedFragment.newsViewModel.addLikeOnPost(
                    Constant.getUserID(it.context),
                    post.id
                )
                binding.postLikeButton.setImageResource(R.drawable.wish_p)
                binding.likeCountText.text = (post.likeCount + 1).toString()

            }

            bottomSheetDialog.setOnDismissListener {

                newsFeedFragment.newsViewModel.getAllPost(Constant.getUserID(itemView.context))


            }


            binding.postShareButton.setOnClickListener {


                Constant.shareImageFromURI(Constant.BASE_URL + post.image, post.title, it.context)

            }


        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.post_item,
            parent, false
        )

        return ViewHolder(view)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.bindData(list.get(position), newsFeedFragment, newsViewModel)


    }


}