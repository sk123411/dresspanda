package com.maestros.dresspanda.home.ui.newsfeed.adapter.model.postcomment


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.maestros.dresspanda.home.ui.newsfeed.adapter.model.postcomment.Data

data class PostComment(
    @SerializedName("aprove_status")
    @Expose
    val aproveStatus: String,
    @SerializedName("data")
    @Expose
    val `data`: List<Data>,
    @SerializedName("id")
    @Expose
    val id: String,
    @SerializedName("image")
    @Expose
    val image: String,
    @SerializedName("status")
    @Expose
    val status: String,
    @SerializedName("title")
    @Expose
    val title: String,
    @SerializedName("user_id")
    @Expose
    val userId: String


)