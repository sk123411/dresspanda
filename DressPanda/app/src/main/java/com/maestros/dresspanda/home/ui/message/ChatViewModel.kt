package com.maestros.dresspanda.home.ui.message

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.maestros.dresspanda.home.ui.message.repository.Chat
import com.maestros.dresspanda.home.ui.shop.ShopViewModel
import com.maestros.dresspanda.util.Constant
import kotlinx.coroutines.launch
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import javax.annotation.Resource

class ChatViewModel @ViewModelInject constructor(
    val chat: Chat
): ViewModel() {

    val chatEvent_ = MutableLiveData<ChatEvent>()

    val chatEvent:LiveData<ChatEvent>
        get() = chatEvent_

    private val imageFile_ = MutableLiveData<File>()



    val imageFile: LiveData<File>
        get() = imageFile_



    sealed class ChatEvent{

        class GetAllChats(val messages:List<MessagesList>):ChatEvent()
        class Failure( val message:String?):ChatEvent()
        class SuccessMessageSent(val message:Message):ChatEvent()
        object Loading : ChatEvent()

    }



    fun getAllMessages(userId:String?){
        chatEvent_.value = ChatEvent.Loading

        val map = HashMap<String,String>()
        map.put("user_id", userId!!)


        viewModelScope.launch {

            val response = chat.showAllMessages("show_all_userchat",map)

            when(response){

                is com.maestros.dresspanda.util.Resource.Success -> {

                    if (response.data!!.status!!){

                        Log.d("MESSSSSSSSSS", "::"+response.data.list!!)
                        chatEvent_.value = ChatEvent.GetAllChats(response.data.list!!)

                    }else {
                        chatEvent_.value = ChatEvent.Failure("Server errror")

                    }

                }
                is com.maestros.dresspanda.util.Resource.Error -> {
                    chatEvent_.value = ChatEvent.Failure("Server errror")
                }
            }

        }
    }


    fun sendMessage(userId: String?, senderId:String?,receiverId:String?,message:String?){
        chatEvent_.value = ChatEvent.Loading

        val map = HashMap<String,String>()
        map.put("user_id", userId!!)
        map.put("sender_id", senderId!!)
        map.put("reciver_id", receiverId!!)
        map.put("messages", message!!)


        viewModelScope.launch {
            val response = chat.sendMessage("message_insert",
               map)


            when(response){

                is com.maestros.dresspanda.util.Resource.Success -> {

                    chatEvent_.value = ChatEvent.SuccessMessageSent(response.data!!)

                }
                is com.maestros.dresspanda.util.Resource.Error -> {

                }
            }

        }

    }


    fun sendMessageImage(


        userId: String?, senderId:String?,receiverId:String?,message:String?, imageFile:File?){

        chatEvent_.value = ChatEvent.Loading

        val image: RequestBody =  RequestBody.create(
            "image/jpg".toMediaTypeOrNull(),
            imageFile!!)

        val storeImageBody: MultipartBody.Part =
            MultipartBody.Part.createFormData(
                Constant.STORE_IMAGE,
                imageFile.name, image)


        val nameBody = getMultiPartFormRequestBody(userId)
        val senderIDBody = getMultiPartFormRequestBody(senderId)
        val receiverIdBody = getMultiPartFormRequestBody(receiverId)
        val messageBody = getMultiPartFormRequestBody(message)


        viewModelScope.launch {
            val response = chat.sendMessageWithImage("message_insert",
                storeImageBody,nameBody,messageBody,senderIDBody,receiverIdBody)


            when(response){

                is com.maestros.dresspanda.util.Resource.Success -> {
                    chatEvent_.value = ChatEvent.SuccessMessageSent(response.data!!)

                }
                is com.maestros.dresspanda.util.Resource.Error -> {

                }
            }

        }







    }




    fun getAllChats(senderId: String?, receiverId: String?){

        chatEvent_.value = ChatEvent.Loading

        val map = HashMap<String,String>()
        map.put("sender_id", senderId!!)
        map.put("reciver_id", receiverId!!)



        viewModelScope.launch {
            val response = chat.showMessage("show_message",
                map)


            when(response){

                is com.maestros.dresspanda.util.Resource.Success -> {

                    if(response.data!!.status!!){
                        chatEvent_.value = ChatEvent.GetAllChats(response.data!!.list)

                    }else {
                        chatEvent_.value = ChatEvent.Failure("Server error occured")

                    }

                }
                is com.maestros.dresspanda.util.Resource.Error -> {
                    chatEvent_.value = ChatEvent.Failure("Server error occured")

                }
            }

        }










    }


    fun getMultiPartFormRequestBody(tag:String?):RequestBody{
        return RequestBody.create(MultipartBody.FORM, tag!!)

    }

    fun sendImageFile(file: File) {

        imageFile_.value = file

    }







}