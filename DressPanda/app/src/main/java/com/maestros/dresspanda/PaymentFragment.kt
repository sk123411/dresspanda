package com.maestros.dresspanda

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import com.google.android.material.snackbar.Snackbar
import com.maestros.dresspanda.databinding.FragmentPaymentBinding
import com.maestros.dresspanda.home.HomeActivity
import com.maestros.dresspanda.home.ui.cart.repository.CartX
import com.maestros.dresspanda.home.ui.orders.OrderViewModel
import com.maestros.dresspanda.util.Constant
import dagger.hilt.android.AndroidEntryPoint
import io.paperdb.Paper
import kotlinx.coroutines.launch

@AndroidEntryPoint
class PaymentFragment :Fragment(){

    lateinit var binding:FragmentPaymentBinding
     var paymentType:String?=""
    lateinit var cartX:CartX

    val orderViewModel:OrderViewModel by viewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {



        binding = FragmentPaymentBinding.inflate(layoutInflater)
        Paper.init(context)
        val map = Paper.book().read<CartX>(Constant.CART_MAP)


        binding.totalItems.setText("Total Items ${map.qntity}")
        binding.totalAmount.setText("Rs ${map.totalAmount}")



        lifecycleScope.launch {

            orderViewModel.ordersEvent.observe(viewLifecycleOwner, Observer {

                when(it){


                    is OrderViewModel.OrdersEvent.AddOrder -> {

                         Constant.showSnackBar(requireView(),"Order successfull","Ok",object:View.OnClickListener{
                            override fun onClick(v: View?) {
                            }

                        }).show()
                        Navigation.findNavController(requireView()).navigate(R.id.ordersFragment)

                        orderViewModel.addNotification(it.resultText.userId,resources.getString(R.string.order_placed))
                        Constant.setCartCount(HomeActivity.bottomNavigationView,0.toString())

                    }

                    is OrderViewModel.OrdersEvent.Failure -> {

                        Constant.showSnackBar(requireView(),"Order failed, please try later","Ok",object:View.OnClickListener{
                            override fun onClick(v: View?) {
                            }

                        }).show()
                    }
                }

            })
        }


        binding.paymentGroup.setOnCheckedChangeListener(object :RadioGroup.OnCheckedChangeListener{
            override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {

                if(checkedId==R.id.radioPaytm){
                    paymentType = "Paytm"

                }else  if(checkedId==R.id.radioRazorpay) {
                    paymentType = "Razorpay"
                }else if(checkedId==R.id.radioUpi){
                    paymentType = "UPI"
                }



            }

        })


        binding.continueButton.setOnClickListener {

            orderViewModel.addOrder(
                userID = Constant.getUserID(it.context),
                paymentType = paymentType,
                addressId = arguments?.getString("id"),
                totalAmount = map.totalAmount.toDouble(),
                deliveryFee = 40


            )





        }



        return binding.root
    }


}