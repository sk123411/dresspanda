package com.maestros.dresspanda.home.ui.newsfeed.adapter.model.postlist


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Post(
    @SerializedName("aprove_status")
    @Expose
    val aproveStatus: String,
    @SerializedName("id")
    @Expose
    val id: String,
    @SerializedName("image")
    @Expose
    val image: String,
    @SerializedName("path")
    @Expose
    val path: String,

    @SerializedName("title")
    @Expose
    val title: String,
    @SerializedName("user_id")
    @Expose
    val userId: String,
    @SerializedName("user_like_status")
    @Expose
    val user_like_status: Int,
    @SerializedName("like_count")
    @Expose
    val likeCount: Int,
    @SerializedName("comment_count")
    @Expose
    val commentCount: Int



)