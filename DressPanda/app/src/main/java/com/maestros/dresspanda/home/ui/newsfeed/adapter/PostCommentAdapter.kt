package com.maestros.dresspanda.home.ui.newsfeed.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.maestros.dresspanda.databinding.CommentItemLytBinding
import com.maestros.dresspanda.home.ui.newsfeed.adapter.model.postcomment.Data

class PostCommentAdapter(val list: List<Data>) :
    RecyclerView.Adapter<PostCommentAdapter.MyViewholder>() {
    class MyViewholder(view: View) : RecyclerView.ViewHolder(view) {

        val commentItemLytBinding = CommentItemLytBinding.bind(view)
        fun bindData(data: Data) {

            commentItemLytBinding.userName.text = data.id
            commentItemLytBinding.userComment.text = data.comment



        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewholder {

        val binding = CommentItemLytBinding.inflate(LayoutInflater.from(parent.context))

        return MyViewholder(binding.root)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewholder, position: Int) {

        holder.bindData(list.get(position))
    }
}