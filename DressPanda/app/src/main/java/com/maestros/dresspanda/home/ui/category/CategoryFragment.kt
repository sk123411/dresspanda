package com.maestros.dresspanda.home.ui.category

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.maestros.dresspanda.R
import com.maestros.dresspanda.databinding.FragmentCategoryBinding
import com.maestros.dresspanda.home.ui.brand.HomeResponse
import com.maestros.dresspanda.home.ui.home.HomeAdapter
import com.maestros.dresspanda.home.ui.category.adapter.model.CategoryResponse
import com.maestros.dresspanda.home.ui.home.HomeViewModel
import dagger.hilt.android.AndroidEntryPoint

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [CategoryFragment.newInstance] factory method to
 * create an instance of this fragment.
 */

@AndroidEntryPoint
class CategoryFragment : Fragment() {

    lateinit var binding:FragmentCategoryBinding
    private val viewModel: CateogoryViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentCategoryBinding.inflate(inflater)

        viewModel.getCategories()


        viewModel.categoryEvent.observe(viewLifecycleOwner, androidx.lifecycle.Observer {

            when (it) {

                is CateogoryViewModel.CategoryEvent.Success -> {
                    activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.GONE

                    binding.categoriesList.apply {
                        layoutManager =
                            LinearLayoutManager(context)
                        adapter =
                            HomeAdapter<CategoryResponse>(
                                it.resultText,
                                R.layout.lyt_category_item
                            )
                    }
                }

                is CateogoryViewModel.CategoryEvent.Failure -> {
                    activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.GONE

                    Toast.makeText(context, "Servor error occured", Toast.LENGTH_SHORT).show()
                }

                is CateogoryViewModel.CategoryEvent.Loading -> {

                    activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.VISIBLE

                }
            }

        })

        return binding.root
    }



}