package com.maestros.dresspanda.home.ui.wishlist.adapter.model

data class Wish(
    val image:String?,
    val title:String?,
    val discountPrice:Double?,
    val originalPrice:Double?
)