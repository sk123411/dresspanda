package com.maestros.dresspanda.home.ui.home.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Slider(
    @SerializedName("title")
    @Expose
    val title:String?,
    @SerializedName("image")
    @Expose
    val image:String?)
