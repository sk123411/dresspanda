package com.maestros.dresspanda.home.ui.wishlist.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(tableName = "wishlist")
data class WishlistProduct(
    @PrimaryKey(autoGenerate = true) val pid: Int,
    @SerializedName("brand")
    @Expose
    val MRP: String,
    @SerializedName("brand")
    @Expose
    val brand: String,
    @SerializedName("category_id")
    @Expose
    val category_id: String,
    @SerializedName("color")
    @Expose
    val color: String,

    @SerializedName("id")
    @Expose
    val id: String,


    @SerializedName("image")
    @Expose
    val image: String,
    @SerializedName("path")
    @Expose
    val path: String,
    @SerializedName("product_description")
    @Expose
    val product_description: String,
    @SerializedName("product_title")
    @Expose
    val product_title: String,
    @SerializedName("seller_id")
    @Expose
    val seller_id: String,
    @SerializedName("selling_price")
    @Expose
    val selling_price: String,
    @SerializedName("size")
    @Expose
    val size: String,
    @SerializedName("stock")
    @Expose
    val stock: String,
    @SerializedName("sub_category_id")
    @Expose
    val sub_category_id: String
)