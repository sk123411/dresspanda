package com.maestros.dresspanda.home.ui.wishlist

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.maestros.dresspanda.R
import com.maestros.dresspanda.databinding.FragmentWishlistBinding
import com.maestros.dresspanda.home.HomeActivity
import com.maestros.dresspanda.home.ui.product.ProductViewModel
import com.maestros.dresspanda.home.ui.wishlist.adapter.WishlistAdapter
import dagger.hilt.android.AndroidEntryPoint

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [WishlistFragment.newInstance] factory method to
 * create an instance of this fragment.
 */

@AndroidEntryPoint
class WishlistFragment : Fragment() {


    lateinit var binding:FragmentWishlistBinding
    private val wishlistViewModel: WishlistViewModel by viewModels()



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentWishlistBinding.inflate(inflater)
        val scope = lifecycleScope


        lifecycleScope.launchWhenStarted {

            wishlistViewModel.getWishlistProducts()

            wishlistViewModel.productWishList.observe(viewLifecycleOwner, Observer {

                binding.wishlist.apply {
                    layoutManager = LinearLayoutManager(context)
                    adapter = WishlistAdapter(it!!,
                        wishlistViewModel,
                        scope)
                }


                if (it.size>0){

                    binding.noWishlisttemText.visibility = View.GONE

                }else {
                    binding.noWishlisttemText.visibility = View.VISIBLE
                }

            })
        }

        return binding.root
    }


    override fun onResume() {
        super.onResume()

        HomeActivity.bottomNavigationView.menu.findItem(R.id.bottom_wishlist).setChecked(true)

    }




}