package com.maestros.dresspanda.home.ui.newsfeed

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.maestros.dresspanda.R
import com.maestros.dresspanda.databinding.AddPostLayoutBinding
import com.maestros.dresspanda.databinding.FragmentNewsFeedBinding
import com.maestros.dresspanda.home.ui.newsfeed.adapter.PostAdapter
import com.maestros.dresspanda.home.ui.newsfeed.repository.News
import com.maestros.dresspanda.home.ui.wishlist.WishlistViewModel
import com.maestros.dresspanda.signup.model.Register
import com.maestros.dresspanda.util.Constant
import dagger.hilt.android.AndroidEntryPoint
import io.paperdb.Paper
import kotlinx.coroutines.launch
import java.io.Console
import java.io.File


@AndroidEntryPoint
class NewsFeedFragment : Fragment() {

    lateinit var binding: FragmentNewsFeedBinding
    lateinit var addPostbinding: AddPostLayoutBinding
    lateinit var postAdapter: PostAdapter
     var userID:String?=""

    val newsViewModel: NewsViewModel by viewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentNewsFeedBinding.inflate(inflater)

        Paper.init(requireActivity())
        val register:Register = Paper.book().read(Constant.USER_DETAILS)
        userID = register.id
        newsViewModel.getAllPost(userID)

        lifecycleScope.launch {

            newsViewModel.newsEvent.observe(viewLifecycleOwner, Observer {

                when(it){

                    is NewsViewModel.NewsEvent.SuccessGetAllPost -> {
                        activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.GONE

                        binding.newsList.apply {

                            layoutManager = LinearLayoutManager(context)
                            postAdapter = PostAdapter(it.resultText,this@NewsFeedFragment,newsViewModel)
                            adapter = postAdapter


                        }



                    }

                    is NewsViewModel.NewsEvent.SuccessGetAllPostUserID -> {

                    }

                    is NewsViewModel.NewsEvent.SuccessAddPost -> {

                        Toast.makeText(activity, "Post added successfully", Toast.LENGTH_SHORT).show()
                    }

                    is NewsViewModel.NewsEvent.Loading -> {

                        activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.GONE

                    }
                }
            })
        }



        binding.addPostButton.setOnClickListener {

            openPostDialog()
        }






        return binding.root
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            val fileUri = data?.data

            addPostbinding.postImage.setImageURI(fileUri)
            //You can get File object from intent
            val file: File = ImagePicker.getFile(data)!!

            //You can also get File Path from intent
            newsViewModel.sendImageFile(file)


            val filePath: String = ImagePicker.getFilePath(data)!!
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(context, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(context, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }



    fun openPostDialog(){

        val bottomSheetDialog = BottomSheetDialog(requireActivity())
        addPostbinding = AddPostLayoutBinding.inflate(layoutInflater)
        bottomSheetDialog.setContentView(addPostbinding.root)
        bottomSheetDialog.show()
        addPostbinding.addImageButton.setOnClickListener {


            ImagePicker.with(this)
                .galleryOnly()
                .compress(1024)
                .start()

        }


        lifecycleScope.launch {

            newsViewModel.imageFile.observe(viewLifecycleOwner, Observer {file->

                addPostbinding.shareButton.setOnClickListener {
                    if (addPostbinding.titleEdit.text.toString().isEmpty()
                        ||addPostbinding.titleEdit.text.toString().equals("")){

                    }else {

                        newsViewModel.insertPost(file!!,userID,addPostbinding.titleEdit.text.toString())
                        bottomSheetDialog.dismiss()
                    }

                }


            })


        }



    }





}