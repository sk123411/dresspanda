package com.maestros.dresspanda.home.ui.orders

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.FtsOptions
import com.maestros.dresspanda.R
import com.maestros.dresspanda.databinding.FragmentOrdersBinding
import com.maestros.dresspanda.util.Constant
import com.maestros.dresspanda.util.TextChangeListernerImple
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch


@AndroidEntryPoint
class OrdersFragment : Fragment() {

    lateinit var binding: FragmentOrdersBinding

    val ordersViewModel: OrderViewModel by viewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentOrdersBinding.inflate(inflater)
        ordersViewModel.showOrders(Constant.getUserID(context))



        binding.searchEdit.addTextChangedListener(TextChangeListernerImple(object :
            TextChangeListernerImple.TextChangeListerner {
            override fun onTextChange(text: String?) {
                if (text!!.isNotEmpty ()) {
                    ordersViewModel.showSearchOrders(Constant.getUserID(context), text)
                }
            }

        }))



        lifecycleScope.launch {


            ordersViewModel.ordersEvent.observe(viewLifecycleOwner, Observer {

                when (it) {
                    is OrderViewModel.OrdersEvent.SuccessGetAllOrders -> {
                        activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.GONE

                        binding.ordersList.apply {

                            layoutManager = LinearLayoutManager(context)
                            setHasFixedSize(true)
                            adapter = OrdersAdapter(it.resultText)

                        }

                        if (it.resultText.size>0){

                            binding.noWishlisttemText.visibility = View.GONE

                        }else {
                            binding.noWishlisttemText.visibility = View.VISIBLE
                        }


                    }

                    is OrderViewModel.OrdersEvent.Failure -> {
                        activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.GONE

                        Toast.makeText(context, "Server error", Toast.LENGTH_SHORT).show()
                    }

                    is OrderViewModel.OrdersEvent.Loading -> {
                        activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.VISIBLE


                    }
                }


            })


        }



        return binding.root

    }


}