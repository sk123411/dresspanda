package com.maestros.dresspanda.home.ui.newsfeed.repository

import com.maestros.dresspanda.home.ui.newsfeed.adapter.model.postlist.BasePost
import com.maestros.dresspanda.home.ui.newsfeed.adapter.model.LikeCommentResponse
import com.maestros.dresspanda.home.ui.newsfeed.adapter.model.postcomment.PostComment
import com.maestros.dresspanda.home.ui.newsfeed.adapter.model.postlist.Post
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*

interface NewsAPI {

    @Multipart
    @POST("dresspanda/api/process.php")
    suspend fun insertPost(
        @Query("action") banner: String?,
        @Part image: MultipartBody.Part?,
        @Part("title") title: RequestBody?,
        @Part("user_id") userID: RequestBody?
    ): Response<Post>




    @POST("dresspanda/api/process.php")
    @FormUrlEncoded
    suspend fun getAllPosts(@Query("action") banner: String?, @FieldMap data:HashMap<String,String>): Response<BasePost<Post>>

    @POST("dresspanda/api/process.php")
    @FormUrlEncoded
    suspend fun likeOnPost(@Query("action") action:String?,@FieldMap data:HashMap<String,String>): Response<LikeCommentResponse>


    @POST("dresspanda/api/process.php")
    @FormUrlEncoded
    suspend fun commentOnPost(@Query("action") action:String?,@FieldMap data:HashMap<String,String>): Response<LikeCommentResponse>



    @POST("dresspanda/api/process.php")
    @FormUrlEncoded
    suspend fun showPostComments(@Query("action") action:String?,@FieldMap data:HashMap<String,String>): Response<PostComment>


}