package com.maestros.dresspanda.home.ui.cart.repository

import com.maestros.dresspanda.home.ui.brand.BaseResponse
import com.maestros.dresspanda.home.ui.product.model.productdetail.ProductResponse
import org.json.JSONObject
import retrofit2.Response
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import retrofit2.http.Query

interface CartAPI {


    @POST("dresspanda/api/process.php")
    @FormUrlEncoded
    suspend fun showCartItems(@Query("action") add_cart:String?, @FieldMap map:HashMap<String,String>): Response<CartX>


    @POST("dresspanda/api/process.php")
    @FormUrlEncoded
    suspend fun updateCartItem(@Query("action") add_cart:String?, @FieldMap map:HashMap<String,String>): Response<CartX>



    @POST("dresspanda/api/process.php")
    @FormUrlEncoded
    suspend fun deleteCartItem(@Query("action") add_cart:String?, @FieldMap map:HashMap<String,String>): Response<JSONObject>



}