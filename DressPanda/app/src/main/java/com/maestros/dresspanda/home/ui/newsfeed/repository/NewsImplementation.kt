package com.maestros.dresspanda.home.ui.newsfeed.repository

import android.util.Log
import com.maestros.dresspanda.home.ui.newsfeed.adapter.model.postlist.BasePost
import com.maestros.dresspanda.home.ui.newsfeed.adapter.model.LikeCommentResponse
import com.maestros.dresspanda.home.ui.newsfeed.adapter.model.postcomment.PostComment
import com.maestros.dresspanda.home.ui.newsfeed.adapter.model.postlist.Post
import com.maestros.dresspanda.util.Resource
import okhttp3.MultipartBody
import okhttp3.RequestBody
import javax.inject.Inject


class NewsImplementation @Inject constructor(val api: NewsAPI):
    News {
    override suspend fun insertPost(
        action: String?,
        image: MultipartBody.Part?,
        title: RequestBody?,
        userID: RequestBody?
    ): Resource<Post> {
        return try {
            val response = api.insertPost(action,image,title,userID)
            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result)

            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }

    override suspend fun getAllPosts(
        action: String?,
        data: HashMap<String, String>
    ): Resource<BasePost<Post>> {
        return try {
            val response = api.getAllPosts(action,data)
            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result)

            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }

    override suspend fun likePost(
        action: String?,
        data: HashMap<String, String>
    ): Resource<LikeCommentResponse> {
        return try {
            val response = api.likeOnPost(action,data)
            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result)

            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }

    override suspend fun commentOnPost(
        action: String?,
        data: HashMap<String, String>
    ): Resource<LikeCommentResponse> {
        return try {
            val response = api.commentOnPost(action,data)
            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result)

            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }

    override suspend fun showPostComments(
        action: String?,
        data: HashMap<String, String>
    ): Resource<PostComment> {
        return try {
            val response = api.showPostComments(action,data)
            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result)

            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }
}



