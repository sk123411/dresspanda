package com.maestros.dresspanda.home.ui.newsfeed.adapter.model.postcomment


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName("comment")
    @Expose
    val comment: String,
    @SerializedName("id")
    @Expose
    val id: String,
    @SerializedName("post_id")
    @Expose
    val postId: String,
    @SerializedName("user_id")
    @Expose
    val userId: String
)