package com.maestros.dresspanda.seller.repository.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(tableName = "sellerresponse")
data class SellerResponse(
    @PrimaryKey(autoGenerate = true)
    val pid: Int,

    @SerializedName("Payment_method")
    @Expose
    val Payment_method: String,
    @SerializedName("address")
    @Expose
    val address: String,
    @SerializedName("aprove_status")
    @Expose
    val aprove_status: String,
    @SerializedName("brand")
    @Expose
    val brand: String,
    @SerializedName("category")
    @Expose
    val category: String,
    @SerializedName("city_id")
    @Expose
    val city_id: String,
    @SerializedName("email")
    @Expose
    val email: String,
    @SerializedName("id")
    @Expose
    val id: String,
    @SerializedName("mobile2")
    @Expose
    val mobile2: String,
    @SerializedName("name")
    @Expose
    val name: String,
    @SerializedName("password")
    @Expose
    val password: String,
    @SerializedName("path")
    @Expose
    val path: String,
    @SerializedName("pincode")
    @Expose
    val pincode: String,
    @SerializedName("result")
    @Expose
    val result: String,
    @SerializedName("role")
    @Expose
    val role: String,
    @SerializedName("shop_title")
    @Expose
    val shop_title: String,
    @SerializedName("store_image")
    @Expose
    val store_image: String,
    @SerializedName("store_name")
    @Expose
    val store_name: String

) {
    override fun toString(): String {
        return "SellerResponse(pid=$pid, Payment_method='$Payment_method', address='$address', aprove_status='$aprove_status', brand='$brand', category='$category', city_id='$city_id', email='$email', id='$id', mobile2='$mobile2', name='$name', password='$password', path='$path', pincode='$pincode', result='$result', role='$role', shop_title='$shop_title', store_image='$store_image', store_name='$store_name')"
    }
}