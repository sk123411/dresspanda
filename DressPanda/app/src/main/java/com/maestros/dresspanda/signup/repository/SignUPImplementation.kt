package com.maestros.dresspanda.signup.repository

import android.util.Log
import com.maestros.dresspanda.signup.model.Register
import com.maestros.dresspanda.util.Resource
import javax.inject.Inject


class SignUPImplementation @Inject constructor(val api: SignUpAPI): SignUP {
    override suspend fun register(action: String?, data: Map<String, String>?): Resource<Register> {
        return try {
            val response = api.register("sign_up", data!!)
            val result = response.body()
            Log.d("XXXXXXXXX", ""+result.toString())

            if(response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", ""+result.toString())

                Resource.Success(result)


            } else {
                Resource.Error(response.message())
            }
        } catch(e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }

    override suspend fun socialLogin(
        action: String?,
        data: Map<String, String>?
    ): Resource<Register> {
        return try {
            val response = api.socialLogin(action, data!!)
            val result = response.body()

            if (response.isSuccessful && result != null) {

                Resource.Success(result)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }




}