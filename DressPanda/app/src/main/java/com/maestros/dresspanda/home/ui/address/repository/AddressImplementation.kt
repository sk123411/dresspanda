package com.maestros.dresspanda.home.ui.address.repository

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import com.maestros.dresspanda.home.ui.address.AddressX
import com.maestros.dresspanda.home.ui.address.repository.allAddressModel.AllAddress
import com.maestros.dresspanda.util.Resource
import org.json.JSONObject
import retrofit2.Response
import javax.inject.Inject


class AddressImplementation @Inject constructor(
    val addressAPI: AddressAPI
):Address{
    override suspend fun showAddress(
        show_address: String?,
        map: HashMap<String, String>
    ): Resource<AllAddress> {
        return try {
            val response = addressAPI.showAddress(show_address,map)
            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())
                Resource.Success(result)

            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }

    override suspend fun updateAddressItem(
        updateAddress: String?,
        map: HashMap<String, String>
    ): Resource<AddressX> {
        return try {
            val response = addressAPI.updateAddressItem(updateAddress,map)
            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())
                Resource.Success(result)

            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }

    override suspend fun deleteAddressItem(
        deleteAddress: String?,
        map: HashMap<String, String>
    ): Resource<JSONObject> {
        return try {
            val response = addressAPI.deleteAddressItem(deleteAddress,map)
            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())
                Resource.Success(result)

            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }

    override suspend fun addAddress(
        addAddress: String?,
        map: HashMap<String, String>
    ): Resource<AddressX> {
        return try {
            val response = addressAPI.addAddress(addAddress,map)
            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())
                Resource.Success(result)

            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }


}