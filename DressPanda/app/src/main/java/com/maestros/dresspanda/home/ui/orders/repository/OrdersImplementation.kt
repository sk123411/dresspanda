package com.maestros.dresspanda.home.ui.orders.repository

import android.util.Log
import com.maestros.dresspanda.home.ui.brand.BaseResponse
import com.maestros.dresspanda.home.ui.notification.Notification
import com.maestros.dresspanda.home.ui.orders.repository.model.CheckoutResponse
import com.maestros.dresspanda.home.ui.orders.repository.model.OrderResponse
import com.maestros.dresspanda.util.Resource
import javax.inject.Inject

class OrdersImplementation @Inject constructor(val ordersAPI: OrdersAPI):Orders {
    override suspend fun addOrder(
        action: String?,
        map: HashMap<String, String>
    ): Resource<CheckoutResponse> {
        return try {
            val response = ordersAPI.addOrder(action,map)
            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result)

            } else {
                Log.d("XXXXXXXXX", "" + response.message())

                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }

    override suspend fun showOrders(
        action: String?,
        map: HashMap<String, String>
    ): Resource<OrderResponse> {
        return try {
            val response = ordersAPI.showOrders(action,map)
            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result)

            } else {
                Log.d("XXXXXXXXX", "" + response.message())

                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }


    override suspend fun addNotification(
        banner: String?,
        map: HashMap<String, String>
    ): Resource<Notification> {
        return try {
            val response = ordersAPI.addNotification(banner,map)
            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result)

            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }

    override suspend fun getNotification(
        banner: String?,
        map: HashMap<String, String>
    ): Resource<BaseResponse<Notification>> {
        return try {
            val response = ordersAPI.getNotification(banner,map)
            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result)

            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }

}