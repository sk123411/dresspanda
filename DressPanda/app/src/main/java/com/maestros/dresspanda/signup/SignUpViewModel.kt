package com.maestros.dresspanda.signup

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.maestros.dresspanda.login.LoginViewModel
import com.maestros.dresspanda.login.model.SocialUser
import com.maestros.dresspanda.signup.model.Register
import com.maestros.dresspanda.signup.repository.SignUP
import com.maestros.dresspanda.util.DispatcherProvider
import com.maestros.dresspanda.util.Resource
import dmax.dialog.SpotsDialog
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import java.util.*
import kotlin.collections.HashMap

class SignUpViewModel @ViewModelInject constructor(
    val dispatchers: DispatcherProvider,
    val signUP: SignUP,
    val context: Context,
    val spotsDialog: SpotsDialog.Builder,
    val fbCallbackManager: CallbackManager,
    val googleSignInClient: GoogleSignInClient
) : ViewModel() {



    sealed class SignUPEvent {
        class Success(val resultText: Register): SignUPEvent()
        class Failure(val errorText: String): SignUPEvent()
        object Loading : SignUPEvent()
        object Empty : SignUPEvent()
    }

    val fbuser_ = MutableLiveData<SocialUser>()

    val fbuser: LiveData<SocialUser>
        get() = fbuser_




    private val _signup = MutableStateFlow<SignUPEvent>(SignUPEvent.Empty)
    val signup: StateFlow<SignUPEvent> = _signup


    fun registerUser(name:String?,email:String?,password:String?) {
        _signup.value = SignUPEvent.Loading
        viewModelScope.launch(dispatchers.io) {

            val map = HashMap<String, String>()
            map.put("name", name!!)
            map.put("email", email!!)
            map.put("password", password!!)

            val signUP: Resource<Register> = signUP.register(name, map)
            when(signUP){

                is Resource.Success -> {
                    if ( signUP.data?.id!=null){
                        _signup.value = SignUpViewModel.SignUPEvent.Success(signUP.data!!)

                    }else _signup.value = SignUpViewModel.SignUPEvent.Failure(signUP.data?.result!!)
                }

                is Resource.Error ->  _signup.value = SignUPEvent.Empty

            }
            }
        }





    fun handleFbLogin(activity: Activity) {
        LoginManager.getInstance().logInWithReadPermissions(activity, Arrays.asList("email"));
        LoginManager.getInstance().registerCallback(fbCallbackManager, object :
            FacebookCallback<LoginResult?> {
            override fun onSuccess(loginResult: LoginResult?) {

                getUserProfile(loginResult?.accessToken, loginResult?.accessToken?.userId)

            }

            override fun onCancel() {

            }

            override fun onError(exception: FacebookException) {
                Log.e("Kdfhkjishf", "::"+exception.localizedMessage)
            }
        })
    }



    fun getUserProfile(token: AccessToken?, userId: String?) {

        val parameters = Bundle()
        val fbUser = SocialUser()

        parameters.putString(
            "fields",
            "id, first_name, middle_name, last_name, name, picture, email"
        )
        GraphRequest(token,
            "/$userId/",
            parameters,
            HttpMethod.GET,
            GraphRequest.Callback { response ->
                val jsonObject = response.jsonObject

                if (BuildConfig.DEBUG) {
                    FacebookSdk.setIsDebugEnabled(true)
                    FacebookSdk.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS)
                }
                // Facebook Id
                if (jsonObject.has("id")) {
                    val facebookId = jsonObject.getString("id")
                    Log.i("Facebook Id: ", facebookId.toString())
                    fbUser.aouth_id = facebookId
                    fbUser.aouth_provider = "Google"

                    //  id = facebookId.toString()
                } else {
                    Log.i("Facebook Id: ", "Not exists")
                    //  id = "Not exists"
                }

                // Facebook First Name
                if (jsonObject.has("first_name")) {
                    val facebookFirstName = jsonObject.getString("first_name")
                    Log.i("Facebook First Name: ", facebookFirstName)
                    fbUser.firstName = facebookFirstName
                } else {
                    Log.i("Facebook First Name: ", "Not exists")
                }

                // Facebook Middle Name
                if (jsonObject.has("middle_name")) {
                    val facebookMiddleName = jsonObject.getString("middle_name")
                    Log.i("Facebook Middle Name: ", facebookMiddleName)
                    //  middleName = facebookMiddleName
                } else {
                    Log.i("Facebook Middle Name: ", "Not exists")
                    // middleName = "Not exists"
                }

                // Facebook Last Name
                if (jsonObject.has("last_name")) {
                    val facebookLastName = jsonObject.getString("last_name")
                    Log.i("Facebook Last Name: ", facebookLastName)
                    fbUser.lastName = facebookLastName
                } else {
                    Log.i("Facebook Last Name: ", "Not exists")

                }
                // Facebook Name
                if (jsonObject.has("name")) {
                    val facebookName = jsonObject.getString("name")

                } else {
                    Log.i("Facebook Name: ", "Not exists")
                }
                // Facebook Profile Pic URL
                if (jsonObject.has("picture")) {
                    val facebookPictureObject = jsonObject.getJSONObject("picture")
                    if (facebookPictureObject.has("data")) {
                        val facebookDataObject = facebookPictureObject.getJSONObject("data")
                        if (facebookDataObject.has("url")) {
                            val facebookProfilePicURL = facebookDataObject.getString("url")
                            fbUser.profilePicture = facebookProfilePicURL
                            //   Log.i("Facebook Profile Pic URL: ", facebookProfilePicURL)
                            // picture = facebookProfilePicURL
                        }
                    }
                } else {
                    //  Log.i("Facebook Profile Pic URL: ", "Not exists")
                    //  picture = "Not exists"
                }

                // Facebook Email
                if (jsonObject.has("email")) {
                    val facebookEmail = jsonObject.getString("email")
                    Log.i("Facebook Email: ", facebookEmail)
                    fbUser.email = facebookEmail
                } else {
                    Log.i("Facebook Email: ", "Not exists")
                }
                fbuser_.value = fbUser

            }).executeAsync()


    }





    fun handleSignInGoogle(data: Intent){
        val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
        handleSignInResult(task)
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account: GoogleSignInAccount? = completedTask.getResult(ApiException::class.java)
            val fbUser = SocialUser()
            fbUser.aouth_id = account?.id
            fbUser.aouth_provider = "Google"
            fbUser.firstName = account!!.givenName
            fbUser.email = account!!.email
            fbuser_.value = fbUser



        } catch (e: ApiException) {
            Log.e("XXXXXXX", "::"+e.message)
            Log.e("XXXXXXX", "::"+e.statusCode)
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
        }
    }

    fun login(socialUser: SocialUser){
        _signup.value = SignUpViewModel.SignUPEvent.Loading
        val map = HashMap<String,String>()
        map.put("name", socialUser.firstName!!)
        map.put("email", socialUser.email!!)
        map.put("aouth_id", socialUser.aouth_id!!)
        map.put("aouth_provider", socialUser.aouth_provider!!)

        viewModelScope.launch {
            val response = signUP.socialLogin("social_sign_up", map)

            when(response){

                is Resource.Success-> {
                    if (response.data?.id != null) {
                        _signup.value = SignUpViewModel.SignUPEvent.Success(response.data!!)

                    } else _signup.value =
                        SignUpViewModel.SignUPEvent.Failure(response.data?.result!!)
                }
                is Resource.Error-> _signup.value = SignUpViewModel.SignUPEvent.Failure(response.message!!)


            }


        }

    }


}





