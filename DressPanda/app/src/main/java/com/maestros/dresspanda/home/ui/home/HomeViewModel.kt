package com.maestros.dresspanda.home.ui.home

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.maestros.dresspanda.home.ui.brand.BaseResponse
import com.maestros.dresspanda.home.ui.brand.HomeResponse
import com.maestros.dresspanda.home.ui.home.repository.Home
import com.maestros.dresspanda.data.home.HomeBrandsDao
import com.maestros.dresspanda.data.product.ProductDAO
import com.maestros.dresspanda.home.ui.home.model.Slider
import com.maestros.dresspanda.home.ui.product.model.productdetail.ProductResponse
import com.maestros.dresspanda.home.ui.shop.ShopViewModel
import com.maestros.dresspanda.seller.repository.model.SellerResponse
import com.maestros.dresspanda.util.Resource
import kotlinx.coroutines.launch

class HomeViewModel @ViewModelInject constructor(
    val home: Home,
    val homeBrandsDao: HomeBrandsDao,
    val productDao: ProductDAO

) : ViewModel() {

    val brandlist_ = MutableLiveData<List<BaseResponse<HomeResponse>>>()

    val brandlist: LiveData<List<BaseResponse<HomeResponse>>>
        get() = brandlist_

    val productList = MutableLiveData<List<ProductResponse>>()

    val productResponseList_: LiveData<List<ProductResponse>>
        get() = productList


    private val _home = MutableLiveData<HomeEvent>(HomeEvent.Empty)
    val homeEvent: LiveData<HomeEvent> = _home

    private val _products = MutableLiveData<HomeEvent>(HomeEvent.Empty)
    val products: LiveData<HomeEvent> = _products


    private val _shops = MutableLiveData<HomeEvent>(HomeEvent.Empty)
    val shops: LiveData<HomeEvent> = _shops


    init {

        _home.value = HomeViewModel.HomeEvent.Loading

    }

    private val _text = MutableLiveData<String>().apply {
        value = "This is home Fragment"
    }
    val text: LiveData<String> = _text

    sealed class HomeEvent {
        class SuccessBrands(val resultText: List<HomeResponse>) : HomeEvent()
        class Failure(val errorText: String) : HomeEvent()
        object Loading : HomeEvent()
        object Empty : HomeEvent()
        class SuccessProduct(val resultText: List<ProductResponse>) : HomeEvent()
        class SuccessShops(val resultText: List<SellerResponse>) : HomeEvent()
        class SliderData(val data: List<Slider>) : HomeEvent()

    }


    fun getHomeBrands(): HomeViewModel.HomeEvent {

        var isListEmpty: Boolean? = false
        _home.value = HomeViewModel.HomeEvent.Loading

        viewModelScope.launch {

            if (homeBrandsDao.getAllBrands().isNotEmpty()) {

                isListEmpty = false
                _home.value = HomeViewModel.HomeEvent.SuccessBrands(homeBrandsDao.getAllBrands())
                return@launch


            } else {
                isListEmpty = true
            }


            val response = home.getBrands(action = "show_brand")

            when (response) {

                is Resource.Success -> {
                    homeBrandsDao.deleteAll()
                    homeBrandsDao.insertBrands(response.data!!)
                    if (isListEmpty!!) {
                        _home.value = HomeViewModel.HomeEvent.SuccessBrands(response.data!!)

                    }

                }
                is Resource.Error -> _home.value =
                    HomeViewModel.HomeEvent.Failure(response.message!!)
            }

        }
        return _home.value!!

    }

    fun getHomeProducts() {

        _home.value = HomeViewModel.HomeEvent.Loading
        var isListEmpty: Boolean? = false


        viewModelScope.launch {

            if (productDao.getAllProducts().isNotEmpty()) {
                isListEmpty = false
                _home.value = HomeViewModel.HomeEvent.SuccessProduct(productDao.getAllProducts())
                return@launch

            } else {
                isListEmpty = true
            }


            val response = home.getProducts("show_product")

            when (response) {

                is Resource.Success -> {
                    productDao.deleteAll()
                    productDao.insertProducts(response.data!!)
                    if (isListEmpty!!){
                        _home.value = HomeViewModel.HomeEvent.SuccessProduct(response.data!!)

                    }

                }
                is Resource.Error -> _home.value =
                    HomeViewModel.HomeEvent.Failure(response.message!!)
            }

        }

    }

    fun getSliderData() {
        _home.value = HomeViewModel.HomeEvent.Loading

        viewModelScope.launch {
            val response = home.getSliderDataHome("show_banner")

            when (response) {

                is Resource.Success -> {

                    _home.value = HomeEvent.SliderData(response.data!!)

                }
                is Resource.Error -> _home.value =
                    HomeViewModel.HomeEvent.Failure(response.message!!)
            }


        }
    }


}