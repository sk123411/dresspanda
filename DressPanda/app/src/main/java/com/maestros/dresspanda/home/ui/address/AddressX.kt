package com.maestros.dresspanda.home.ui.address


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.maestros.dresspanda.home.ui.address.repository.allAddressModel.Data

data class AddressX(
    @SerializedName("data")
    @Expose
    val `data`: Data,
    @SerializedName("result")
    @Expose
    val result: Boolean

)