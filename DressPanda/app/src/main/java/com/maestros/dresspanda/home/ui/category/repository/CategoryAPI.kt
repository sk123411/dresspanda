package com.maestros.dresspanda.login.repository

import com.maestros.dresspanda.home.ui.brand.BaseResponse
import com.maestros.dresspanda.home.ui.brand.HomeResponse
import com.maestros.dresspanda.home.ui.category.adapter.model.CategoryResponse
import com.maestros.dresspanda.home.ui.category.adapter.model.SubCategoryResponse
import com.maestros.dresspanda.login.model.ForgetPassword
import com.maestros.dresspanda.signup.model.Register
import retrofit2.Response
import retrofit2.http.*

interface CategoryAPI {

    @GET("dresspanda/api/process.php")
    suspend fun getCategories(@Query("action") category:String?): Response<BaseResponse<CategoryResponse>>

    @POST("dresspanda/api/process.php")
    @FormUrlEncoded
    suspend fun getSubCategoriesByCategory(@Query("action") category:String?,
    @FieldMap map:HashMap<String,String>): Response<BaseResponse<SubCategoryResponse>>



}