package com.maestros.dresspanda.login.repository

import com.maestros.dresspanda.login.model.ForgetPassword
import com.maestros.dresspanda.signup.model.Register
import retrofit2.Response
import retrofit2.http.*

interface LoginAPI {

    @POST("dresspanda/api/process.php")
    @FormUrlEncoded
    suspend fun login(@Query("action") signup:String?, @FieldMap data:Map<String,String>?):Response<Register>



    @POST("dresspanda/api/process.php")
    @FormUrlEncoded
    suspend fun socialLogin(@Query("action")
                                social_sign_up:String?, @FieldMap data:Map<String,String>?):Response<Register>



    @POST("dresspanda/api/process.php")
    @FormUrlEncoded
    suspend fun forgetPassword(@Query("action")
                            forget_password:String?, @FieldMap data:Map<String,String>?):Response<ForgetPassword>





}