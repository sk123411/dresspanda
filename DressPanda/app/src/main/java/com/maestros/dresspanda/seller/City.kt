package com.maestros.dresspanda.seller

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class City(
    @SerializedName("city_id")
    @Expose
    val city_id: String,
    @SerializedName("city_name")
    @Expose
    val city_name: String,
    @SerializedName("id")
    @Expose
    val id: String,
    @SerializedName("mini_city")
    @Expose
    val mini_city: String,
    @SerializedName("path")
    @Expose
    val path: String,
    @SerializedName("state_id")
    @Expose
    val state_id: String


)