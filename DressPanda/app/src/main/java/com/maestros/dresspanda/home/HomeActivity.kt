package com.maestros.dresspanda.home

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.StrictMode
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.RadioGroup
import android.widget.Toast
import androidx.activity.viewModels
import com.google.android.material.navigation.NavigationView
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import com.facebook.login.LoginManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.maestros.dresspanda.R
import com.maestros.dresspanda.databinding.ActivityHomeBinding
import com.maestros.dresspanda.databinding.SelectThemeLayoutBinding
import com.maestros.dresspanda.helper.HelperViewModel
import com.maestros.dresspanda.home.ui.cart.CartFragmentViewModel
import com.maestros.dresspanda.home.ui.home.HomeViewModel
import com.maestros.dresspanda.login.LoginActivity
import com.maestros.dresspanda.login.LoginViewModel
import com.maestros.dresspanda.util.Constant
import com.maestros.dresspanda.util.Resource
import dagger.hilt.android.AndroidEntryPoint
import io.paperdb.Paper
import kotlinx.coroutines.launch

@AndroidEntryPoint
class HomeActivity : AppCompatActivity() {


    lateinit var binding: ActivityHomeBinding

    private lateinit var appBarConfiguration: AppBarConfiguration
    private val loginViewModel: LoginViewModel by viewModels()
    var isUserLoggedIn:Boolean?=false
    var from:String?=""
    private val cartViewModel: CartFragmentViewModel by viewModels()

    companion object {
        lateinit var bottomNavigationView: BottomNavigationView
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val builder: StrictMode.VmPolicy.Builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())


        Paper.init(this)
        val user = Paper.book().read<Int>(Constant.USER_LOGGED_IN,0)

        isUserLoggedIn = user != 0
        Log.d("UUUUUUUUU", "::"+isUserLoggedIn)

        from = intent.getStringExtra(Constant.FROM)




        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        bottomNavigationView = binding.barMain.contentMain.bottomNavView

        if (isUserLoggedIn!!) {
            cartViewModel.getCartItems(Constant.getUserID(applicationContext))
        }
        setSupportActionBar(binding.barMain.toolbarMain.toolbar)

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home

            ), drawerLayout
        )

        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)



        if (from.equals("cart")){
            navController.navigate(R.id.cartFragment)

        }else if(from.equals("product_detail")){
            navController.navigate(R.id.productDetailFragment)

        }else if(from.equals("orders")){
            navController.navigate(R.id.ordersFragment)

        }else if(from.equals("shop_detail")){

            val bundle = Bundle()
            bundle.putBoolean(Constant.FROM_FOLLOWED_SHOPS,false)
            navController.navigate(R.id.shopDetailsFragment,bundle)

        }else if(from.equals("news")){


            navController.navigate(R.id.newsFeedFragment)

        }



        val orderMenu = navView.menu.findItem(R.id.nav_orders)
        val dashboardMenu = navView.menu.findItem(R.id.nav_dashboard)
        val messagesMenu = navView.menu.findItem(R.id.nav_messages)
        val loginMenu = navView.menu.findItem(R.id.nav_sign_out)

        if (!isUserLoggedIn!!){
            orderMenu.isVisible = false
            dashboardMenu.isVisible = false
            messagesMenu.isVisible = false

            loginMenu.setTitle("Log in")


        }



        navView.setNavigationItemSelectedListener { it ->


            if (it.itemId == R.id.nav_wishlist) {


                binding.drawerLayout.closeDrawer(GravityCompat.START)

                false
            } else if (it.itemId == R.id.nav_contact) {

                navController.navigate(R.id.contactUsFragment)

                binding.drawerLayout.closeDrawer(GravityCompat.START)

                false
            } else if (it.itemId == R.id.nav_dashboard) {

                navController.navigate(R.id.myAccountFragment)

                binding.drawerLayout.closeDrawer(GravityCompat.START)

                false
            } else if (it.itemId == R.id.nav_shop) {

                navController.navigate(R.id.followedShopFragment)

                binding.drawerLayout.closeDrawer(GravityCompat.START)

                false
            } else if (it.itemId == R.id.nav_orders) {

                navController.navigate(R.id.ordersFragment)

                binding.drawerLayout.closeDrawer(GravityCompat.START)

                false
            } else if (it.itemId == R.id.nav_dashboard) {

                navController.navigate(R.id.myAccountFragment)

                binding.drawerLayout.closeDrawer(GravityCompat.START)

                false
            } else if (it.itemId == R.id.nav_messages) {

                navController.navigate(R.id.chatFragment)

                binding.drawerLayout.closeDrawer(GravityCompat.START)

                false
            } else if (it.itemId == R.id.nav_apply_as_seller) {
                navController.navigate(R.id.applySellerFragment)
                binding.drawerLayout.closeDrawer(GravityCompat.START)

                false
            } else if (it.itemId == R.id.nav_apply_as_manager) {
                navController.navigate(R.id.applyManagerFragment)
                binding.drawerLayout.closeDrawer(GravityCompat.START)

                false

            } else if (it.itemId == R.id.all_shop) {
                Paper.init(applicationContext)
                Paper.book().write(Constant.FROM_BRANDS, 0)
                navController.navigate(R.id.shoplistFragment)
                binding.drawerLayout.closeDrawer(GravityCompat.START)

                false

            } else if (it.itemId == R.id.nav_sign_out) {


                if (isUserLoggedIn!!) {
                    loginViewModel.saveUserLogin(0)
                    LoginManager.getInstance().logOut()
                    Paper.init(this)
                    Paper.book().delete(Constant.USER_LOGGED_IN)
                    Paper.book().delete(Constant.USER_DETAILS)

                    startActivity(
                        Intent(this@HomeActivity, LoginActivity::class.java).addFlags(
                            Intent.FLAG_ACTIVITY_CLEAR_TASK
                        ).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    )

                }else {
                    startActivity(Intent(this@HomeActivity, LoginActivity::class.java))

                }

                false

            } else if (it.itemId == R.id.nav_dark_mode) {
                openDialog()
                false
            } else if (it.itemId == R.id.nav_cart) {




                if(isUserLoggedIn!!) {
                    val bundle = Bundle()
                    bundle.putString(Constant.FROM,"cart")

                    navController.navigate(R.id.cartFragment, bundle)
                }else {

                    startActivity(Intent(this,LoginActivity::class.java).putExtra(Constant.FROM,"cart"))

                }
                false
            }

            false

        }


        navController.addOnDestinationChangedListener(object :
            NavController.OnDestinationChangedListener {
            override fun onDestinationChanged(
                controller: NavController,
                destination: NavDestination,
                arguments: Bundle?
            ) {

                when (destination.id) {
//
//                    R.id.nav_home -> bottomNavigationView.selectedItemId = R.id.bottom_home
//                    R.id.wishlistFragment -> bottomNavigationView.selectedItemId = R.id.bottom_wishlist
//                    R.id.cartFragment -> bottomNavigationView.selectedItemId = R.id.bottom_cart
//                    R.id.nav_dashboard -> bottomNavigationView.selectedItemId = R.id.bottom_profile


                }













            }

        })


        binding.barMain.contentMain.bottomNavView.setOnNavigationItemSelectedListener { item ->

            val root = binding.barMain.contentMain.bottomNavView
            when (item.itemId) {
                R.id.bottom_home -> {

//                    val menuItem:MenuItem = root.menu.findItem(R.id.bottom_home)
//                    menuItem.
                    navController.navigate(R.id.nav_home)
                    true
                }
                R.id.bottom_wishlist -> {

                    navController.navigate(R.id.wishlistFragment)
                    // Respond to navigation item 2 click
                    true
                }
                R.id.bottom_profile -> {



                    if(isUserLoggedIn!!) {
                        val bundle = Bundle()
                        bundle.putString(Constant.FROM,"dashboard")

                        navController.navigate(R.id.myAccountFragment)
                    }else {

                        startActivity(Intent(this,LoginActivity::class.java).putExtra(Constant.FROM,"dashboard"))

                    }


                    // Respond to navigation item 2 click
                    true
                }

                R.id.bottom_cart -> {


                    if(isUserLoggedIn!!) {
                        val bundle = Bundle()
                        bundle.putString(Constant.FROM,"cart")

                        navController.navigate(R.id.cartFragment)
                    }else {

                        startActivity(Intent(this,LoginActivity::class.java).putExtra(Constant.FROM,"dashboard"))

                    }

                    true
                }
                else -> false
            }


        }





        lifecycleScope.launchWhenStarted {

            cartViewModel.cartItems.observe(this@HomeActivity, Observer {

                when (it) {


                    is CartFragmentViewModel.CartEvent.GetCartItemSuccess -> {

                        Constant.setCartCount(
                            bottomNavigationView,
                            it.cartItems.qntity
                        )
                    }
                    is CartFragmentViewModel.CartEvent.Failure -> {


                    }
                }


            })
        }

    }


    private fun openDialog() {
        val dialog = Dialog(this)
        val binding = SelectThemeLayoutBinding.inflate(layoutInflater)
        dialog.setContentView(binding.root)
        dialog.show()
        var radioStatusText: Int? = 0


        when (getThemeStatus()) {
            0 -> binding.lightRadio.isChecked = true
            1 -> binding.darkRadio.isChecked = true
        }


        binding.radioGrounpTheme.setOnCheckedChangeListener(object :
            RadioGroup.OnCheckedChangeListener {
            override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {

                if (checkedId == R.id.lightRadio) {
                    radioStatusText = 0
                    saveThemeOption(radioStatusText)

                } else {

                    radioStatusText = 1
                    saveThemeOption(radioStatusText)
                }


            }

        })

        binding.changeBtn.setOnClickListener {
            if (radioStatusText == 0) {

                dialog.dismiss()

                startActivity(
                    Intent(this, HomeActivity::class.java).addFlags(
                        Intent.FLAG_ACTIVITY_CLEAR_TASK
                    ).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                )

            } else {

                dialog.dismiss()
                startActivity(
                    Intent(this, HomeActivity::class.java).addFlags(
                        Intent.FLAG_ACTIVITY_CLEAR_TASK
                    ).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                )

            }
        }

    }


    private fun saveThemeOption(themeStatus: Int?) {

        Paper.init(this)
        Paper.book().write(Constant.THEME_OPTION, themeStatus)

    }

    private fun getThemeStatus(): Int? {
        Paper.init(this)
        return Paper.book().read<Int>(Constant.THEME_OPTION, 0)
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        //    menuInflater.inflate(R.menu.home, menu)
        return true
    }


    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) ||
                super.onSupportNavigateUp()
    }


    override fun onStart() {
        super.onStart()
        when (getThemeStatus()) {
            0 -> {

                AppCompatDelegate
                    .setDefaultNightMode(
                        AppCompatDelegate
                            .MODE_NIGHT_NO
                    );

            }
            1 -> {
                AppCompatDelegate
                    .setDefaultNightMode(
                        AppCompatDelegate
                            .MODE_NIGHT_YES
                    )

            }
        }
    }


}


