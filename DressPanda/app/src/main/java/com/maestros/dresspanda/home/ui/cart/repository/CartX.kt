package com.maestros.dresspanda.home.ui.cart.repository


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.maestros.dresspanda.home.ui.product.model.productdetail.ProductResponse

data class CartX(
    @SerializedName("check_out_status")
    @Expose
    val checkOutStatus: String,
    @SerializedName("id")
    @Expose

    val id: String,
    @SerializedName("data")
    @Expose

    val product: MutableList<Product>,
    @SerializedName("product_id")
    @Expose

    val productId: String,
    @SerializedName("product_price")
    @Expose

    val productPrice: String,
    @SerializedName("qntity")
    @Expose

    val qntity: String,
    @SerializedName("seller_id")
    @Expose

    val sellerId: String,
    @SerializedName("strtotime")
    @Expose

    val strtotime: String,
    @SerializedName("total_amount")
    @Expose

    val totalAmount: Int,
    @SerializedName("total_price")
    @Expose

    val totalPrice: String,
    @SerializedName("user_id")
    @Expose

    val userId: String
)