package com.maestros.dresspanda.signup

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.google.android.material.snackbar.Snackbar
import com.maestros.dresspanda.R
import com.maestros.dresspanda.databinding.ActivityHomeBinding
import com.maestros.dresspanda.databinding.ActivitySignUpBinding
import com.maestros.dresspanda.login.LoginActivity
import com.maestros.dresspanda.login.LoginViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class SignUpActivity : AppCompatActivity() {


    private val viewModel: SignUpViewModel by viewModels()
    lateinit var binding: ActivitySignUpBinding
    lateinit var progressDialog: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySignUpBinding.inflate(layoutInflater)
        setContentView(binding.root)
        progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Loading....")


        binding.spSignUpButton.setOnClickListener {

                if (binding.spEditEmail.text.toString()
                        .isNotEmpty() && binding.spEditName.text.toString().isNotEmpty()
                    && binding.spEditPassword.text.toString().isNotEmpty()
                ) {
                    viewModel.registerUser(
                        binding.spEditName.text.toString().trim(),
                        binding.spEditEmail.text.toString().trim(),
                        binding.spEditPassword.text.toString().trim()
                    )
                } else Toast.makeText(
                    applicationContext,
                    "Email or Password is empty",
                    Toast.LENGTH_SHORT
                ).show()

        }

        lifecycleScope.launchWhenStarted {

            viewModel.signup.collect {
                when (it) {

                    is SignUpViewModel.SignUPEvent.Success -> {

                        proceedToSignIn(binding.root)
                        progressDialog.dismiss()

                    }
                    is SignUpViewModel.SignUPEvent.Loading -> {
                        progressDialog.show()
                    }
                    is SignUpViewModel.SignUPEvent.Empty -> {
                        progressDialog.dismiss()
                        viewModel.spotsDialog.build().dismiss()
                    }

                    is SignUpViewModel.SignUPEvent.Failure -> {
                        progressDialog.dismiss()

                    }
                    else -> Unit
                }
            }
        }


        viewModel.fbuser.observe(this, Observer {
            if (it!=null){
                viewModel.login(it)
            }
        })

    }

        fun proceedToSignIn(v: View) {

            startActivity(Intent(v.context, LoginActivity::class.java))
        }


        fun navigateToSignIn(view: View) {
            startActivity(Intent(view.context, LoginActivity::class.java))
        }



    fun registerWithFB(view: View) {

        viewModel.handleFbLogin(this)
    }
    fun registerWithGoogle(view: View) {


        val intent = viewModel.googleSignInClient.signInIntent
        startActivityForResult(intent, 101)

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d("DATAAAA", "::"+data)
        if(requestCode==101){
            viewModel.handleSignInGoogle(data!!)
        }else{
            Log.d("DATAAAA", "::called"+data)
            viewModel.fbCallbackManager.onActivityResult(requestCode, resultCode, data!!);

        }

    }

}
