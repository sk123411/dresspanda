package com.maestros.dresspanda.util

import android.text.Editable
import android.text.TextWatcher

class TextChangeListernerImple(val textChangeListener: TextChangeListerner): TextWatcher {
    override fun afterTextChanged(s: Editable?) {
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        textChangeListener.onTextChange(s.toString())
    }


    interface TextChangeListerner{

        fun onTextChange(text:String?)

    }
}