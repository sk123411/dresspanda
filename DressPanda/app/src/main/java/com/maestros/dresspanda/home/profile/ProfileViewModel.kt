package com.maestros.dresspanda.home.profile

import android.content.Context
import android.text.TextUtils
import android.util.Log
import android.widget.EditText
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.maestros.dresspanda.R
import com.maestros.dresspanda.home.profile.repository.Profile
import com.maestros.dresspanda.home.profile.repository.ProfileAPI
import com.maestros.dresspanda.home.ui.brand.HomeResponse
import com.maestros.dresspanda.home.ui.home.HomeViewModel
import com.maestros.dresspanda.signup.model.Register
import com.maestros.dresspanda.util.Constant
import com.maestros.dresspanda.util.Resource
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

class ProfileViewModel @ViewModelInject constructor(
    val context: Context,
    val profile: Profile):ViewModel(){


    private val _profile = MutableStateFlow<ProfileEvent>(ProfileEvent.Empty)
    val profileEvent: StateFlow<ProfileEvent> = _profile

    val map = HashMap<String,String>()

    private val _register = MutableStateFlow<Register>(Register("","","","","","",
    "","","","","","",""))

    val register: StateFlow<Register> = _register




    private val _hidePasswordMap = MutableLiveData<HashMap<String,String>>()

    val hidePasswordMap:LiveData<HashMap<String,String>>
        get() = _hidePasswordMap








    private val imageFile_ = MutableLiveData<File>()



    val imageFile: LiveData<File>
        get() = imageFile_




   private val _emptyValueEvent = MutableLiveData<Boolean>(true)
    val emptyValueEvent: LiveData<Boolean> = _emptyValueEvent


    sealed class ProfileEvent {
        class SuccessPasswordChange(val resultText: Register): ProfileEvent()
        class SuccessProfileUpdate(val resultText: Register): ProfileEvent()
        class SuccessGetProfile(val resultText: Register): ProfileEvent()
        class GetProfile(val resultText: Register): ProfileEvent()

        class Failure(val errorText: String): ProfileEvent()
        object Loading : ProfileEvent()
        object Empty :ProfileEvent()

        }

    fun setHidePasswordValues(key:String?, value:String?){
        map.put(key!!,value!!)
        _hidePasswordMap.value = map
    }

    fun changePassword(
        userId:String?,
        oldPassword:String?, newPassword:String?,
        confirmPassword:String?){

        _profile.value = ProfileEvent.Loading
        val map = HashMap<String,String>()
        map.put("user_id", userId!!)
        map.put("old_password", oldPassword!!)
        map.put("new_password", newPassword!!)
        map.put("confirm_password", confirmPassword!!)


        viewModelScope.launch {

            val response = profile.changePassword(action = "change_password",
               data =  map)

            when(response){
                is Resource.Success->{
                    _profile.value = ProfileEvent.SuccessPasswordChange(response.data!!)

                    _register.value = response.data
                }
                is Resource.Error-> _profile.value = ProfileEvent.Failure(response.message!!)
            }

        }
    }


    fun checkInputEmptyFields(oldPassword: EditText?, newPassword: EditText?,
                              cnfPassword: EditText?):Boolean {

        if (TextUtils.isEmpty(oldPassword?.text.toString().trim())){
            oldPassword?.setError(context.resources.getString(R.string.emptyName))
            _emptyValueEvent.value = true
            return false
        }else if (TextUtils.isEmpty(newPassword?.text.toString().trim())){
            newPassword?.setError(context.resources.getString(R.string.emptyName))
            _emptyValueEvent.value = true

            return false
        }else if (TextUtils.isEmpty(cnfPassword?.text.toString().trim())) {
            cnfPassword?.setError(context.resources.getString(R.string.emptyName))
            _emptyValueEvent.value = true

        } else if (!newPassword?.text.toString().trim().equals(cnfPassword?.text.toString().trim())){
            _emptyValueEvent.value = true
            newPassword?.setError(context.resources.getString(R.string.password_nt_matched))

            return false

        }else {
            _emptyValueEvent.value = false
        }

    return _emptyValueEvent.value!!
    }



    fun changeName(name:String?, userId: String?){
        _profile.value = ProfileEvent.Loading

        val map = HashMap<String,String>()
        map.put("user_id", userId!!)
        map.put("name", name!!)
        viewModelScope.launch {
            val response = profile.changeName("update_profile",map)

            when(response){

                is Resource.Success -> {

                    _profile.value = ProfileViewModel.ProfileEvent.SuccessProfileUpdate(response.data!!)
                }

                is Resource.Error -> {

                    _profile.value = ProfileViewModel.ProfileEvent.Failure(response.message!!)
                }
            }
        }
    }



    fun changeEmail(email:String?, userId: String?){
        _profile.value = ProfileEvent.Loading

        val map = HashMap<String,String>()
        map.put("user_id", userId!!)
        map.put("email", email!!)
        viewModelScope.launch {
            val response = profile.changeEmail("update_profile",map)

            when(response){

                is Resource.Success -> {

                    _profile.value = ProfileViewModel.ProfileEvent.SuccessProfileUpdate(response.data!!)

                }

                is Resource.Error -> {

                    _profile.value = ProfileViewModel.ProfileEvent.Failure(response.message!!)
                }
            }
        }
    }


    fun changeDOB(dob:String? , userId: String?){
        _profile.value = ProfileEvent.Loading

        val map = HashMap<String,String>()
        map.put("user_id", userId!!)
        map.put("DOB", dob!!)
        viewModelScope.launch {
            val response = profile.changeEmail("update_profile",map!!)

            when(response){

                is Resource.Success -> {

                    _profile.value = ProfileViewModel.ProfileEvent.SuccessProfileUpdate(response.data!!)

                }

                is Resource.Error -> {

                    _profile.value = ProfileViewModel.ProfileEvent.Failure(response.message!!)
                }
            }
        }
    }


    fun changeNumber(number:String? , userId: String?){
        _profile.value = ProfileEvent.Loading

        val map = HashMap<String,String>()
        map.put("user_id", userId!!)
        map.put("phone_number", number!!)
        viewModelScope.launch {
            val response = profile.changeNumber("update_profile",map!!)

            when(response){

                is Resource.Success -> {

                    _profile.value = ProfileViewModel.ProfileEvent.SuccessProfileUpdate(response.data!!)

                }

                is Resource.Error -> {

                    _profile.value = ProfileViewModel.ProfileEvent.Failure(response.message!!)
                }
            }
        }
    }



    fun changeGender(gender:String? , userId: String?){
        _profile.value = ProfileEvent.Loading

        val map = HashMap<String,String>()
        map.put("user_id", userId!!)
        map.put("gender", gender!!)
        viewModelScope.launch {
            val response = profile.changeNumber("update_profile",map)

            when(response){

                is Resource.Success -> {

                    _profile.value = ProfileViewModel.ProfileEvent.SuccessProfileUpdate(response.data!!)

                }

                is Resource.Error -> {

                    _profile.value = ProfileViewModel.ProfileEvent.Failure(response.message!!)
                }
            }
        }
    }


    fun changeImage(file: File?, user_id:String?){
        _profile.value = ProfileEvent.Loading

        val storeImage: RequestBody =  RequestBody.create(
            "image/jpg".toMediaTypeOrNull(),
            file!!)

        val storeImageBody: MultipartBody.Part =
            MultipartBody.Part.createFormData(
                Constant.IMAGE!!,
                file.getName(), storeImage)


        val userBody = getMultiPartFormRequestBody(user_id)

        viewModelScope.launch {


            val response = profile.changeImage("update_profile",
                storeImageBody, userBody)

            when(response){

                is Resource.Success -> {

                    _profile.value = ProfileViewModel.ProfileEvent.SuccessProfileUpdate(response.data!!)

                }

                is Resource.Error -> {

                    _profile.value = ProfileViewModel.ProfileEvent.Failure(response.message!!)
                }
            }


        }






    }


    fun getMultiPartFormRequestBody(tag:String?):RequestBody{
        return RequestBody.create(MultipartBody.FORM, tag!!)

    }


    fun getProfile(user_id:String?){

        _profile.value = ProfileEvent.Loading


        val map = HashMap<String,String>()
        map.put("user_id",user_id!!)

        viewModelScope.launch {
            val response = profile.getProfile("update_profile",map)
            Log.d("RESSSSSSSSSS"," " +response.data );

            when(response){

                is Resource.Success -> {

                    _profile.value = ProfileViewModel.ProfileEvent.GetProfile(response.data!!)
                    _register.value = response.data
                    Log.d("RESSSSSSSSSS"," " +response.data );

                }

                is Resource.Error -> {

                    _profile.value = ProfileViewModel.ProfileEvent.Failure(response.message!!)
                }
            }
        }
    }

    fun sendImageFile(file: File) {

        imageFile_.value = file
    }


}