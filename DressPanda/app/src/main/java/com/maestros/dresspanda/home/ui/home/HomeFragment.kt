package com.maestros.dresspanda.home.ui.home

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.activity.viewModels
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.maestros.dresspanda.R
import com.maestros.dresspanda.databinding.FragmentHomeBinding
import com.maestros.dresspanda.helper.HelperViewModel
import com.maestros.dresspanda.home.HomeActivity
import com.maestros.dresspanda.home.ui.brand.HomeResponse
import com.maestros.dresspanda.home.ui.cart.CartFragmentViewModel
import com.maestros.dresspanda.home.ui.home.model.Slider
import com.maestros.dresspanda.home.ui.product.model.productdetail.ProductResponse
import com.maestros.dresspanda.login.LoginActivity
import com.maestros.dresspanda.util.Constant
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType
import com.smarteist.autoimageslider.SliderAnimations
import dagger.hilt.android.AndroidEntryPoint
import io.paperdb.Paper

@AndroidEntryPoint
class HomeFragment : Fragment() {

    private  val homeViewModel: HomeViewModel by viewModels()
    private val cartViewModel: CartFragmentViewModel by viewModels()

    lateinit var binding:FragmentHomeBinding
    lateinit var customToolbarRoot:RelativeLayout
    lateinit var sliderAdapter: SliderAdapter





    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeBinding.inflate(inflater)

        homeViewModel.getHomeBrands()
        homeViewModel.getHomeProducts()
        homeViewModel.getSliderData()



        if (Constant.isUserLogged(requireActivity())!!) {
            cartViewModel.getCartItems(Constant.getUserID(context))
        }




        customToolbarRoot= requireActivity().findViewById<RelativeLayout>(R.id.toolbarParent)

        val notiButton = activity?.findViewById<ImageView>(R.id.notificationButton)

        notiButton?.setOnClickListener {

            findNavController().navigate(R.id.settingsFragment)

        }


        lifecycleScope.launchWhenStarted {
            homeViewModel.homeEvent.observe(viewLifecycleOwner, Observer { it ->
                when (it) {

                    is HomeViewModel.HomeEvent.SuccessBrands -> {
                        activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.GONE

                        binding.brandlist.apply {
                            layoutManager =
                                LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                            adapter =
                                HomeAdapter<HomeResponse>(
                                    it.resultText,
                                    R.layout.brand_item_list
                                )
                        }
                    }

                    is HomeViewModel.HomeEvent.SuccessProduct -> {
                        activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.GONE

                        binding.productsList.apply {
                            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                            adapter =
                                HomeAdapter<ProductResponse>(
                                    it.resultText,
                                    R.layout.lyt_product_item
                                )
                        }

                    }

                    is HomeViewModel.HomeEvent.SliderData -> {
                        activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.GONE

                        sliderAdapter = SliderAdapter(requireContext())
                        for (data in it.data){
                            sliderAdapter.addItem(Slider(data.title, "${Constant.BASE_URL}${data.image}" ))

                        }

                        binding.imageSlider.setSliderAdapter(sliderAdapter)
                        binding.imageSlider.setIndicatorAnimation(IndicatorAnimationType.WORM);
                        binding.imageSlider.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
                        binding.imageSlider.startAutoCycle();

                    }

                    is HomeViewModel.HomeEvent.Failure -> {
                        activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.GONE

                        Toast.makeText(context, "Servor error occured", Toast.LENGTH_SHORT).show()
                    }

                    is HomeViewModel.HomeEvent.Loading -> {
                        activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.VISIBLE

                    }



                }
            })


        }

        lifecycleScope.launchWhenStarted {

            cartViewModel.cartItems.observe(viewLifecycleOwner, Observer {

                when(it) {
                    is CartFragmentViewModel.CartEvent.Loading -> {



                    }
                    is CartFragmentViewModel.CartEvent.GetCartItemSuccess -> {
//                        val bottomNavigationView = activity?.findViewById<BottomNavigationView>(R.id.bottomNavView)
//
//                        val actionLayout:MenuItem =  bottomNavigationView?.menu!!.findItem(R.id.bottom_cart)
//
//                        val rootLayout:FrameLayout = actionLayout.actionView.findViewById(R.id.rootLayout)
//
//                        val counterLayout = rootLayout.findViewById<FrameLayout>(R.id.cartRootLayout)
//                        counterLayout.visibility = View.VISIBLE




                    }
                }


            })
        }


        binding.fhNewsFeed.setOnClickListener {


            if(Constant.isUserLogged(requireActivity())!!){

                navigateTo(it, R.id.newsFeedFragment)

            }else {

                startActivity(Intent(requireContext(), LoginActivity::class.java).putExtra(Constant.FROM,"news"))
            }
        }



        binding.fhCategories.setOnClickListener {

            navigateTo(it, R.id.categoryFragment)
        }


        binding.showAllProducts.setOnClickListener {


            Paper.init(context)
            Paper.book().write(Constant.IS_FROM_HOME,true)
            navigateTo(it, R.id.productListFragment)
        }

        binding.showAllBrands.setOnClickListener {

            navigateTo(it, R.id.brandsFragment)

        }

//        binding.fhConpaigns.setOnClickListener {
//            navigateTo(it, R.id.compaignsFragment)
//        }
//
//        binding.fhGifts.setOnClickListener {
//            navigateTo(it, R.id.giftFragment)
//        }

        binding.fhOrders.setOnClickListener {



            if(Constant.isUserLogged(requireActivity())!!){

                navigateTo(it, R.id.ordersFragment)

            }else {

                startActivity(Intent(requireContext(), LoginActivity::class.java).putExtra(Constant.FROM,"orders"))
            }
        }

        return binding.root

    }

    private fun navigateTo(view: View?, destinationId:Int?) {
        activity?.findNavController(R.id.nav_host_fragment)!!.navigate(destinationId!!)

    }

    override fun onPause() {
        super.onPause()

        customToolbarRoot.visibility = View.GONE
    }

    override fun onResume() {
        super.onResume()


        customToolbarRoot.visibility = View.VISIBLE
        HomeActivity.bottomNavigationView.menu.findItem(R.id.bottom_home).setChecked(true)


    }

}