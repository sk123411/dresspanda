package com.maestros.dresspanda.home.ui.category.subcategory

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.maestros.dresspanda.R
import com.maestros.dresspanda.databinding.FragmentSubCategoryBinding
import com.maestros.dresspanda.databinding.ToolbarMainBinding
import com.maestros.dresspanda.home.ui.category.CateogoryViewModel
import com.maestros.dresspanda.home.ui.category.adapter.model.SubCategoryResponse
import com.maestros.dresspanda.home.ui.home.HomeAdapter
import com.maestros.dresspanda.util.Constant
import dagger.hilt.android.AndroidEntryPoint

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"




@AndroidEntryPoint
class SubCategoryFragment : Fragment() {


    private val categoryViewModel:CateogoryViewModel by viewModels()



    lateinit var binding:FragmentSubCategoryBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentSubCategoryBinding.inflate(inflater)
        // Inflate the layout for this fragment
        var id:String? = "4"
        if ( arguments?.getString("id")!=null){
            id = arguments?.getString("id")
        }
        categoryViewModel.getSubCategoriesByCategory(id)


        categoryViewModel.categoryEvent.observe(viewLifecycleOwner, Observer {
            when(it) {
                is CateogoryViewModel.CategoryEvent.SubCategorySuccess -> {
                    activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.GONE

                    binding.subCategoryList.apply {
                        layoutManager = GridLayoutManager(context, 2)
                        adapter = HomeAdapter<SubCategoryResponse>(
                            it.resultText,
                            R.layout.brand_item_list
                        )

                    }
                }
                is CateogoryViewModel.CategoryEvent.Loading -> {
                    activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.VISIBLE

                }
                is CateogoryViewModel.CategoryEvent.Failure -> {

                    activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.GONE
                    Constant.showSnackBar(requireView(),it.errorText,"Ok",object:View.OnClickListener{
                        override fun onClick(v: View?) {
                        }

                    }).show()
                }
            }

        })









        return binding.root


    }



    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment SubCategoryFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            SubCategoryFragment()
                .apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}