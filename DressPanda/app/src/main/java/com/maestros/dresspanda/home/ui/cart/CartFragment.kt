package com.maestros.dresspanda.home.ui.cart

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer

import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.androidnetworking.model.Progress
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.maestros.dresspanda.R
import com.maestros.dresspanda.databinding.FragmentCartBinding
import com.maestros.dresspanda.databinding.SelectAddressLytBinding
import com.maestros.dresspanda.home.HomeActivity
import com.maestros.dresspanda.home.ui.address.AddressViewModel
import com.maestros.dresspanda.home.ui.cart.address.SelectAddressAdapter
import com.maestros.dresspanda.signup.model.Register
import com.maestros.dresspanda.util.Constant
import com.maestros.dresspanda.util.Resource

import dagger.hilt.android.AndroidEntryPoint
import io.paperdb.Paper
import kotlinx.coroutines.launch

@AndroidEntryPoint
class CartFragment : Fragment() {


    lateinit var mainbinding: FragmentCartBinding

    private val addressViewModel: AddressViewModel by viewModels()
    private val cartViewModel: CartFragmentViewModel by viewModels()

    var userID:String?=""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mainbinding = FragmentCartBinding.inflate(inflater)
        val register: Register = Paper.book().read<Register>(Constant.USER_DETAILS)
        userID = register.id
        cartViewModel.getCartItems(userID)

        lifecycleScope.launch {

            cartViewModel.cartItems.observe(viewLifecycleOwner, Observer {


                when(it){

                    is CartFragmentViewModel.CartEvent.GetCartItemSuccess -> {
                        activity?.findViewById<ProgressBar>(R.id.progressBar)?.isVisible = false

                        Paper.init(context)
                        Paper.book().write(Constant.CART_MAP, it.cartItems)
                        mainbinding.totalItems.setText("Total Items: ${it.cartItems.qntity}")
                        mainbinding.totalAmount.setText("Rs ${it.cartItems.totalAmount}")
                        mainbinding.cartList.apply {


                            layoutManager = LinearLayoutManager(context)
                            adapter = CartAdapter(it.cartItems.product, cartViewModel,userID)
                            
                            //setBottomCart(it.cartItems.qntity)
                            Constant.setCartCount(HomeActivity.bottomNavigationView,it.cartItems.qntity)
                        }



                        mainbinding.continueButton.setOnClickListener {view->


                            Navigation.findNavController(view).navigate(R.id.selectAddressFragment)

                        }


                    }

                    is CartFragmentViewModel.CartEvent.UpdateCartItemSuccess -> {
                        activity?.findViewById<ProgressBar>(R.id.progressBar)?.isVisible = false
                        cartViewModel.getCartItems(userID)
                        Toast.makeText(context, "Cart updated", Toast.LENGTH_SHORT).show()
                        
                    }

                    is CartFragmentViewModel.CartEvent.DeleteCartItemSuccess -> {
                        activity?.findViewById<ProgressBar>(R.id.progressBar)?.isVisible = false
                        cartViewModel.getCartItems(userID)
                        Toast.makeText(context, "Cart item deleted", Toast.LENGTH_SHORT).show()
                    }

                    is CartFragmentViewModel.CartEvent.Failure -> {

                        activity?.findViewById<ProgressBar>(R.id.progressBar)?.isVisible = false
                        Constant.setCartCount(HomeActivity.bottomNavigationView,0.toString())

                    }

                    is CartFragmentViewModel.CartEvent.Loading -> {
                        activity?.findViewById<ProgressBar>(R.id.progressBar)?.isVisible = true
                    }
                    
                }


            })



        }






        return mainbinding.root
    }

    private fun setBottomCart(qntity: String) {
        
        val bottomNavigation = activity?.findViewById<BottomNavigationView>(R.id.bottomNavView)
        
        val badge = bottomNavigation?.getOrCreateBadge(R.id.bottom_cart)

        if(qntity.toInt()>0){
            badge?.isVisible = true
            badge?.number = qntity.toInt()

        }else {
            badge?.isVisible = false
        }
        
    }


    fun openAddressDialog() {

        val binding = SelectAddressLytBinding.inflate(layoutInflater)
        val bottomSheetDialog = BottomSheetDialog(requireActivity())
        bottomSheetDialog.setContentView(binding.root)
        bottomSheetDialog.show()
        addressViewModel.getAddress(userID)

        lifecycleScope.launch {

            addressViewModel.addressEvent.observe(viewLifecycleOwner, Observer {

                when (it) {

                    is AddressViewModel.AddressEvent.AddressFetched -> {

                        binding.addressList.apply {

                            layoutManager = LinearLayoutManager(context)
                            adapter = SelectAddressAdapter(it.address)

                        }

                    }
                }


            })
        }


    }

    override fun onResume() {
        super.onResume()

        HomeActivity.bottomNavigationView.menu.findItem(R.id.bottom_cart).setChecked(true)

    }


}