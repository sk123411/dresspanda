package com.maestros.dresspanda.module

import ShopImplementation
import android.content.Context
import androidx.room.Room
import com.facebook.CallbackManager
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.maestros.dresspanda.home.ui.home.repository.Home
import com.maestros.dresspanda.home.ui.home.repository.HomeAPI
import com.maestros.dresspanda.home.ui.home.repository.HomeImplementation
import com.maestros.dresspanda.data.home.HomeBrandsDao
import com.maestros.dresspanda.data.AppDatabase
import com.maestros.dresspanda.data.address.AddressDAO
import com.maestros.dresspanda.data.product.ProductDAO
import com.maestros.dresspanda.data.wishlist.ProductWishlistDao
import com.maestros.dresspanda.data.shop.ShopsDao
import com.maestros.dresspanda.data.wishlist.ShopWishlistDao
import com.maestros.dresspanda.home.profile.repository.Profile
import com.maestros.dresspanda.home.profile.repository.ProfileAPI
import com.maestros.dresspanda.home.profile.repository.ProfileImplementation
import com.maestros.dresspanda.home.ui.address.repository.Address
import com.maestros.dresspanda.home.ui.address.repository.AddressAPI
import com.maestros.dresspanda.home.ui.address.repository.AddressImplementation
import com.maestros.dresspanda.home.ui.cart.repository.Cart
import com.maestros.dresspanda.home.ui.cart.repository.CartAPI
import com.maestros.dresspanda.home.ui.cart.repository.CartImplementation
import com.maestros.dresspanda.home.ui.message.repository.Chat
import com.maestros.dresspanda.home.ui.message.repository.ChatAPI
import com.maestros.dresspanda.home.ui.message.repository.ChatImplementation
import com.maestros.dresspanda.home.ui.newsfeed.repository.News
import com.maestros.dresspanda.home.ui.newsfeed.repository.NewsAPI
import com.maestros.dresspanda.home.ui.newsfeed.repository.NewsImplementation
import com.maestros.dresspanda.home.ui.orders.repository.Orders
import com.maestros.dresspanda.home.ui.orders.repository.OrdersAPI
import com.maestros.dresspanda.home.ui.orders.repository.OrdersImplementation
import com.maestros.dresspanda.home.ui.product.repository.*
import com.maestros.dresspanda.home.ui.shop.repository.ShopAPI
import com.maestros.dresspanda.login.repository.*
import com.maestros.dresspanda.paper.PaperImplementation
import com.maestros.dresspanda.seller.repository.Seller
import com.maestros.dresspanda.seller.repository.SellerAPI
import com.maestros.dresspanda.seller.repository.SellerImplementation
import com.maestros.dresspanda.signup.repository.SignUpAPI
import com.maestros.dresspanda.signup.repository.SignUP
import com.maestros.dresspanda.signup.repository.SignUPImplementation
import com.maestros.dresspanda.util.DispatcherProvider
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import dmax.dialog.SpotsDialog
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import retrofit2.CallAdapter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class AppModule  {

    val BASE_URL = "https://ruparnatechnology.com/"


    @Singleton
    @Provides
    fun provideGoogleSignInOptions(@ApplicationContext context:Context):GoogleSignInOptions {
      return GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()
    }



    @Singleton
    @Provides
    fun provideGoogleSignInClient(@ApplicationContext context:Context):GoogleSignInClient {
        return  GoogleSignIn.getClient(context, provideGoogleSignInOptions(context));

    }


    @Singleton
    @Provides
    fun provideFacebookCallback():CallbackManager {
        return  CallbackManager.Factory.create();

    }
    @Singleton
    @Provides
    fun provideContext(@ApplicationContext context: Context): Context {
        return context;

    }



    @Singleton
    @Provides
    fun provideDialog(@ApplicationContext context: Context): SpotsDialog.Builder? {
        return SpotsDialog.Builder().setContext(context);

    }

    @Singleton
    @Provides
    fun provideSignUPApi(): SignUpAPI = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(SignUpAPI::class.java)


    @Singleton
    @Provides
    fun provideSignUPRepository(api: SignUpAPI): SignUP = SignUPImplementation(api)




    @Singleton
    @Provides
    fun provideProfileApi(): ProfileAPI = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(ProfileAPI::class.java)



    @Singleton
    @Provides
    fun provideChatApi(): ChatAPI = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(ChatAPI::class.java)



    @Singleton
    @Provides
    fun provideProfileRepository(api: ProfileAPI): Profile = ProfileImplementation(api)



    @Singleton
    @Provides
    fun provideChatRepository(chatAPI: ChatAPI): Chat = ChatImplementation(chatAPI)






    @Singleton
    @Provides
    fun provideLoginApi(): LoginAPI = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(LoginAPI::class.java)


    @Singleton
    @Provides
    fun provideHomeApi(): HomeAPI = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(HomeAPI::class.java)


    @Singleton
    @Provides
    fun provideProductApi(): ProductAPI = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(ProductAPI::class.java)




    @Singleton
    @Provides
    fun provideCategoryApi(): CategoryAPI = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(CategoryAPI::class.java)



    @Singleton
    @Provides
    fun provideSellerApi(): SellerAPI = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(SellerAPI::class.java)



    @Singleton
    @Provides
    fun provideShopApi(): ShopAPI = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(ShopAPI::class.java)







    @Singleton
    @Provides
    fun provideSellerRepository(api: SellerAPI): Seller = SellerImplementation(api)



    @Singleton
    @Provides
    fun provideCategoryRepository(api: CategoryAPI): Category = CategoryImplementation(api)



    @Singleton
    @Provides
    fun provideProductRepository(api: ProductAPI): Product = ProductImplementation(api)








    @Singleton
    @Provides
    fun provideLoginRepository(api: LoginAPI): Login = LoginImplementation(api)


    @Singleton
    @Provides
    fun provideNewsAPI(): NewsAPI = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(NewsAPI::class.java)





    @Singleton
    @Provides
    fun provideNewsRepository(api: NewsAPI): News = NewsImplementation(api)


    @Singleton
    @Provides
    fun provideCartApi(): CartAPI = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(CartAPI::class.java)




    @Singleton
    @Provides
    fun provideCartRepository(api: CartAPI): Cart = CartImplementation(api)




    @Singleton
    @Provides
    fun provideAddressAPI(): AddressAPI = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(AddressAPI::class.java)





    @Singleton
    @Provides
    fun provideAddressRepository(api: AddressAPI): Address = AddressImplementation(api)



    @Singleton
    @Provides
    fun provideOrdersAPI(): OrdersAPI = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(OrdersAPI::class.java)





    @Singleton
    @Provides
    fun provideOrderRepository(api: OrdersAPI): Orders = OrdersImplementation(api)



    @Singleton
    @Provides
    fun providePaperDB(
        @ApplicationContext applicationContext: Context):com.maestros.dresspanda.paper.Paper
    = PaperImplementation(applicationContext)


    @Provides
    @Singleton
    fun provideAppDatabase(@ApplicationContext appContext: Context): AppDatabase {
        return Room.databaseBuilder(
            appContext,
            AppDatabase::class.java,
            "dress_panda.db"
        ).build()
    }




    @Singleton
    @Provides
    fun provideProductDao(appDatabase: AppDatabase): ProductDAO {
        return appDatabase.productDao()
    }
    @Singleton
    @Provides
    fun provideHomeDao(appDatabase: AppDatabase): HomeBrandsDao {
        return appDatabase.homeBrands()
    }

    @Singleton
    @Provides
    fun provideShopDao(appDatabase: AppDatabase): ShopsDao {
        return appDatabase.shops()
    }

    @Singleton
    @Provides
    fun provideWishlistDao(appDatabase: AppDatabase): ProductWishlistDao {
        return appDatabase.productWishlistDao()
    }

    @Singleton
    @Provides
    fun provideWishlistShopDao(appDatabase: AppDatabase): ShopWishlistDao {
        return appDatabase.wishlistShops()
    }


    @Singleton
    @Provides
    fun provideAddressDao(appDatabase: AppDatabase): AddressDAO {
        return appDatabase.addressDAO()
    }


    @Singleton
    @Provides
    fun provideHomeRepository(api: HomeAPI, homeBrandsDao: HomeBrandsDao): Home =
        HomeImplementation(api,homeBrandsDao)


    @Singleton
    @Provides
    fun provideShopRepository(api: ShopAPI): com.maestros.dresspanda.home.ui.shop.repository.Shop =
        ShopImplementation(api)


    @Singleton
    @Provides
    fun provideDispatchers(): DispatcherProvider = object : DispatcherProvider {
        override val main: CoroutineDispatcher
            get() = Dispatchers.Main
        override val io: CoroutineDispatcher
            get() = Dispatchers.IO
        override val default: CoroutineDispatcher
            get() = Dispatchers.Default
        override val unconfined: CoroutineDispatcher
            get() = Dispatchers.Unconfined
    }


}



