package com.maestros.dresspanda.home.ui.orders.repository

import com.maestros.dresspanda.home.ui.brand.BaseResponse
import com.maestros.dresspanda.home.ui.notification.Notification
import com.maestros.dresspanda.home.ui.orders.repository.model.CheckoutResponse
import com.maestros.dresspanda.home.ui.orders.repository.model.OrderResponse
import retrofit2.Response
import retrofit2.http.*

interface OrdersAPI {

    @POST("dresspanda/api/process.php")
    @FormUrlEncoded
    suspend fun addOrder(@Query("action") action:String?,@FieldMap map:HashMap<String,String>):Response<CheckoutResponse>


    @POST("dresspanda/api/process.php")
    @FormUrlEncoded
    suspend fun showOrders(@Query("action") action:String?, @FieldMap map:HashMap<String,String>):Response<OrderResponse>


    @POST("dresspanda/api/process.php")
    @FormUrlEncoded
    suspend fun addNotification(@Query("action") banner:String?,@FieldMap map:HashMap<String,String>): Response<Notification>


    @POST("dresspanda/api/process.php")
    @FormUrlEncoded
    suspend fun getNotification(@Query("action") banner:String?,@FieldMap map:HashMap<String,String>): Response<BaseResponse<Notification>>







}