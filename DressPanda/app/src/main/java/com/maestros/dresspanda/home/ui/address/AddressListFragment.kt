package com.maestros.dresspanda.home.ui.address

import android.app.Activity
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.viewModels
import androidx.lifecycle.LifecycleCoroutineScope
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.maestros.dresspanda.R
import com.maestros.dresspanda.databinding.AddAddressLayoutBinding
import com.maestros.dresspanda.databinding.FragmentAddressListBinding
import com.maestros.dresspanda.home.ui.address.repository.allAddressModel.Data
import com.maestros.dresspanda.home.ui.home.HomeAdapter
import com.maestros.dresspanda.home.ui.wishlist.adapter.AddressAdapter
import com.maestros.dresspanda.signup.model.Register
import com.maestros.dresspanda.util.Constant
import com.maestros.dresspanda.util.TextChangeListernerImple
import dagger.hilt.android.AndroidEntryPoint
import io.paperdb.Paper
import kotlinx.coroutines.launch


@AndroidEntryPoint
class AddressListFragment : Fragment() {


    lateinit var bindingAddress: FragmentAddressListBinding
    val addressViewModel: AddressViewModel by viewModels()
    var userId:String?=""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        bindingAddress = FragmentAddressListBinding.inflate(inflater)
        Paper.init(context)
        val register:Register = Paper.book().read(Constant.USER_DETAILS)
        userId = register.id

        addressViewModel.getAddress(user_id = userId)

        bindingAddress.addAddressButton.setOnClickListener {

            openAddressDialog(data = Data(
                name = "",
                address ="",
                buldingNo = "",
                city = "",
                state = "",
                pincode = "binding.pincodeEdit.text.toString().trim()",
                landmark = "",
                streetNo ="",
                userId = "",
                id = ""

            ),isUpdate = false,user_id = userId,activity = requireActivity(),addressViewModel = addressViewModel,lifecycleScope = lifecycleScope)
        }

        lifecycleScope.launch {

            addressViewModel.addressEvent.observe(viewLifecycleOwner, Observer {
                when(it){

                    is AddressViewModel.AddressEvent.AddressFetched -> {
                        activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.GONE

                        bindingAddress.addressList.layoutManager = LinearLayoutManager(context)
                        bindingAddress.addressList.adapter = AddressAdapter(it.address,addressViewModel,
                        lifecycleScope,this@AddressListFragment)

                       }

                    is AddressViewModel.AddressEvent.AddressAdded -> {
                        activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.GONE

                        Toast.makeText(requireActivity(), "address added", Toast.LENGTH_SHORT).show()
                        addressViewModel.getAddress(userId)

                    }
                    is AddressViewModel.AddressEvent.AddressDeleted-> {
                        activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.GONE

                        Toast.makeText(requireActivity(), "address cleared", Toast.LENGTH_SHORT).show()
                        addressViewModel.getAddress(userId)

                    } is AddressViewModel.AddressEvent.AddressUpdated-> {
                    activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.GONE

                    Toast.makeText(requireActivity(), "address updated", Toast.LENGTH_SHORT).show()
                    addressViewModel.getAddress(userId)

                }

                    is AddressViewModel.AddressEvent.Loading -> {
                        activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.VISIBLE

                    }
                }


            })

        }

        return bindingAddress.root
    }
        companion object {

            fun openAddressDialog(data: Data, isUpdate: Boolean,user_id:String?,activity:FragmentActivity,addressViewModel:AddressViewModel,lifecycleScope:LifecycleCoroutineScope) {

                val bottomSheetDialog = BottomSheetDialog(activity)
                val binding = AddAddressLayoutBinding.inflate(activity.layoutInflater)
                bottomSheetDialog.setContentView(binding.root)
                bottomSheetDialog.show()

                if (isUpdate) {

                    binding.nameEdit.setText(data.name)
                    binding.LandMartEdit.setText(data.landmark)
                    binding.buildingNumberEdit.setText(data.buldingNo)
                    binding.cityEdit.setText(data.city)
                    binding.stateEdit.setText(data.state)
                    binding.pincodeEdit.setText(data.pincode)
                    binding.streetNumberEdit.setText(data.streetNo)
                    binding.addressEdit.setText(data.address)
                    binding.addButton.setText("Update")
                }

                binding.nameEdit.addTextChangedListener(TextChangeListernerImple(object :
                    TextChangeListernerImple.TextChangeListerner {
                    override fun onTextChange(text: String?) {
                        addressViewModel.checkInputEmptyFields(
                            binding.nameEdit,
                            binding.buildingNumberEdit,
                            binding.streetNumberEdit,
                            binding.addressEdit,
                            binding.LandMartEdit,
                            binding.pincodeEdit,
                            binding.cityEdit,
                            binding.stateEdit
                        )


                    }

                }))


                binding.buildingNumberEdit.addTextChangedListener(TextChangeListernerImple(object :
                    TextChangeListernerImple.TextChangeListerner {
                    override fun onTextChange(text: String?) {
                        addressViewModel.checkInputEmptyFields(
                            binding.nameEdit,
                            binding.buildingNumberEdit,
                            binding.streetNumberEdit,
                            binding.addressEdit,
                            binding.LandMartEdit,
                            binding.pincodeEdit,
                            binding.cityEdit,
                            binding.stateEdit
                        )


                    }

                }))


                binding.streetNumberEdit.addTextChangedListener(TextChangeListernerImple(object :
                    TextChangeListernerImple.TextChangeListerner {
                    override fun onTextChange(text: String?) {
                        addressViewModel.checkInputEmptyFields(
                            binding.nameEdit,
                            binding.buildingNumberEdit,
                            binding.streetNumberEdit,
                            binding.addressEdit,
                            binding.LandMartEdit,
                            binding.pincodeEdit,
                            binding.cityEdit,
                            binding.stateEdit
                        )


                    }

                }))

                binding.addressEdit.addTextChangedListener(TextChangeListernerImple(object :
                    TextChangeListernerImple.TextChangeListerner {
                    override fun onTextChange(text: String?) {
                        addressViewModel.checkInputEmptyFields(
                            binding.nameEdit,
                            binding.buildingNumberEdit,
                            binding.streetNumberEdit,
                            binding.addressEdit,
                            binding.LandMartEdit,
                            binding.pincodeEdit,
                            binding.cityEdit,
                            binding.stateEdit
                        )


                    }

                }))

                binding.LandMartEdit.addTextChangedListener(TextChangeListernerImple(object :
                    TextChangeListernerImple.TextChangeListerner {
                    override fun onTextChange(text: String?) {
                        addressViewModel.checkInputEmptyFields(
                            binding.nameEdit,
                            binding.buildingNumberEdit,
                            binding.streetNumberEdit,
                            binding.addressEdit,
                            binding.LandMartEdit,
                            binding.pincodeEdit,
                            binding.cityEdit,
                            binding.stateEdit
                        )


                    }

                }))

                binding.pincodeEdit.addTextChangedListener(TextChangeListernerImple(object :
                    TextChangeListernerImple.TextChangeListerner {
                    override fun onTextChange(text: String?) {
                        addressViewModel.checkInputEmptyFields(
                            binding.nameEdit,
                            binding.buildingNumberEdit,
                            binding.streetNumberEdit,
                            binding.addressEdit,
                            binding.LandMartEdit,
                            binding.pincodeEdit,
                            binding.cityEdit,
                            binding.stateEdit
                        )


                    }

                }))

                binding.cityEdit.addTextChangedListener(TextChangeListernerImple(object :
                    TextChangeListernerImple.TextChangeListerner {
                    override fun onTextChange(text: String?) {
                        addressViewModel.checkInputEmptyFields(
                            binding.nameEdit,
                            binding.buildingNumberEdit,
                            binding.streetNumberEdit,
                            binding.addressEdit,
                            binding.LandMartEdit,
                            binding.pincodeEdit,
                            binding.cityEdit,
                            binding.stateEdit
                        )

                    }

                }))



                binding.stateEdit.addTextChangedListener(TextChangeListernerImple(object :
                    TextChangeListernerImple.TextChangeListerner {
                    override fun onTextChange(text: String?) {
                        addressViewModel.checkInputEmptyFields(
                            binding.nameEdit,
                            binding.buildingNumberEdit,
                            binding.streetNumberEdit,
                            binding.addressEdit,
                            binding.LandMartEdit,
                            binding.pincodeEdit,
                            binding.cityEdit,
                            binding.stateEdit
                        )

                    }

                }))







                lifecycleScope.launch {

                    addressViewModel.emptyFieldsEvent.observe(activity, Observer {

                        when (it) {

                            false -> {
                                binding.addButton.isEnabled = true

                                if (isUpdate) {


                                    binding.addButton.setOnClickListener {


                                        addressViewModel.updateAddress(
                                            addressID = data.id,
                                            address = Data(
                                                name = binding.nameEdit.text.toString().trim(),
                                                address = binding.addressEdit.text.toString()
                                                    .trim(),
                                                buldingNo = binding.buildingNumberEdit.text.toString()
                                                    .trim(),
                                                city = binding.cityEdit.text.toString().trim(),
                                                state = binding.stateEdit.text.toString().trim(),
                                                pincode = binding.pincodeEdit.text.toString()
                                                    .trim(),
                                                landmark = binding.LandMartEdit.text.toString()
                                                    .trim(),
                                                streetNo = binding.streetNumberEdit.text.toString()
                                                    .trim(),
                                                userId = user_id!!,
                                                id = user_id!!

                                            )

                                        )

                                        bottomSheetDialog.dismiss()
                                    }

                                } else {
                                    binding.addButton.setOnClickListener {


                                        addressViewModel.insertAddress(
                                            Data(
                                                name = binding.nameEdit.text.toString().trim(),
                                                address = binding.addressEdit.text.toString()
                                                    .trim(),
                                                buldingNo = binding.buildingNumberEdit.text.toString()
                                                    .trim(),
                                                city = binding.cityEdit.text.toString().trim(),
                                                state = binding.stateEdit.text.toString().trim(),
                                                pincode = binding.pincodeEdit.text.toString()
                                                    .trim(),
                                                landmark = binding.LandMartEdit.text.toString()
                                                    .trim(),
                                                streetNo = binding.streetNumberEdit.text.toString()
                                                    .trim(),
                                                userId = user_id!!,
                                                id = user_id!!

                                            )
                                            , user_id = user_id
                                        )

                                        bottomSheetDialog.dismiss()
                                    }
                                }


                            }
                            true -> binding.addButton.isEnabled = false

                        }


                    })

                }


            }
        }

}