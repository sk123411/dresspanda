package com.maestros.dresspanda.home.ui.message


import com.google.gson.annotations.SerializedName

data class MessagesList(
    @SerializedName("dates")
    val dates: String,
    @SerializedName("file")
    val `file`: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("message")
    val message: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("path")
    val path: String,
    @SerializedName("read_message")
    val readMessage: String,
    @SerializedName("reciver_id")
    val reciverId: String,
    @SerializedName("sender_id")
    val senderId: String,
    @SerializedName("strtotime")
    val strtotime: String
)