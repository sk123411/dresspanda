package com.maestros.dresspanda.home.ui.gift

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.maestros.dresspanda.R
import com.maestros.dresspanda.databinding.FragmentGiftBinding
import com.maestros.dresspanda.home.ui.home.HomeAdapter

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [GiftFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class GiftFragment : Fragment() {
    private lateinit var giftViewModel: GiftViewModel
    lateinit var binding: FragmentGiftBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        giftViewModel = ViewModelProvider(this).get(GiftViewModel::class.java)
        binding = FragmentGiftBinding.inflate(inflater)

        // Inflate the layout for this fragment
        giftViewModel.generateTempList()

        giftViewModel.giftlist.observe(viewLifecycleOwner, Observer {

            binding.giftList.apply {

                layoutManager = GridLayoutManager(context, 2)
                adapter = HomeAdapter<Gift>(
                    it,
                    R.layout.gift_item
                )
            }
        })




        return binding.root
    }














}