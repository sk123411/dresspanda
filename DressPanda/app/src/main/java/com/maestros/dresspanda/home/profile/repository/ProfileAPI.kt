package com.maestros.dresspanda.home.profile.repository

import com.maestros.dresspanda.signup.model.Register
import com.maestros.dresspanda.util.Resource
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*

interface ProfileAPI {


    @POST("dresspanda/api/process.php")
    @FormUrlEncoded
    suspend fun changePassword(@Query("action") action:String?, @FieldMap data:HashMap<String,String>): Response<Register>



    @POST("dresspanda/api/process.php")
    @FormUrlEncoded
    suspend fun getProfile(@Query("action") action:String?, @FieldMap data:HashMap<String,String>): Response<Register>



    @POST("dresspanda/api/process.php")
    @FormUrlEncoded
    suspend fun changeName(@Query("action") updateProfile:String?, @FieldMap name:HashMap<String,String>): Response<Register>

    @POST("dresspanda/api/process.php")
    @FormUrlEncoded
    suspend fun changeEmail(@Query("action") updateProfile:String?, @FieldMap name:HashMap<String,String>): Response<Register>

    @POST("dresspanda/api/process.php")
    @FormUrlEncoded
    suspend fun changeDOB(@Query("action") updateProfile:String?,@FieldMap  name:HashMap<String,String>): Response<Register>


    @POST("dresspanda/api/process.php")
    @FormUrlEncoded
    suspend fun changeNumber(@Query("action") updateProfile:String?, @FieldMap name:HashMap<String,String>): Response<Register>


    @Multipart
    @POST("dresspanda/api/process.php")
    suspend fun changeImage(@Query("action") updateProfile:String?,
                            @Part image:MultipartBody.Part?,
                            @Part("user_id") userId: RequestBody?): Response<Register>




}