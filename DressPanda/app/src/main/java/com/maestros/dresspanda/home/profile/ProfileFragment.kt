package com.maestros.dresspanda.home.profile

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.maestros.dresspanda.R
import com.maestros.dresspanda.databinding.FieldChangeEditBinding
import com.maestros.dresspanda.databinding.FragmentProfileBinding
import com.maestros.dresspanda.login.LoginViewModel
import com.maestros.dresspanda.util.Constant
import com.squareup.picasso.Picasso
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.io.File


@AndroidEntryPoint
class ProfileFragment : Fragment() {

    private var imageEmpty: Boolean = true
    lateinit var binding:FragmentProfileBinding
    lateinit var profile:Profile

    val profileViewModel:ProfileViewModel by viewModels()
    val loginViewModel:LoginViewModel by viewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentProfileBinding.inflate(inflater)
        loginViewModel.getUserDetails()



        lifecycleScope.launchWhenStarted {


            loginViewModel.login.collect { user -> when(user){

                is LoginViewModel.LoginEvent.UserDetails -> {

                    profileViewModel.getProfile(user.userDetails.id)


                    binding.nameEdit.setOnClickListener {
                        openEditDialog(binding.nameEdit,checkInput(binding.nameEdit, "Name"), user.userDetails.id,Profile.NAME)
                    }
                    binding.emailEdit.setOnClickListener {
                        openEditDialog(binding.emailEdit,checkInput(binding.emailEdit, "Email"), user.userDetails.id, Profile.EMAIL)
                    }
                    binding.phoneEdit.setOnClickListener {
                        openEditDialog(binding.phoneEdit,checkInput(binding.phoneEdit, "Number"), user.userDetails.id, Profile.NUMBER)

                    }
                    binding.dobEdit.setOnClickListener {
                        openEditDialog(binding.dobEdit,checkInput(binding.dobEdit, "DOB"), user.userDetails.id, Profile.DOB)

                    }

                    binding.genderEdit.setOnClickListener {
                        openEditDialog(binding.genderEdit,checkInput(binding.genderEdit, "gender"), user.userDetails.id, Profile.GENDER)

                    }


                    profileViewModel.imageFile.observe(viewLifecycleOwner, Observer {

                        Log.d("image", "here")

                        profileViewModel.changeImage(it, user.userDetails.id)



                    })



                }



            }
            }



            profileViewModel.profileEvent.collect {

                when(it){

                    is ProfileViewModel.ProfileEvent.SuccessProfileUpdate -> {
                        Toast.makeText(context, "Profile updated", Toast.LENGTH_SHORT).show()
                        activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.GONE

                    }


                    is ProfileViewModel.ProfileEvent.GetProfile ->{

                        activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.GONE

                    }

                    is ProfileViewModel.ProfileEvent.Loading -> {

                        activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.VISIBLE

                    }

                }

            }


        }

        lifecycleScope.launch {


            profileViewModel.register.collect {

                binding.nameEdit.setText(checkEmptyString("Name", it.name))
                binding.dobEdit.setText(checkEmptyString("DOB", it.DOB))
                binding.genderEdit.setText(checkEmptyString("Gender", it.gender))
                binding.phoneEdit.setText(checkEmptyString("Number", it.phone_number))
                binding.emailEdit.setText(checkEmptyString("Email", it.email))

                if (!it.image.equals("")){

                    Picasso.get().load(Constant.BASE_URL+it.image).into(binding.profileImage)

                }

            }




        }


        binding.profileImage.setOnClickListener {

            ImagePicker.with(this)
                .galleryOnly()
                .compress(1024)
                .start()


        }



        return binding.root
    }


    fun checkEmptyString(tag:String?, data:String?):String{
        var temptag:String?

        if (data.equals(""))
            temptag = tag
        else
           temptag = data

        return temptag!!

    }



    fun checkInput(editText: EditText, defaultHint:String?):String?{
        var hint:String?
        if (editText.text.toString().trim().length> 0){

            hint = editText.text.toString()
        }else {

            hint = defaultHint
        }
        return hint
    }


    fun openEditDialog(selectedEditText: EditText?,hint:String?,userId:String?, dob:Profile){
        val bottomSheetDialog = BottomSheetDialog(requireActivity());
        val changePasswordBinding = FieldChangeEditBinding.inflate(layoutInflater)
        bottomSheetDialog.setContentView(changePasswordBinding.root)
        bottomSheetDialog.show()
        changePasswordBinding.editFiledText.setHint(hint!!)
        changePasswordBinding.setFilledButton.setOnClickListener {

            when(dob){

               Profile.NAME -> {


                   profileViewModel.changeName(changePasswordBinding.editFiledText.text.toString().trim(),userId)
                   selectedEditText?.setText(changePasswordBinding.editFiledText.text.toString())
                   bottomSheetDialog.dismiss()

               }


                Profile.EMAIL -> {

                    profileViewModel.changeEmail(changePasswordBinding.editFiledText.text.toString().trim(),userId)
                    selectedEditText?.setText(changePasswordBinding.editFiledText.text.toString())
                    bottomSheetDialog.dismiss()

                }

                Profile.NUMBER -> {

                    profileViewModel.changeNumber(changePasswordBinding.editFiledText.text.toString().trim(),userId)
                    selectedEditText?.setText(changePasswordBinding.editFiledText.text.toString())
                    bottomSheetDialog.dismiss()

                }

                Profile.DOB -> {

                    profileViewModel.changeDOB(changePasswordBinding.editFiledText.text.toString().trim(),userId)
                    selectedEditText?.setText(changePasswordBinding.editFiledText.text.toString())
                    bottomSheetDialog.dismiss()

                }

                Profile.GENDER -> {

                    profileViewModel.changeGender(changePasswordBinding.editFiledText.text.toString().trim(),userId)
                    selectedEditText?.setText(changePasswordBinding.editFiledText.text.toString())
                    bottomSheetDialog.dismiss()

                }


            }

        }


    }


    enum class Profile{
        NAME,
        EMAIL,
        NUMBER,
        DOB,
        GENDER
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            val fileUri = data?.data
            binding.profileImage.setImageURI(fileUri)
            //You can get File object from intent
            val file: File = ImagePicker.getFile(data)!!

            //You can also get File Path from intent
            profileViewModel.sendImageFile(file)


            val filePath: String = ImagePicker.getFilePath(data)!!
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(context, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(context, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }

}