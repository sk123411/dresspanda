package com.maestros.dresspanda.home.ui.newsfeed.adapter.model.postlist

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class BasePost<T>(
    @SerializedName("status")
    @Expose
    val result: Boolean,
    @SerializedName("data")
    @Expose
    val postList: List<T>

    )