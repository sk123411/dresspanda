package com.maestros.dresspanda.home.profile.repository
import com.maestros.dresspanda.signup.model.Register
import com.maestros.dresspanda.util.Resource
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*

interface Profile {

    suspend fun changePassword(action:String?, data:HashMap<String,String>):Resource<Register>


    suspend fun changeName(updateProfile:String?, data:HashMap<String,String>):Resource<Register>


    suspend fun changeEmail(updateProfile:String?, data:HashMap<String,String>):Resource<Register>


    suspend fun changeDOB(updateProfile:String?, data:HashMap<String,String>):Resource<Register>


    suspend fun changeNumber(updateProfile:String?, data:HashMap<String,String>):Resource<Register>


    suspend fun getProfile(action:String?,data:HashMap<String,String>): Resource<Register>

    suspend fun changeImage(action:String?,image:MultipartBody.Part?,
                            userId:RequestBody?): Resource<Register>



}