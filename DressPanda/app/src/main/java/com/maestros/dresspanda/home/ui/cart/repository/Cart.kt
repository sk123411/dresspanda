package com.maestros.dresspanda.home.ui.cart.repository

import com.maestros.dresspanda.home.ui.brand.BaseResponse
import com.maestros.dresspanda.home.ui.product.model.productdetail.ProductResponse
import com.maestros.dresspanda.util.Resource
import org.json.JSONObject
import retrofit2.Response
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import retrofit2.http.Query

interface Cart {


    suspend fun showCartItems(show_cart:String?, map:HashMap<String,String>): Resource<CartX>


    suspend fun updateCartItem(update_cart:String?,  map:HashMap<String,String>): Resource<CartX>


    suspend fun deleteCartItem(delete_cart:String?,map:HashMap<String,String>): Resource<JSONObject>


}