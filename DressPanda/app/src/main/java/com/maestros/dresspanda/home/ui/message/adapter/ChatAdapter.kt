package com.maestros.dresspanda.home.ui.message.adapter

import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import com.maestros.dresspanda.R
import com.maestros.dresspanda.databinding.ChatItemMessageBinding
import com.maestros.dresspanda.home.ui.message.MessagesList
import com.maestros.dresspanda.signup.model.Register
import com.maestros.dresspanda.util.Constant
import com.squareup.picasso.Picasso
import io.paperdb.Paper

class ChatAdapter(val messagesList: List<MessagesList>) : RecyclerView.Adapter<ChatAdapter.ViewHolder>() {

    lateinit var binding:ChatItemMessageBinding

    class ViewHolder(view:View):RecyclerView.ViewHolder(view) {

        val binding = ChatItemMessageBinding.bind(view)

        fun bindData(messagesList: MessagesList) {

            binding.chatMessage.setText(messagesList.message)
            Paper.init(itemView.context)
            val registerUser = Paper.book().read<Register>(Constant.USER_DETAILS)
            Log.d("RAGGGGGGGGGG","::"+registerUser.id )

            if (messagesList.senderId.equals(registerUser.id)){

                binding.rootLayout.setBackgroundColor(itemView.resources.getColor(R.color.light_green))

            }else {

            }


            if (!messagesList.file.equals("")){
                binding.chatImage.visibility = View.VISIBLE
                Picasso.get().load(Constant.BASE_URL + messagesList.file).into(binding.chatImage)
            }
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        binding = ChatItemMessageBinding.inflate(LayoutInflater.from(parent.context))

        return ViewHolder(binding.root)
    }

    override fun getItemCount(): Int {
        return messagesList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(messagesList.get(position))
    }
}