package com.maestros.dresspanda.home.ui.address.repository.allAddressModel


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName("address")
    @Expose
    val address: String,
    @SerializedName("bulding_no")
    @Expose
    val buldingNo: String,
    @SerializedName("city")
    @Expose
    val city: String,
    @SerializedName("id")
    @Expose
    val id: String,
    @SerializedName("landmark")
    @Expose
    val landmark: String,
    @SerializedName("name")
    @Expose
    val name: String,
    @SerializedName("pincode")
    @Expose
    val pincode: String,
    @SerializedName("state")
    @Expose
    val state: String,
    @SerializedName("street_no")
    @Expose
    val streetNo: String,
    @SerializedName("user_id")
    @Expose
    val userId: String
)