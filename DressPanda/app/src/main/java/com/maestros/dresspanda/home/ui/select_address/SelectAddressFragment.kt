package com.maestros.dresspanda.home.ui.select_address

import android.os.Bundle
import android.transition.PatternPathMotion
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.maestros.dresspanda.R
import com.maestros.dresspanda.databinding.SelectAddressLytBinding
import com.maestros.dresspanda.home.ui.address.AddressListFragment
import com.maestros.dresspanda.home.ui.address.AddressViewModel
import com.maestros.dresspanda.home.ui.address.repository.allAddressModel.Data
import com.maestros.dresspanda.home.ui.cart.address.SelectAddressAdapter
import com.maestros.dresspanda.home.ui.cart.repository.CartX
import com.maestros.dresspanda.signup.model.Register
import com.maestros.dresspanda.util.Constant
import com.maestros.dresspanda.util.Resource
import dagger.hilt.android.AndroidEntryPoint
import io.paperdb.Paper
import kotlinx.coroutines.launch

@AndroidEntryPoint
class SelectAddressFragment : Fragment() {


    val addressViewModel:AddressViewModel by viewModels()
    lateinit var binding:SelectAddressLytBinding
    var userID:String?=""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Paper.init(context)
        val register:Register = Paper.book().read(Constant.USER_DETAILS)
        userID = register.id
        addressViewModel.getAddress(userID)
        binding = SelectAddressLytBinding.inflate(layoutInflater)
        val map:CartX = Paper.book().read<CartX>(Constant.CART_MAP)

        lifecycleScope.launch {

            addressViewModel.addressEvent.observe(viewLifecycleOwner, Observer {

                when(it){

                    is AddressViewModel.AddressEvent.AddressFetched -> {
                        activity?.findViewById<ProgressBar>(R.id.progressBar)?.isVisible = false


                        binding.totalItems.setText("Total Items ${map.qntity}")
                        binding.totalAmount.setText("Rs ${map.totalAmount}")

                        binding.addressList.apply {

                            layoutManager = LinearLayoutManager(context)
                            adapter = SelectAddressAdapter(it.address)

                        }

                    }

                    is AddressViewModel.AddressEvent.AddressAdded -> {
                        activity?.findViewById<ProgressBar>(R.id.progressBar)?.isVisible = false

                        addressViewModel.getAddress(userID)

                    }

                    is AddressViewModel.AddressEvent.Loading -> {
                        activity?.findViewById<ProgressBar>(R.id.progressBar)?.isVisible = true
                    }
                }


            })

        }


        binding.continueButton.setOnClickListener {
            val bundle = Bundle();
            bundle.putString("id", Constant.SelectedAddressID)
            Navigation.findNavController(it).navigate(R.id.paymentFragment,bundle)



        }


        binding.addAddressButton.setOnClickListener {

            AddressListFragment. openAddressDialog(data = Data(
                name = "",
                address ="",
                buldingNo = "",
                city = "",
                state = "",
                pincode = "",
                landmark = "",
                streetNo ="",
                userId = "",
                id = ""

            ),isUpdate = false,user_id = userID,activity = requireActivity(),addressViewModel = addressViewModel,lifecycleScope = lifecycleScope)




        }


        return binding.root
    }
}