package com.maestros.dresspanda.home.ui.notification


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Notification(
    @SerializedName("id")
    @Expose
    val id: String,
    @SerializedName("path")
    @Expose
    val path: String,
    @SerializedName("result")
    @Expose
    val result: String,
    @SerializedName("text")
    @Expose
    val text: String,
    @SerializedName("time")
    @Expose
    val time: String,
    @SerializedName("user_id")
    @Expose
    val userId: String
)