package com.maestros.dresspanda.signup.repository

import com.maestros.dresspanda.signup.model.Register
import com.maestros.dresspanda.util.Resource
import retrofit2.Response
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import retrofit2.http.Query

interface SignUP {
    suspend fun register(action: String?,data:Map<String,String>?): Resource<Register>

    suspend fun socialLogin(action: String?,  data:Map<String,String>?): Resource<Register>



}