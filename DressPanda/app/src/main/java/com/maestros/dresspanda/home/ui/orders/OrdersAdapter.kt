package com.maestros.dresspanda.home.ui.orders

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.maestros.dresspanda.R
import com.maestros.dresspanda.databinding.OrderItemLytBinding
import com.maestros.dresspanda.home.ui.orders.repository.model.CheckoutResponse
import com.maestros.dresspanda.home.ui.product.adapter.ProductAdapter

class OrdersAdapter(val list: List<CheckoutResponse>) :
    RecyclerView.Adapter<OrdersAdapter.MyViewholder>() {
    class MyViewholder(view: View) : RecyclerView.ViewHolder(view) {

        val orderItemLytBinding = OrderItemLytBinding.bind(view)
        fun bindData(data: CheckoutResponse) {

            val resources =  orderItemLytBinding.root.resources

            orderItemLytBinding.orderID.text = resources.getString(R.string.order_id) + data.orderId
            orderItemLytBinding.productID.text =  data.productId
            orderItemLytBinding.totalAmount.text = resources.getString(R.string.order_amount) + data.totalAmount.toString()



            orderItemLytBinding.productList.apply {
                layoutManager = GridLayoutManager(context,2)
                adapter = ProductAdapter(data.products)

            }

        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewholder {

        val binding = OrderItemLytBinding.inflate(LayoutInflater.from(parent.context))

        return MyViewholder(binding.root)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewholder, position: Int) {

        holder.bindData(list.get(position))
    }
}