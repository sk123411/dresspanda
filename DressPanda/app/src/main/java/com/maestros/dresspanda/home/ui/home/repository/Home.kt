package com.maestros.dresspanda.home.ui.home.repository

import com.maestros.dresspanda.home.ui.brand.BaseResponse
import com.maestros.dresspanda.home.ui.brand.HomeResponse
import com.maestros.dresspanda.home.ui.home.model.Slider
import com.maestros.dresspanda.home.ui.notification.Notification
import com.maestros.dresspanda.home.ui.product.model.productdetail.ProductResponse
import com.maestros.dresspanda.util.Resource
import retrofit2.Response
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import retrofit2.http.Query

interface Home {

    suspend fun getBrands(action:String?):Resource<List<HomeResponse>>
    suspend fun getProducts(action:String?):Resource<List<ProductResponse>>

    suspend fun getSliderDataHome(action:String?):Resource<List<Slider>>






}