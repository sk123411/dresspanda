package com.maestros.dresspanda.home.ui.message.repository

import com.maestros.dresspanda.home.ui.brand.BaseResponse
import com.maestros.dresspanda.home.ui.message.Message
import com.maestros.dresspanda.home.ui.message.MessagesList
import com.maestros.dresspanda.home.ui.message.model.BaseMessage
import retrofit2.Response
import retrofit2.http.FieldMap
import retrofit2.http.POST
import retrofit2.http.Query
import com.maestros.dresspanda.util.Resource
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.Part

interface Chat {

    suspend fun sendMessage(action: String?, data: HashMap<String, String>): Resource<Message>


    suspend fun showMessage(action: String?, data: HashMap<String, String>): Resource<BaseMessage<MessagesList>>

    suspend fun showAllMessages(
        action: String?,
        data: HashMap<String, String>
    ): Resource<BaseMessage<MessagesList>>

    suspend fun sendMessageWithImage(
        action: String?,
        image: MultipartBody.Part?,
        userID:RequestBody?,
        message: RequestBody?,
        senderID: RequestBody?,
        receiverID: RequestBody?):Resource<Message>

}