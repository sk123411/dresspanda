package com.maestros.dresspanda.home.ui.notification

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.FtsOptions
import com.maestros.dresspanda.R
import com.maestros.dresspanda.databinding.FragmentNotificationBinding
import com.maestros.dresspanda.home.ui.compaigns.CompaignViewModel
import com.maestros.dresspanda.home.ui.notification.adapter.NotificationAdapter
import com.maestros.dresspanda.home.ui.orders.OrderViewModel
import com.maestros.dresspanda.util.Constant
import dagger.hilt.android.AndroidEntryPoint
import java.util.function.BiConsumer

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"




@AndroidEntryPoint
class NotificationFragment : Fragment() {
    lateinit var viewModel: NotificationViewModel
    lateinit var binding:FragmentNotificationBinding

    val orderViewModel: OrderViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this).get(NotificationViewModel::class.java)
        binding = FragmentNotificationBinding.inflate(inflater)
        orderViewModel.showNotification(Constant.getUserID(context))


        orderViewModel.ordersEvent.observe(viewLifecycleOwner, Observer {

            when(it){


                is OrderViewModel.OrdersEvent.ShowNotification -> {

                    binding.notificationlist.apply {

                        layoutManager = LinearLayoutManager(context)
                        adapter = NotificationAdapter(it.resultText, R.layout.notificaiton_item)

                    }
                }

                is OrderViewModel.OrdersEvent.Failure -> {

                    Constant.showMessage("Server error occured, Please try later",context)

                }
            }


        })



        return binding.root
    }

}