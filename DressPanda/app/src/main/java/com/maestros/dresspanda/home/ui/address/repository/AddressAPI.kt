package com.maestros.dresspanda.home.ui.address.repository

import com.maestros.dresspanda.home.ui.address.AddressX
import com.maestros.dresspanda.home.ui.address.repository.allAddressModel.AllAddress
import com.maestros.dresspanda.home.ui.cart.repository.CartX
import org.json.JSONObject
import retrofit2.Response
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import retrofit2.http.Query

interface AddressAPI {

    @POST("dresspanda/api/process.php")
    @FormUrlEncoded
    suspend fun showAddress(@Query("action") show_address:String?, @FieldMap map:HashMap<String,String>): Response<AllAddress>

    @POST("dresspanda/api/process.php")
    @FormUrlEncoded
    suspend fun addAddress(@Query("action") addAddress:String?, @FieldMap map:HashMap<String,String>): Response<AddressX>



    @POST("dresspanda/api/process.php")
    @FormUrlEncoded
    suspend fun updateAddressItem(@Query("action") updateAddress:String?, @FieldMap map:HashMap<String,String>): Response<AddressX>



    @POST("dresspanda/api/process.php")
    @FormUrlEncoded
    suspend fun deleteAddressItem(@Query("action") deleteAddress:String?, @FieldMap map:HashMap<String,String>): Response<JSONObject>


}