package com.maestros.dresspanda.home.ui.notification

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.maestros.dresspanda.R

class NotificationViewModel : ViewModel() {

    val notification_ = MutableLiveData<List<Notification>>()

    val giftlist:LiveData<List<Notification>>
        get() = notification_


}