package com.maestros.dresspanda.login.repository

import android.util.Log
import com.maestros.dresspanda.home.ui.brand.BaseResponse
import com.maestros.dresspanda.home.ui.brand.HomeResponse
import com.maestros.dresspanda.home.ui.category.adapter.model.CategoryResponse
import com.maestros.dresspanda.home.ui.category.adapter.model.SubCategoryResponse
import com.maestros.dresspanda.login.model.ForgetPassword
import com.maestros.dresspanda.signup.model.Register
import com.maestros.dresspanda.util.Resource
import retrofit2.Response
import javax.inject.Inject


class CategoryImplementation @Inject constructor(val api: CategoryAPI): Category {
    override suspend fun getCategories(category: String?):Resource<BaseResponse<CategoryResponse>> {
        return try {
            val response = api.getCategories(category)
            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())
                Resource.Success(result)

            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }

    override suspend fun getSubCategoriesByCategory(
        category: String?,
        map: HashMap<String, String>
    ):Resource<BaseResponse<SubCategoryResponse>>{
        return try {
            val response = api.getSubCategoriesByCategory(category,map)
            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())
                Resource.Success(result)

            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }    }


}