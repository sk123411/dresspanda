package com.maestros.dresspanda.home.ui.notification.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.maestros.dresspanda.databinding.*
import com.maestros.dresspanda.home.ui.notification.Notification

class NotificationAdapter(val list: List<Notification>, val layout: Int) :
    RecyclerView.Adapter<NotificationAdapter.MyViewHolder>() {
    lateinit var binding: NotificaitonItemBinding

    class MyViewHolder(val view: View) : RecyclerView.ViewHolder(view) {


        fun bindCategory(notification: Notification, binding: NotificaitonItemBinding) {

            binding.notificationTitle.text = notification.text
             binding.notificationTime.text = notification.time

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        binding = NotificaitonItemBinding.inflate(LayoutInflater.from(parent.context))

        return MyViewHolder(binding.root)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {


        holder.bindCategory(list.get(position), binding)

    }


}