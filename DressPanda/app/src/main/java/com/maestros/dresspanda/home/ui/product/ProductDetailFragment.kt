package com.maestros.dresspanda.home.ui.product

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import com.maestros.dresspanda.R
import com.maestros.dresspanda.databinding.FragmentProductDetailBinding
import com.maestros.dresspanda.home.HomeActivity
import com.maestros.dresspanda.home.ui.cart.CartFragmentViewModel
import com.maestros.dresspanda.home.ui.cart.repository.Cart
import com.maestros.dresspanda.home.ui.product.model.productdetail.ProductResponse
import com.maestros.dresspanda.home.ui.wishlist.model.WishlistProduct
import com.maestros.dresspanda.login.LoginActivity
import com.maestros.dresspanda.signup.model.Register
import com.maestros.dresspanda.util.Constant
import com.squareup.picasso.Picasso
import dagger.hilt.android.AndroidEntryPoint
import io.paperdb.Paper


@AndroidEntryPoint
class ProductDetailFragment : Fragment() {

    private lateinit var binding: FragmentProductDetailBinding

    private val productViewModel:ProductViewModel by viewModels()
    private val cartFragmentViewModel:CartFragmentViewModel by viewModels()

    var userID:String? = ""
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentProductDetailBinding.inflate(inflater)


        Paper.init(activity)
        val paperDB = Paper.book().read<ProductResponse>(Constant.PRODUCT_DETAIL)
        val register:Register = Paper.book().read<Register>(Constant.USER_DETAILS)
        userID = register.id

        val productToWishlist =
            WishlistProduct(id = paperDB.id, category_id = paperDB.category_id,
            sub_category_id = paperDB.sub_category_id,brand = paperDB.brand,
            image = paperDB.image,color = paperDB.color,MRP = paperDB.MRP,path = paperDB.path,
            product_description = paperDB.product_description,product_title = paperDB.product_title,
            seller_id = paperDB.seller_id,selling_price = paperDB.selling_price, pid = paperDB.pid,
            size = paperDB.size,stock = paperDB.stock)

        productViewModel.isProductOnDB(productToWishlist)
        productViewModel.getProductDetailsByID(productToWishlist.id)



        binding.pDdescriptionText.text = paperDB.product_description
        binding.pdProductName.text = paperDB.product_title
        binding.pdBrandText.text = paperDB.brand
//        binding.pdPrice.text = "Rs ${paperDB.MRP}"
        Picasso.get().load(Constant.BASE_URL + paperDB.image).
        placeholder(R.mipmap.ic_launcher_round).into(binding.pdImage)



        productViewModel.productEvent.observe(viewLifecycleOwner, Observer { it ->

            when (it) {

                is ProductViewModel.ProductEvent.ProductAddedSuccess -> {




                    activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.GONE

                    Toast.makeText(context, "Product added successfully", Toast.LENGTH_SHORT).show()

                    cartFragmentViewModel.getCartItems(Constant.getUserID(context))


                    }

                is ProductViewModel.ProductEvent.ProductOnDb ->
                    when (it.boolean) {
                        true -> binding.wihslistButton.setImageResource(R.drawable.heart_full)
                        false -> binding.wihslistButton.setImageResource(R.drawable.heart)
                    }

                is ProductViewModel.ProductEvent.Failure -> {
                    activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.GONE
                    Toast.makeText(context, "Error occured", Toast.LENGTH_SHORT).show()
                }
                is ProductViewModel.ProductEvent.ProductDetailsByID -> {
                    activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.GONE
                    try {
                        binding.shopName.text = it.productResponse.data.shop.store_name
                        binding.ppPrice.text = paperDB.selling_price
                        binding.ppPriceMRP.text = paperDB.MRP
                        binding.shopLocation.text = it.productResponse.data.shop.email
                    }catch (e:Exception){
                        binding.shopName.text = "unknown"
                    }
                    binding.shopName.setOnClickListener { view ->

                        Paper.init(activity)
                        Paper.book().write(Constant.SHOP_DETAILS, it.productResponse.data.shop)
                        val data = Bundle()
                        data.putBoolean(Constant.FROM_FOLLOWED_SHOPS, false)
                        Navigation.findNavController(view).navigate(R.id.shopDetailsFragment)
                    }


                }

                is ProductViewModel.ProductEvent.AddProductToCartSuccess -> {

                    activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.GONE
                    cartFragmentViewModel.getCartItems(Constant.getUserID(context))
                    Toast.makeText(requireContext(), "Product Added to cart", Toast.LENGTH_SHORT).show()
                    binding.addToCartButton.setText("Added to cart")

                }

                is ProductViewModel.ProductEvent.Loading -> {

                    activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.VISIBLE

                }

            }

        })

        cartFragmentViewModel.cartItems.observe(viewLifecycleOwner, Observer {

            when(it){

                is CartFragmentViewModel.CartEvent.GetCartItemSuccess -> {
                    Constant.setCartCount(
                        HomeActivity.bottomNavigationView,
                        it.cartItems.qntity
                    )
                }
            }

        })

        binding.wihslistButton.setOnClickListener {



            lifecycleScope.launchWhenStarted {
                productViewModel.insertProduct(productToWishlist!!)
                binding.wihslistButton.setImageResource(R.drawable.wish_p)
                Toast.makeText(context, "Product added to wishlist", Toast.LENGTH_SHORT).show()
            }

}


        binding.addToCartButton.setOnClickListener { view->

            if (!Constant.isUserLogged(requireActivity())!!){

                startActivity(Intent(view.context, LoginActivity::class.java).putExtra(Constant.FROM,"product_detail"))


            }else {

            productViewModel.addToCart(userId = userID, productID = productToWishlist.id,
                quantity = "1")
                }
        }



        binding.postShareButton.setOnClickListener {

            Constant.shareImageFromURI(Constant.BASE_URL + productToWishlist.image, productToWishlist.product_title, it.context)


        }
        return binding.root
    }

    override fun onResume() {
        super.onResume()


        val toolbar = activity?.findViewById<Toolbar>(R.id.toolbar)
        toolbar?.visibility = View.GONE
    }

    override fun onPause() {
        super.onPause()

        val toolbar = activity?.findViewById<Toolbar>(R.id.toolbar)
        toolbar?.visibility = View.VISIBLE
    }


}