package com.maestros.dresspanda.home.ui.address

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "address")
data class Address(
    @PrimaryKey(autoGenerate = true)
    var id:Int?=null,
    var name:String?,
    var buildingNumber:String?,
    var streetNumber:String?,
    var address:String?,
    var landmarkEdit:String?,
    var pincodeNumber:String?,
    var cityEdit:String?,
    var stateEdit:String?
    ){

}