package com.maestros.dresspanda.home.ui.compaigns.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.maestros.dresspanda.R
import com.maestros.dresspanda.databinding.*
import com.maestros.dresspanda.home.ui.home.HomeAdapter
import com.maestros.dresspanda.home.ui.compaigns.CompaignItem
import com.maestros.dresspanda.home.ui.compaigns.Compaigns

class CampaignAdapter(val list:List<Compaigns>, val layout:Int) :RecyclerView.Adapter<CampaignAdapter.MyViewHolder>(){
    lateinit var binding:CompaignItemListBinding


    class MyViewHolder(val view:View):RecyclerView.ViewHolder(view) {


         fun bindCategory(compaigns: Compaigns, binding: CompaignItemListBinding) {


             binding.compaignTitle.text = compaigns.compaignTitle
             binding.compaignSubtitle.text = compaigns.compaignDescription
//             binding.compaignParentLayout.setBackgroundColor(Integer.parseInt(compaigns.compaignBackgroundColor!!))

             binding.offerList.apply {

                 layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                 adapter =
                     HomeAdapter<CompaignItem>(
                         compaigns.list,
                         R.layout.brand_item_list
                     )

             }
        }

    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CampaignAdapter.MyViewHolder {

        binding = CompaignItemListBinding.inflate(LayoutInflater.from(parent.context))

        return MyViewHolder(binding.root)


    }

    override fun getItemCount(): Int {

        return list.size
    }

    override fun onBindViewHolder(holder: CampaignAdapter.MyViewHolder, position: Int) {

        holder.bindCategory(list.get(position),binding)


    }

}