
import android.util.Log
import com.maestros.dresspanda.home.ui.brand.BaseResponse
import com.maestros.dresspanda.home.ui.brand.HomeResponse
import com.maestros.dresspanda.home.ui.category.adapter.model.CategoryResponse
import com.maestros.dresspanda.home.ui.product.model.productdetail.ProductResponse
import com.maestros.dresspanda.home.ui.shop.repository.Shop
import com.maestros.dresspanda.home.ui.shop.repository.ShopAPI
import com.maestros.dresspanda.seller.repository.model.SellerResponse
import com.maestros.dresspanda.util.Resource
import javax.inject.Inject


class ShopImplementation @Inject constructor(val api: ShopAPI): Shop {
    override suspend fun getShops(action: String?): Resource<List<SellerResponse>> {
        return try {
            val response = api.getShops(action)
            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())
                Resource.Success(result.list)

            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }

    }

    override suspend fun getProductsByShopID(
        action: String?,
        data: HashMap<String, String>
    ): Resource<BaseResponse<ProductResponse>> {
        return try {
            val response = api.getProductsByShopID(action,data)
            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())
                Resource.Success(result)

            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }

    override suspend fun getCategoriesByShopID(
        action: String?,
        data: HashMap<String, String>
    ):Resource<BaseResponse<CategoryResponse>> {
        return try {
            val response = api.getCategoriesByShopID(action,data)
            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())
                Resource.Success(result)

            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }

    override suspend fun getShopsByBrandID(
        action: String?,
        map: HashMap<String, String>
    ): Resource<BaseResponse<SellerResponse>> {
        return try {
            val response = api.getShopsByBrandID(action,map)
            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())
                Resource.Success(result)

            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }


}