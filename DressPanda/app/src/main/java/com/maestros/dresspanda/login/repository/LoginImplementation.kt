package com.maestros.dresspanda.login.repository

import android.util.Log
import com.maestros.dresspanda.login.model.ForgetPassword
import com.maestros.dresspanda.signup.model.Register
import com.maestros.dresspanda.util.Resource
import javax.inject.Inject


class LoginImplementation @Inject constructor(val api: LoginAPI): Login {
    override suspend fun login(action: String?, data: Map<String, String>?): Resource<Register> {
        return try {
            val response = api.login("login", data!!)
            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }

    }

    override suspend fun socialLogin(
        action: String?,
        data: Map<String, String>?
    ): Resource<Register> {

        return try {
            val response = api.socialLogin(action, data!!)
            val result = response.body()

            if (response.isSuccessful && result != null) {

                Resource.Success(result)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }


    }

    override suspend fun forgetPassword(
        action: String?,
        data: Map<String, String>?
    ): Resource<ForgetPassword> {

        return try {
            val response = api.forgetPassword(action, data!!)
            val result = response.body()

            if (response.isSuccessful && result != null) {

                Resource.Success(result)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }

    }




}