package com.maestros.dresspanda.home.ui.brand

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.maestros.dresspanda.R
import com.maestros.dresspanda.databinding.FragmentBrandsBinding
import com.maestros.dresspanda.home.ui.home.HomeAdapter
import com.maestros.dresspanda.home.ui.home.HomeViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [BrandsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */

@AndroidEntryPoint
class BrandsFragment : Fragment() {



    lateinit var binding: FragmentBrandsBinding
    lateinit var brandViewModel: BrandViewModel
    private val homeViewModel:HomeViewModel by viewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentBrandsBinding.inflate(layoutInflater)
        homeViewModel.getHomeBrands()


        lifecycleScope.launch {

         homeViewModel.homeEvent.observe(viewLifecycleOwner, Observer {
             when (it) {

                 is HomeViewModel.HomeEvent.SuccessBrands -> {

                     activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.GONE

                     binding.brandlist.apply {
                         layoutManager =
                             GridLayoutManager(context, 2)
                         adapter =
                             HomeAdapter<HomeResponse>(
                                 it.resultText,
                                 R.layout.brand_item_list)


                     }
                 }

                 is HomeViewModel.HomeEvent.Failure -> {
                     activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.GONE

                     Toast.makeText(context, "Error occured", Toast.LENGTH_SHORT).show()
                 }

                 is HomeViewModel.HomeEvent.Loading -> {
                     activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.VISIBLE

                 }
             }
         })
        }

        return binding.root
    }


}