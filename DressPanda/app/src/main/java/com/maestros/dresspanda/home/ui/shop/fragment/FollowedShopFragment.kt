package com.maestros.dresspanda.home.ui.shop.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.maestros.dresspanda.R
import com.maestros.dresspanda.databinding.FragmentFollowedShopBinding
import com.maestros.dresspanda.home.ui.home.HomeAdapter
import com.maestros.dresspanda.home.ui.shop.ShopViewModel
import com.maestros.dresspanda.home.ui.wishlist.model.WishlistShop
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FollowedShopFragment : Fragment() {

    lateinit var binding: FragmentFollowedShopBinding
    val shopViewModel: ShopViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentFollowedShopBinding.inflate(inflater)
        shopViewModel.getWishlistShops()

        shopViewModel.shopsWishList.observe(viewLifecycleOwner, Observer {

            binding.followedShopList.apply {
                layoutManager = GridLayoutManager(context, 2)
                adapter = HomeAdapter<WishlistShop>(it.toList(), R.layout.brand_item_list)
            }

        })

        return binding.root
    }


}