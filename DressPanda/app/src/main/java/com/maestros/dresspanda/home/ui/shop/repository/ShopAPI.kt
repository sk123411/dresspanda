package com.maestros.dresspanda.home.ui.shop.repository

import com.maestros.dresspanda.home.ui.brand.BaseResponse
import com.maestros.dresspanda.home.ui.brand.HomeResponse
import com.maestros.dresspanda.home.ui.category.adapter.model.CategoryResponse
import com.maestros.dresspanda.home.ui.product.model.productdetail.ProductResponse
import com.maestros.dresspanda.seller.repository.model.SellerResponse
import retrofit2.Response
import retrofit2.http.*

interface ShopAPI {

    @GET("dresspanda/api/process.php")
    suspend fun getShops(@Query("action") get_shops:String?):
            Response<BaseResponse<SellerResponse>>

    @POST("dresspanda/api/process.php")
    @FormUrlEncoded
    suspend fun getProductsByShopID(@Query("action") get_shops:String?,
                                    @FieldMap data:HashMap<String,String>):
            Response<BaseResponse<ProductResponse>>

    @POST("dresspanda/api/process.php")
    @FormUrlEncoded
    suspend fun getCategoriesByShopID(@Query("action") get_shops:String?,
                                      @FieldMap data:HashMap<String,String>):
            Response<BaseResponse<CategoryResponse>>

    @POST("dresspanda/api/process.php")
    @FormUrlEncoded
    suspend fun getShopsByBrandID(@Query("action") banner:String?,@FieldMap map:HashMap<String,String>): Response<BaseResponse<SellerResponse>>


}