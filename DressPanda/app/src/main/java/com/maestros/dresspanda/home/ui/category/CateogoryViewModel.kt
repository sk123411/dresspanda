package com.maestros.dresspanda.home.ui.category

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.maestros.dresspanda.home.ui.category.adapter.model.CategoryResponse
import com.maestros.dresspanda.home.ui.category.adapter.model.SubCategoryResponse
import com.maestros.dresspanda.util.Resource
import kotlinx.coroutines.launch

class CateogoryViewModel  @ViewModelInject constructor(
        val category: com.maestros.dresspanda.login.repository.Category
    ):ViewModel() {

    private val _categoryEvent = MutableLiveData<CategoryEvent>(CategoryEvent.Empty)
    val categoryEvent: LiveData<CategoryEvent> = _categoryEvent



        sealed class CategoryEvent {
            class Success(val resultText: List<CategoryResponse>): CategoryEvent()
            class Failure(val errorText: String): CategoryEvent()
            object Loading : CategoryEvent()
            object Empty : CategoryEvent()
            class SubCategorySuccess(val resultText: List<SubCategoryResponse>): CategoryEvent()

        }


    fun getSubCategoriesByCategory(categoryID:String?){

        _categoryEvent.value = CategoryEvent.Loading
        val map = HashMap<String,String>()
        map.put("category_id", categoryID!!)

        viewModelScope.launch {
            val response = category.getSubCategoriesByCategory(category = "show_subcategory_by_catid",map = map)
            when(response){

                is Resource.Success->{
                    if(response.data?.status!!){
                        _categoryEvent.value = CategoryEvent.SubCategorySuccess(response.data!!.list)
                    }else {
                        _categoryEvent.value = CategoryEvent.Failure("No results found")
                    }


                }
                is Resource.Error-> _categoryEvent.value = CategoryEvent.Failure(response.message!!)
            }

        }

    }

    fun getCategories(){

        _categoryEvent.value = CategoryEvent.Loading

        viewModelScope.launch {
            val response = category.getCategories(category = "show_catetory")
            when(response){

                is Resource.Success->{
                    if(response.data?.status!!){
                        _categoryEvent.value = CategoryEvent.Success(response.data.list)
                    }else {
                        _categoryEvent.value = CategoryEvent.Failure("No results found")
                    }
                }
                is Resource.Error-> _categoryEvent.value = CategoryEvent.Failure(response.message!!)
            }

        }

    }


}