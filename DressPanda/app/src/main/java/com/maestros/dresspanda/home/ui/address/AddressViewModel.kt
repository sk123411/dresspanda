package com.maestros.dresspanda.home.ui.address

import android.content.Context
import android.os.Message
import android.text.TextUtils
import android.widget.EditText
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.maestros.dresspanda.R
import com.maestros.dresspanda.data.address.AddressDAO
import com.maestros.dresspanda.home.profile.ProfileViewModel
import com.maestros.dresspanda.home.ui.address.repository.Address
import com.maestros.dresspanda.home.ui.address.repository.allAddressModel.Data
import com.maestros.dresspanda.home.ui.wishlist.model.WishlistProduct
import com.maestros.dresspanda.util.Resource
import kotlinx.coroutines.launch
import org.json.JSONObject

class AddressViewModel @ViewModelInject constructor(
    val addressInt:Address,
    val context: Context
):ViewModel(){


    val addressEvent_ = MutableLiveData<AddressEvent>()

    val addressEvent: LiveData<AddressEvent>
        get() = addressEvent_



    val emptyFieldsEvent_ = MutableLiveData<Boolean>()

    val emptyFieldsEvent: LiveData<Boolean>
        get() = emptyFieldsEvent_



    sealed class AddressEvent{

        class AddressFetched(val address: MutableList<Data>) :AddressEvent()
        class AddressAdded(val address: AddressX?) :AddressEvent()
        class AddressDeleted(val address: JSONObject?) :AddressEvent()
        class AddressUpdated(val address: AddressX?) :AddressEvent()
        class Failure( val message: String?):AddressEvent()
        object Loading : AddressEvent()
        object Empty :AddressEvent()

    }





    fun getAddress(user_id: String?){
        addressEvent_.value = AddressEvent.Loading
        val map = HashMap<String,String>()
        map.put("user_id",user_id!!)
        viewModelScope.launch {


            val response = addressInt.showAddress("show_address", map)

            when(response){

                is Resource.Success -> {


                    val list = mutableListOf<Data>()
                    list.addAll(response.data?.data!!.allAddress)

                    addressEvent_.value = AddressEvent.AddressFetched(list)
                }


            }

        }






    }


    fun updateAddress(addressID: String?,address: Data){
        addressEvent_.value = AddressEvent.Loading

        val map = HashMap<String,String>()
        map.put("id", addressID!!)
        map.put("name", address.name)
        map.put("bulding_no", address.buldingNo)
        map.put("street_no", address.streetNo)
        map.put("address", address.address)
        map.put("landmark", address.landmark)
        map.put("pincode", address.pincode)
        map.put("city", address.city)
        map.put("state", address.state)

        viewModelScope.launch {



            viewModelScope.launch {


                val response = addressInt.updateAddressItem("update_address", map)

                when(response){

                    is Resource.Success -> {


                        addressEvent_.value = AddressEvent.AddressUpdated(response.data)
                    }


                }
            }

        }



    }


    fun deleteAddress(addressID: String?){
        addressEvent_.value = AddressEvent.Loading

        val map = HashMap<String,String>()
        map.put("id", addressID!!)

        viewModelScope.launch {


            val response = addressInt.deleteAddressItem("delete_address", map)

            when(response){

                is Resource.Success -> {

                    addressEvent_.value = AddressEvent.AddressDeleted(response.data)
                }


            }
        }



    }

    fun insertAddress(
        address:Data, user_id:String?){
        addressEvent_.value = AddressEvent.Loading

        val map = HashMap<String,String>()
        map.put("user_id", user_id!!)
        map.put("name", address.name)
        map.put("bulding_no", address.buldingNo)
        map.put("street_no", address.streetNo)
        map.put("address", address.address)
        map.put("landmark", address.landmark)
        map.put("pincode", address.pincode)
        map.put("city", address.city)
        map.put("state", address.state)


        viewModelScope.launch {

            val response = addressInt.addAddress("add_address", map)

            when(response){

                is Resource.Success -> {

                    addressEvent_.value = AddressEvent.AddressAdded(response.data)

                }

                is Resource.Error -> {


                }
            }

        }


    }








    fun checkInputEmptyFields(name: EditText?, buildingNumber: EditText?, streetNumberEdit: EditText?,
                              address: EditText?,
                              landmarkEdit: EditText?, pincodeNumber: EditText?, cityEdit: EditText?,
                              stateEdit: EditText?
                              ):Boolean{

        if (TextUtils.isEmpty(name?.text.toString().trim())){
            name?.setError(context.resources.getString(R.string.emptyName))
            emptyFieldsEvent_.value = true
            return false
        }else if (TextUtils.isEmpty(buildingNumber?.text.toString().trim())){
            buildingNumber?.setError(context.resources.getString(R.string.emptyName))
            emptyFieldsEvent_.value = true

            return false
        }else if (TextUtils.isEmpty(streetNumberEdit?.text.toString().trim())) {
            streetNumberEdit?.setError(context.resources.getString(R.string.emptyName))
            emptyFieldsEvent_.value = true
        }else if (TextUtils.isEmpty(address?.text.toString().trim())) {
            address?.setError(context.resources.getString(R.string.emptyName))
            emptyFieldsEvent_.value = true
        }else if (TextUtils.isEmpty(landmarkEdit?.text.toString().trim())) {
            landmarkEdit?.setError(context.resources.getString(R.string.emptyName))
            emptyFieldsEvent_.value = true
        }else if (TextUtils.isEmpty(pincodeNumber?.text.toString().trim())) {
            pincodeNumber?.setError(context.resources.getString(R.string.emptyName))
            emptyFieldsEvent_.value = true
        }else if (TextUtils.isEmpty(cityEdit?.text.toString().trim())) {
            cityEdit?.setError(context.resources.getString(R.string.emptyName))
            emptyFieldsEvent_.value = true
        }
        else if (TextUtils.isEmpty(stateEdit?.text.toString().trim())) {
            stateEdit?.setError(context.resources.getString(R.string.emptyName))
            emptyFieldsEvent_.value = true
        }else {
            emptyFieldsEvent_.value = false
        }
        return emptyFieldsEvent_.value!!
    }




    }