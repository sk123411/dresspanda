package com.maestros.dresspanda.home.profile

import android.os.Binder
import android.os.Bundle
import android.text.InputType
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.maestros.dresspanda.R
import com.maestros.dresspanda.databinding.FragmentChangePasswordBinding
import com.maestros.dresspanda.home.profile.repository.Profile
import com.maestros.dresspanda.login.LoginViewModel
import com.maestros.dresspanda.util.TextChangeListernerImple
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect


@AndroidEntryPoint
class ChangePasswordFragment : Fragment() {


    private val profileViewModel: ProfileViewModel by viewModels()
    private val loginViewModel:LoginViewModel by viewModels()
    lateinit var binding: FragmentChangePasswordBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentChangePasswordBinding.inflate(layoutInflater)
        // Inflate the layout for this fragment
        loginViewModel.getUserDetails()

        lifecycleScope.launchWhenStarted {

            profileViewModel.emptyValueEvent.observe(viewLifecycleOwner, Observer {
                when(it){

                    true -> binding.changePasswordBtn.isEnabled = false
                    false -> {

                        binding.changePasswordBtn.isEnabled = true
                        binding.changePasswordBtn.setOnClickListener {

                            lifecycleScope.launchWhenStarted {
                                loginViewModel.login.collect {
                                    when(it) {
                                        is LoginViewModel.LoginEvent.UserDetails -> {
                                                profileViewModel.changePassword(
                                                    userId = it.userDetails.id,
                                                    oldPassword = binding.cnfPasswordEdit.text.toString().trim(),
                                                    newPassword = binding.newPasswordEdit.text.toString().trim(),
                                                    confirmPassword = binding.cnfPasswordEdit.text.toString().trim()

                                                )
                                        }
                                    }
                                }
                            }

                        }

                    }
                }


            })



            profileViewModel.profileEvent.collect {

                when(it){

                    is ProfileViewModel.ProfileEvent.SuccessPasswordChange -> {
                        Toast.makeText(context, "Password change successfully", Toast.LENGTH_SHORT).show()
                    }
                }

            }



            binding.oldCurrentPasswordBtn.setOnClickListener {

                profileViewModel.setHidePasswordValues("isHideOld","1")

                binding.currentPasswordEdit.inputType =InputType.TYPE_TEXT_VARIATION_PASSWORD

            }

            binding.cnfPasswordBtn.setOnClickListener {
                profileViewModel.setHidePasswordValues("isHideCnfNew","1")
                binding.cnfPasswordEdit.inputType =InputType.TYPE_TEXT_VARIATION_PASSWORD

            }
            binding.newCurrentPasswordBtn.setOnClickListener {
                profileViewModel.setHidePasswordValues("isHideNew","1")
                binding.newPasswordEdit.inputType =InputType.TYPE_TEXT_VARIATION_PASSWORD

            }




            profileViewModel.hidePasswordMap.observe(viewLifecycleOwner, Observer {


                if (it.get("isHideOld").equals("0")){

                    binding.oldCurrentPasswordBtn.setImageResource(R.drawable.hide)


                }else {




                    binding.oldCurrentPasswordBtn.setImageResource(R.drawable.show)

                    binding.oldCurrentPasswordBtn.setOnClickListener {

                        profileViewModel.setHidePasswordValues("isHideOld","0")

                        binding.currentPasswordEdit.inputType = InputType.TYPE_CLASS_TEXT

                    }
                }


                if (it.get("isHideNew").equals("0")){

                    binding.newCurrentPasswordBtn.setImageResource(R.drawable.hide)


                }else {
                    binding.newCurrentPasswordBtn.setImageResource(R.drawable.show)
                    binding.newCurrentPasswordBtn.setOnClickListener {
                        profileViewModel.setHidePasswordValues("isHideNew","0")
                        binding.newPasswordEdit.inputType = InputType.TYPE_CLASS_TEXT

                    }


                }


                if (it.get("isHideCnfNew").equals("0")){

                    binding.cnfPasswordBtn.setImageResource(R.drawable.hide)


                }else {
                    binding.cnfPasswordBtn.setImageResource(R.drawable.show)

                    binding.cnfPasswordBtn.setOnClickListener {
                        profileViewModel.setHidePasswordValues("isHideCnfNew","0")

                        binding.cnfPasswordEdit.inputType = InputType.TYPE_CLASS_TEXT


                    }

                }



            })


        }

        binding.currentPasswordEdit.addTextChangedListener(TextChangeListernerImple(
            object : TextChangeListernerImple.TextChangeListerner {
                override fun onTextChange(text: String?) {
                    profileViewModel.checkInputEmptyFields(
                        binding.currentPasswordEdit,
                        binding.newPasswordEdit,
                        binding.cnfPasswordEdit
                    )

                }

            }
        ))


        binding.newPasswordEdit.addTextChangedListener(TextChangeListernerImple(
            object : TextChangeListernerImple.TextChangeListerner {
                override fun onTextChange(text: String?) {
                    profileViewModel.checkInputEmptyFields(
                        binding.currentPasswordEdit,
                        binding.newPasswordEdit,
                        binding.cnfPasswordEdit
                    )

                }

            }
        ))


        binding.cnfPasswordEdit.addTextChangedListener(TextChangeListernerImple(
            object : TextChangeListernerImple.TextChangeListerner {
                override fun onTextChange(text: String?) {
                    profileViewModel.checkInputEmptyFields(
                        binding.currentPasswordEdit,
                        binding.newPasswordEdit,
                        binding.cnfPasswordEdit
                    )

                }

            }
        ))











        return binding.root
    }


}