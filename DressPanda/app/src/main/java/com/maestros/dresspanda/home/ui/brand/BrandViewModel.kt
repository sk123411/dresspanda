package com.maestros.dresspanda.home.ui.brand

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.maestros.dresspanda.home.ui.home.HomeViewModel
import com.maestros.dresspanda.util.Resource
import kotlinx.coroutines.launch

class BrandViewModel : ViewModel() {

    val brandlist_ = MutableLiveData<List<HomeResponse>>()

    val brandlist:LiveData<List<HomeResponse>>
        get() = brandlist_



    private val _text = MutableLiveData<String>().apply {
        value = "This is home Fragment"
    }
    val text: LiveData<String> = _text






}