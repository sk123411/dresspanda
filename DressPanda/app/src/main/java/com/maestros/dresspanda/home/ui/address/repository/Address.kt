package com.maestros.dresspanda.home.ui.address.repository

import com.maestros.dresspanda.home.ui.address.AddressX
import com.maestros.dresspanda.home.ui.address.repository.allAddressModel.AllAddress
import com.maestros.dresspanda.util.Resource
import org.json.JSONObject
import retrofit2.Response
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import retrofit2.http.Query

interface Address {

    suspend fun showAddress( show_address:String?,map:HashMap<String,String>): Resource<AllAddress>

    suspend fun updateAddressItem(updateAddress:String?,map:HashMap<String,String>): Resource<AddressX>


    suspend fun deleteAddressItem(deleteAddress:String?, map:HashMap<String,String>): Resource<JSONObject>
    suspend fun addAddress(addAddress:String?,map:HashMap<String,String>): Resource<AddressX>



}