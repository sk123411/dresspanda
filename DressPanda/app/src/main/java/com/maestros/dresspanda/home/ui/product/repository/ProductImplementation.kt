package com.maestros.dresspanda.home.ui.product.repository

import android.util.Log
import com.maestros.dresspanda.home.ui.brand.BaseResponse
import com.maestros.dresspanda.home.ui.product.model.productdetail.ProductDetailResponse
import com.maestros.dresspanda.home.ui.product.model.productdetail.ProductResponse
import com.maestros.dresspanda.util.Resource
import org.json.JSONObject
import javax.inject.Inject


class ProductImplementation @Inject constructor(val api: ProductAPI):Product {
    override suspend fun getProductsByCategory(
        action: String?,
        category_id: HashMap<String, String>
    ): Resource<BaseResponse<ProductResponse>> {
        return try {
            val response = api.getProductsByCategoryID(action,category_id)
            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())
                Resource.Success(result)

            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }

    }

    override suspend fun getProductDetailsByID(
        action: String?,
        product_id: HashMap<String, String>
    ): Resource<ProductDetailResponse> {
        return try {
            val response = api.getProductDetailsByID(action,product_id)
            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())
                Resource.Success(result)

            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }

    }

    override suspend fun addProductToCart(
        action: String?,
        data: HashMap<String, String>
    ): Resource<ProductResponse> {
        return try {
            val response = api.addProductToCart(action,data)
            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())
                Resource.Success(result)

            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }

    override suspend fun addCountProductSeen(
        count_view_product: String?,
        map: HashMap<String, String>
    ): Resource<JSONObject> {
        return try {
            val response = api.addCountProductSeen(count_view_product,map)
            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())
                Resource.Success(result)

            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }


}