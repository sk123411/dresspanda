package com.maestros.dresspanda.home.ui.address.repository.allAddressModel


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class AllAddress(
    @SerializedName("data")
    @Expose
    val `data`: DataX,
    @SerializedName("result")
    @Expose
    val result: Boolean
)