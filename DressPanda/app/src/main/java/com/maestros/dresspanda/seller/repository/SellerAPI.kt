package com.maestros.dresspanda.seller.repository

import com.maestros.dresspanda.home.ui.brand.BaseResponse
import com.maestros.dresspanda.seller.City
import com.maestros.dresspanda.seller.repository.model.ManagerResponse
import com.maestros.dresspanda.seller.repository.model.SellerResponse
import com.maestros.dresspanda.util.Constant
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*
import javax.annotation.PostConstruct

interface SellerAPI {

    @GET("dresspanda/api/process.php")
    suspend fun getCities(@Query("action") show_city:String?):Response<BaseResponse<City>>


    @Multipart
    @POST("dresspanda/api/process.php")
    suspend fun applyAsSeller(@Query("action") action:String?,
                      @Part image: MultipartBody.Part?,
                      @Part("name") name: RequestBody?,
                      @Part("email") email: RequestBody?,
                      @Part("password") password: RequestBody?,
                      @Part("address") address: RequestBody?,
                      @Part("pincode") pincode: RequestBody?,
                      @Part("Payment_method") Payment_method: RequestBody?,
                      @Part("category") category: RequestBody?,
                      @Part("shop_title") shop_title: RequestBody?,
                      @Part("mobile") mobile: RequestBody?,
                      @Part("store_name") store_name: RequestBody?,
                      @Part("brand") brand: RequestBody?,
                      @Part("city") city: RequestBody):Response<SellerResponse>


    @POST("dresspanda/api/process.php")
    @FormUrlEncoded
    suspend fun applyAsManager(@Query("action") action:String?,
                               @FieldMap body: HashMap<String,String>)
                            :Response<ManagerResponse>


}