data class Register(
    val email: String,
    val id: String,
    val name: String,
    val password: String,
    val path: String,
    val result: String
)