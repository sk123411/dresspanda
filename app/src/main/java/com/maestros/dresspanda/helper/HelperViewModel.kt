package com.maestros.dresspanda.helper

import androidx.constraintlayout.solver.widgets.Helper
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.hilt.lifecycle.ViewModelInject

class HelperViewModel @ViewModelInject constructor() : ViewModel() {


    private val _helperEvent = MutableLiveData<HelperEvent>()

    val helperEvent:LiveData<HelperEvent>
        get() = _helperEvent

 init {
     _helperEvent.value = HelperEvent.Loading(true)

 }



    sealed class HelperEvent(){

        class Loading(val boolean: Boolean):HelperEvent()

    }



    fun setLoading(boolean: Boolean){

        _helperEvent.value = HelperEvent.Loading(boolean)

    }






}