package com.maestros.dresspanda.signup.repository

import com.maestros.dresspanda.signup.model.Register
import retrofit2.Response
import retrofit2.http.*

interface SignUpAPI {

    @POST("dresspanda/api/process.php")
    @FormUrlEncoded
    suspend fun register(@Query("action") signup:String?, @FieldMap data:Map<String,String>?):Response<Register>

    @POST("dresspanda/api/process.php")
    @FormUrlEncoded
    suspend fun socialLogin(@Query("action")
                            social_sign_up:String?, @FieldMap data:Map<String,String>?):Response<Register>



}