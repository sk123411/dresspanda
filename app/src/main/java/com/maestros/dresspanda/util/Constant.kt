package com.maestros.dresspanda.util

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.snackbar.Snackbar
import com.maestros.dresspanda.R
import com.maestros.dresspanda.application.DressApplication
import com.maestros.dresspanda.signup.model.Register
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import io.paperdb.Paper
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class Constant {

    companion object {
        val CURRENCYSYMBOL: String="৳"
        val STORE_PASSWORD: String?="dress60839e17edd14@ssl"
        val STORE_ID: String?="dress60839e17edd14"
        var FILTER: String?="filter"

        var LANGUAGE: String?="language"
        var FROM: String?="from"
        val FROM_BRANDS: String?="from_brands"
        var THEME_OPTION: String?="0"
        val SHOP_WISHLIST_DETAILS: String?="shop_wishlist_details"
        val PLACE_ORDER: String?="place_order"
        var SelectedAddressID: String?=""
        val CART_MAP: String?="cart_map"
        val IMAGE: String?="image"
        val USER_DETAILS: String?="user_details"
        val FROM_FOLLOWED_SHOPS: String? = "followed_shops"
        val SHOP_DETAILS: String?="shop_details"
        const val USER_LOGGED_IN: String="user_logged_in"
        const val IS_FROM_HOME: String ="from_home"
        const  val PRODUCT_DETAIL: String="product_detail"
        const val BASE_URL:String="https://ruparnatechnology.com/dresspanda/image/"
        const val NAME: String="name"
        const val PASSWORD: String="password"
        const val EMAIL: String="email"
        const val PINCODE: String="pincode"
        const val ADDRESS: String="address"

        const val PAYMENT_METHOD: String="Payment_method"
        const val SHOP_TITLE: String="shop_title"
        const val CATEGORY: String="category"
        const  val MOBILE: String="mobile"
        const val STORE_NAME: String="store_name"
        const val BRAND: String="brand"
        const val CITY: String="city"
        const val STORE_IMAGE:String = "store_image"
        const val ACTION_APPLY_AS_SELLER = "apply_as_seller"









        fun getUserID(context: Context?):String?{
            var USER_ID:String?= ""

            Paper.init(context)
            val register:Register? = Paper.book().read(Constant.USER_DETAILS)
            USER_ID = register?.id
            return USER_ID
        }

        fun showSnackBar(view: View?, errorText:String?, actionTitle:String?, listener:View.OnClickListener):Snackbar {

            return Snackbar.make(view!!,errorText!!,Snackbar.LENGTH_SHORT).
            setAction(actionTitle,listener)



        }


        fun setCartCount(bottomNavigationView: BottomNavigationView, quantity:String?){
            val badge = bottomNavigationView.getOrCreateBadge(R.id.bottom_cart)

            if (quantity!!.toInt() > 0) {
                badge.isVisible = true
                badge.number = quantity.toInt()
                badge.backgroundColor = bottomNavigationView.resources.getColor(R.color.purple_700)
            }else {
                badge.isVisible = false

            }
        }


        fun showMessage(message:String?,context: Context?){

            Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
        }


        fun shareImageFromURI(url: String?, title: String?, context: Context) {
            Picasso.get().load(url).into(object : Target {
                override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {

                    val intent = Intent(Intent.ACTION_SEND)
                    intent.type = "*/*"
                    intent.putExtra(Intent.EXTRA_STREAM, getBitmapFromView(bitmap, context))
                    intent.putExtra(
                        Intent.EXTRA_TEXT,
                        title + "\n" + "Dresspanda download app from playstore"
                    )
                    context.startActivity(Intent.createChooser(intent, "Share Image"))
                }

                override fun onPrepareLoad(placeHolderDrawable: Drawable?) {}
                override fun onBitmapFailed(e: java.lang.Exception?, errorDrawable: Drawable?) {}
            })
        }


        fun getBitmapFromView(bitmap: Bitmap?, context: Context?): Uri? {
            var bmpUri: Uri? = null
            try {
                val file =
                    File(
                        context!!.externalCacheDir,
                        System.currentTimeMillis().toString() + ".jpg"
                    )

                val out = FileOutputStream(file)
                bitmap?.compress(Bitmap.CompressFormat.JPEG, 90, out)
                out.close()
                bmpUri = Uri.fromFile(file)

            } catch (e: IOException) {
                e.printStackTrace()
            }

            return bmpUri!!

        }



        fun isUserLogged(activity: Context):Boolean?{

            Paper.init(activity)
            val user = Paper.book().read<Int>(Constant.USER_LOGGED_IN,0)

            return user != 0



        }


        fun getLanguage(context: Context?):String{
            Paper.init(context)
            return Paper.book().read<String>(Constant.LANGUAGE,"en")

        }


        fun setLanguage(context: Context?,language:String?){
            Paper.init(context)
            Paper.book().write(Constant.LANGUAGE,language)


        }



    }



}