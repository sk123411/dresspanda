package com.maestros.dresspanda

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.facebook.AccessToken
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.maestros.dresspanda.databinding.ActivitySplashBinding
import com.maestros.dresspanda.home.HomeActivity
import com.maestros.dresspanda.login.LoginActivity
import com.maestros.dresspanda.login.LoginViewModel
import com.maestros.dresspanda.util.Constant
import dagger.hilt.android.AndroidEntryPoint
import io.paperdb.Paper
import kotlinx.coroutines.flow.collect


class SplashActivity : AppCompatActivity() {
    lateinit var binding: ActivitySplashBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)

        Paper.init(this@SplashActivity)
        val paper = Paper.book().read<Int>(Constant.USER_LOGGED_IN, 0)
        val languageCode = Paper.book().read<String>(Constant.LANGUAGE)

        if (languageCode==null){
           Paper.book().write(Constant.LANGUAGE,"en")
        }




        Handler().postDelayed({


                startActivity(
                    Intent(
                        applicationContext,
                        HomeActivity::class.java
                    )
                )


        }, 2000)

    }


}
