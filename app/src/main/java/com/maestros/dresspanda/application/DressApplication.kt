package com.maestros.dresspanda.application

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class DressApplication : Application()