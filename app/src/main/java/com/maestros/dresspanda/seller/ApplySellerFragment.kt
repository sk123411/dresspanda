package com.maestros.dresspanda.seller

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.material.snackbar.Snackbar
import com.maestros.dresspanda.R
import com.maestros.dresspanda.databinding.FragmentApplySellerBinding
import com.maestros.dresspanda.home.ui.brand.HomeResponse
import com.maestros.dresspanda.home.ui.category.CateogoryViewModel
import com.maestros.dresspanda.home.ui.category.adapter.model.CategoryResponse
import com.maestros.dresspanda.home.ui.home.HomeViewModel
import com.maestros.dresspanda.seller.repository.SellerManagerViewModel
import com.maestros.dresspanda.util.TextChangeListernerImple
import dagger.hilt.android.AndroidEntryPoint
import java.io.File


/**
 * A simple [Fragment] subclass.
 * Use the [ApplySellerFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
class ApplySellerFragment : Fragment() {


    lateinit var binding: FragmentApplySellerBinding
    val categoryViewModel: CateogoryViewModel by viewModels()
    private val homeViewModel: HomeViewModel by viewModels()
    val sellerViewModel: SellerManagerViewModel by viewModels()
    lateinit var file: File


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentApplySellerBinding.inflate(inflater)
        categoryViewModel.getCategories()
        homeViewModel.getHomeBrands()
        sellerViewModel.getCities()


        binding.submitBtn.isEnabled = sellerViewModel.checkInputEmptyFields(
                name = binding.name,
                email = binding.email,
                password = binding.password,
                address = binding.address,
                pincode = binding.pincode,
                paymentMethod = binding.paymentMethod,
                shop_title = binding.storeName,
                store_name = binding.storeName,
                mobile = binding.mobile)






        binding.name.addTextChangedListener(
            TextChangeListernerImple(object : TextChangeListernerImple.TextChangeListerner {
                override fun onTextChange(text: String?) {

                    sellerViewModel.checkInputEmptyFields(
                        name = binding.name,
                        email = binding.email,
                        password = binding.password,
                        address = binding.address,
                        pincode = binding.pincode,
                        paymentMethod = binding.paymentMethod,
                        shop_title = binding.storeName,
                        store_name = binding.storeName,
                        mobile = binding.mobile
                    )

                }
            })
        )

        binding.email.addTextChangedListener(
            TextChangeListernerImple(object : TextChangeListernerImple.TextChangeListerner {
                override fun onTextChange(text: String?) {

                    sellerViewModel.checkInputEmptyFields(
                        name = binding.name,
                        email = binding.email,
                        password = binding.password,
                        address = binding.address,
                        pincode = binding.pincode,
                        paymentMethod = binding.paymentMethod,
                        shop_title = binding.storeName,
                        store_name = binding.storeName,
                        mobile = binding.mobile
                    )

                }
            })
        )

        binding.password.addTextChangedListener(
            TextChangeListernerImple(object : TextChangeListernerImple.TextChangeListerner {
                override fun onTextChange(text: String?) {

                    sellerViewModel.checkInputEmptyFields(
                        name = binding.name,
                        email = binding.email,
                        password = binding.password,
                        address = binding.address,
                        pincode = binding.pincode,
                        paymentMethod = binding.paymentMethod,
                        shop_title = binding.storeName,
                        store_name = binding.storeName,
                        mobile = binding.mobile
                    )

                }
            })
        )

        binding.address.addTextChangedListener(
            TextChangeListernerImple(object : TextChangeListernerImple.TextChangeListerner {
                override fun onTextChange(text: String?) {

                    sellerViewModel.checkInputEmptyFields(
                        name = binding.name,
                        email = binding.email,
                        password = binding.password,
                        address = binding.address,
                        pincode = binding.pincode,
                        paymentMethod = binding.paymentMethod,
                        shop_title = binding.storeName,
                        store_name = binding.storeName,
                        mobile = binding.mobile
                    )

                }
            })
        )
        binding.pincode.addTextChangedListener(
            TextChangeListernerImple(object : TextChangeListernerImple.TextChangeListerner {
                override fun onTextChange(text: String?) {

                    sellerViewModel.checkInputEmptyFields(
                        name = binding.name,
                        email = binding.email,
                        password = binding.password,
                        address = binding.address,
                        pincode = binding.pincode,
                        paymentMethod = binding.paymentMethod,
                        shop_title = binding.storeName,
                        store_name = binding.storeName,
                        mobile = binding.mobile
                    )

                }
            })
        )
        binding.mobile.addTextChangedListener(
            TextChangeListernerImple(object : TextChangeListernerImple.TextChangeListerner {
                override fun onTextChange(text: String?) {

                    sellerViewModel.checkInputEmptyFields(
                        name = binding.name,
                        email = binding.email,
                        password = binding.password,
                        address = binding.address,
                        pincode = binding.pincode,
                        paymentMethod = binding.paymentMethod,
                        shop_title = binding.storeName,
                        store_name = binding.storeName,
                        mobile = binding.mobile
                    )

                }
            })
        )
        binding.paymentMethod.addTextChangedListener(
            TextChangeListernerImple(object : TextChangeListernerImple.TextChangeListerner {
                override fun onTextChange(text: String?) {

                    sellerViewModel.checkInputEmptyFields(
                        name = binding.name,
                        email = binding.email,
                        password = binding.password,
                        address = binding.address,
                        pincode = binding.pincode,
                        paymentMethod = binding.paymentMethod,
                        shop_title = binding.storeName,
                        store_name = binding.storeName,
                        mobile = binding.mobile
                    )

                }
            })
        )

        binding.storeName.addTextChangedListener(
            TextChangeListernerImple(object : TextChangeListernerImple.TextChangeListerner {
                override fun onTextChange(text: String?) {

                    sellerViewModel.checkInputEmptyFields(
                        name = binding.name,
                        email = binding.email,
                        password = binding.password,
                        address = binding.address,
                        pincode = binding.pincode,
                        paymentMethod = binding.paymentMethod,
                        shop_title = binding.storeName,
                        store_name = binding.storeName,
                        mobile = binding.mobile
                    )

                }
            })
        )






        binding.submitBtn.setOnClickListener {
            Toast.makeText(context, "Please upload your store image", Toast.LENGTH_SHORT).show()

        }



        lifecycleScope.launchWhenStarted {

            var cateogoryList = listOf<CategoryResponse>()
            var cityList = listOf<City>()
            var brandList = listOf<HomeResponse>()

            var cateogoryIndex = 0
            var cityListIndex = 0
            var brandListIndex = 0



            sellerViewModel.emptyFieldsEvent.observe(viewLifecycleOwner, Observer {

                when (it) {
                    true -> binding.submitBtn.isEnabled = false
                    false -> binding.submitBtn.isEnabled = true
                }

            })



            categoryViewModel.categoryEvent.observe(viewLifecycleOwner, Observer {
                val categoryNameList = arrayListOf<String>()

                when (it) {

                    is CateogoryViewModel.CategoryEvent.Success -> {

                        cateogoryList = it.resultText
                        it.resultText.forEach { it ->
                            categoryNameList.add(it.title)
                        }
                        binding.categorySpinner.apply {
                            adapter = ArrayAdapter<String>(
                                context,
                                android.R.layout.simple_list_item_1,
                                categoryNameList
                            )
                        }


                        binding.categorySpinner.onItemSelectedListener =
                            object : AdapterView.OnItemSelectedListener {
                                override fun onNothingSelected(parent: AdapterView<*>?) {

                                }

                                override fun onItemSelected(
                                    parent: AdapterView<*>?,
                                    view: View?,
                                    position: Int,
                                    id: Long
                                ) {

                                    cateogoryIndex = position

                                }

                            }


                    }
                    is CateogoryViewModel.CategoryEvent.Failure ->
                        Toast.makeText(context, "Some error occured", Toast.LENGTH_SHORT).show()

                    else -> Unit
                }
            })

            homeViewModel.homeEvent.observe(viewLifecycleOwner, Observer {

                val brandNameList = arrayListOf<String>()

                when (it) {

                    is HomeViewModel.HomeEvent.SuccessBrands -> {

                        brandList = it.resultText
                        it.resultText.forEach { it ->
                            brandNameList.add(it.title)
                        }

                        binding.brandSpinner.apply {
                            adapter = ArrayAdapter<String>(
                                context,
                                android.R.layout.simple_list_item_1,
                                brandNameList
                            )
                        }


                        binding.brandSpinner.onItemSelectedListener =
                            object : AdapterView.OnItemSelectedListener {
                                override fun onNothingSelected(parent: AdapterView<*>?) {

                                }

                                override fun onItemSelected(
                                    parent: AdapterView<*>?,
                                    view: View?,
                                    position: Int,
                                    id: Long
                                ) {

                                    brandListIndex = position

                                }

                            }

                    }


                    is HomeViewModel.HomeEvent.Failure -> {
                        Toast.makeText(context, "Error occured", Toast.LENGTH_SHORT).show()
                    }

                    else -> HomeViewModel.HomeEvent.Empty
                }
            })


            sellerViewModel.sellerManagerEvent.observe(viewLifecycleOwner, Observer {
                val cityNameList = arrayListOf<String>()

                when (it) {
                    is SellerManagerViewModel.SellerManagerEvent.SuccessCity -> {

                        cityList = it.cityList
                        it.cityList.forEach { it ->
                            cityNameList.add(it.city_name)
                        }
                        binding.citySpinner.apply {
                            adapter = ArrayAdapter<String>(
                                context, android.R.layout.simple_list_item_1,
                                cityNameList
                            )

                        }

                        binding.citySpinner.onItemSelectedListener =
                            object : AdapterView.OnItemSelectedListener {
                                override fun onNothingSelected(parent: AdapterView<*>?) {

                                }

                                override fun onItemSelected(
                                    parent: AdapterView<*>?,
                                    view: View?,
                                    position: Int,
                                    id: Long
                                ) {

                                    cityListIndex = position

                                }

                            }
                    }
                    is SellerManagerViewModel.SellerManagerEvent.Failure ->
                        Toast.makeText(context, "Server error occured", Toast.LENGTH_SHORT).show()

                    is SellerManagerViewModel.SellerManagerEvent.SuccessSeller -> {
                        Snackbar.make(
                            binding.root, resources.getString(R.string.request_submitted),
                            Snackbar.LENGTH_SHORT
                        ).setAction(resources.getString(R.string.ok), {
                        }).show()


                    }
                    is SellerManagerViewModel.SellerManagerEvent.Loading ->
                        Toast.makeText(context, "Loading....", Toast.LENGTH_SHORT).show()

                    else -> SellerManagerViewModel.SellerManagerEvent.Empty

                }


            })



            sellerViewModel.imageFile.observe(viewLifecycleOwner, Observer { file ->


                binding.submitBtn.setOnClickListener {


                    sellerViewModel.applyAsSeller(
                        file,
                        name = binding.sellerName.text.toString().trim(),
                        password = binding.password.text.toString().trim(),
                        email = binding.email.text.toString().trim(),
                        store_name = binding.storeName.text.toString().trim(),
                        city = cityList[cityListIndex].city_id,
                        brand = brandList[brandListIndex].id,
                        category = cateogoryList[cateogoryIndex].id,
                        shop_title = binding.storeName.text.toString().trim(),
                        mobile = binding.mobile.text.toString().trim(),
                        Payment_method = binding.paymentMethod.text.toString().trim(),
                        pincode = binding.pincode.text.toString().trim(),
                        address = binding.address.text.toString().trim()

                    )


                }

            })


        }


        binding.profileImage.setOnClickListener {

            ImagePicker.with(this)
                .galleryOnly()
                .compress(1024)
                .start()

        }



        binding.submitBtn.setOnClickListener {

        }

        return binding.root


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            val fileUri = data?.data
            binding.profileImage.setImageURI(fileUri)
            //You can get File object from intent
            val file: File = ImagePicker.getFile(data)!!
            this.file = file

            //You can also get File Path from intent
            sellerViewModel.sendImageFile(file)


            val filePath: String = ImagePicker.getFilePath(data)!!
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(context, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(context, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }
}