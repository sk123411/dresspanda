package com.maestros.dresspanda.seller

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.google.android.material.snackbar.Snackbar
import com.maestros.dresspanda.R
import com.maestros.dresspanda.databinding.FragmentApplyManagerBinding
import com.maestros.dresspanda.home.ui.brand.HomeResponse
import com.maestros.dresspanda.seller.repository.SellerManagerViewModel
import com.maestros.dresspanda.util.TextChangeListernerImple
import dagger.hilt.android.AndroidEntryPoint

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ApplyManagerFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
class ApplyManagerFragment : Fragment() {
    val sellerViewModel: SellerManagerViewModel by viewModels()

    lateinit var binding:FragmentApplyManagerBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentApplyManagerBinding.inflate(inflater)
        sellerViewModel.getCities()

        binding.submitBtn.isEnabled = sellerViewModel.checkInputEmptyFields(
            name = binding.nameEdit,
            email = binding.emailEdit,
            password = binding.passwordEdit,
            address = binding.addressEdit,
            pincode = binding.pincodeEdit,
            paymentMethod = binding.pincodeEdit,
            shop_title = binding.pincodeEdit,
            store_name = binding.pincodeEdit,
            mobile = binding.mobileEdit
        )


        binding.nameEdit.addTextChangedListener(
            TextChangeListernerImple(object : TextChangeListernerImple.TextChangeListerner {
                override fun onTextChange(text: String?) {

                    sellerViewModel.checkInputEmptyFields(
                        name = binding.nameEdit,
                        email = binding.emailEdit,
                        password = binding.passwordEdit,
                        address = binding.addressEdit,
                        pincode = binding.pincodeEdit,
                        paymentMethod = binding.pincodeEdit,
                        shop_title = binding.pincodeEdit,
                        store_name = binding.pincodeEdit,
                        mobile = binding.mobileEdit
                    )

                }
            })
        )

        binding.emailEdit.addTextChangedListener(
            TextChangeListernerImple(object : TextChangeListernerImple.TextChangeListerner {
                override fun onTextChange(text: String?) {

                    sellerViewModel.checkInputEmptyFields(
                        name = binding.nameEdit,
                        email = binding.emailEdit,
                        password = binding.passwordEdit,
                        address = binding.addressEdit,
                        pincode = binding.pincodeEdit,
                        paymentMethod = binding.pincodeEdit,
                        shop_title = binding.pincodeEdit,
                        store_name = binding.pincodeEdit,
                        mobile = binding.mobileEdit

                    )

                }
            })
        )

        binding.passwordEdit.addTextChangedListener(
            TextChangeListernerImple(object : TextChangeListernerImple.TextChangeListerner {
                override fun onTextChange(text: String?) {

                    sellerViewModel.checkInputEmptyFields(
                        name = binding.nameEdit,
                        email = binding.emailEdit,
                        password = binding.passwordEdit,
                        address = binding.addressEdit,
                        pincode = binding.pincodeEdit,
                        paymentMethod = binding.pincodeEdit,
                        shop_title = binding.pincodeEdit,
                        store_name = binding.pincodeEdit,
                        mobile = binding.mobileEdit

                    )

                }
            })
        )

        binding.addressEdit.addTextChangedListener(
            TextChangeListernerImple(object : TextChangeListernerImple.TextChangeListerner {
                override fun onTextChange(text: String?) {

                    sellerViewModel.checkInputEmptyFields(
                        name = binding.nameEdit,
                        email = binding.emailEdit,
                        password = binding.passwordEdit,
                        address = binding.addressEdit,
                        pincode = binding.pincodeEdit,
                        paymentMethod = binding.pincodeEdit,
                        shop_title = binding.pincodeEdit,
                        store_name = binding.pincodeEdit,
                        mobile = binding.mobileEdit
                    )

                }
            })
        )
        binding.pincodeEdit.addTextChangedListener(
            TextChangeListernerImple(object : TextChangeListernerImple.TextChangeListerner {
                override fun onTextChange(text: String?) {

                    sellerViewModel.checkInputEmptyFields(
                        name = binding.nameEdit,
                        email = binding.emailEdit,
                        password = binding.passwordEdit,
                        address = binding.addressEdit,
                        pincode = binding.pincodeEdit,
                        paymentMethod = binding.pincodeEdit,
                        shop_title = binding.pincodeEdit,
                        store_name = binding.pincodeEdit,
                        mobile = binding.mobileEdit
                    )

                }
            })
        )
        binding.mobileEdit.addTextChangedListener(
            TextChangeListernerImple(object : TextChangeListernerImple.TextChangeListerner {
                override fun onTextChange(text: String?) {

                    sellerViewModel.checkInputEmptyFields(
                        name = binding.nameEdit,
                        email = binding.emailEdit,
                        password = binding.passwordEdit,
                        address = binding.addressEdit,
                        pincode = binding.pincodeEdit,
                        paymentMethod = binding.pincodeEdit,
                        shop_title = binding.pincodeEdit,
                        store_name = binding.pincodeEdit,
                        mobile = binding.mobileEdit
                    )

                }
            })
        )






        lifecycleScope.launchWhenStarted {

            var cityList = listOf<City>()
            var cityListIndex = 0

            sellerViewModel.sellerManagerEvent.observe(viewLifecycleOwner, Observer {
                val cityNameList = arrayListOf<String>()

                when (it) {
                    is SellerManagerViewModel.SellerManagerEvent.SuccessCity -> {

                        cityList = it.cityList
                        it.cityList.forEach { it ->
                            cityNameList.add(it.city_name)
                        }
                        binding.citySpinner.apply {
                            adapter = ArrayAdapter<String>(
                                context, android.R.layout.simple_list_item_1,
                                cityNameList
                            )

                        }

                        binding.citySpinner.onItemSelectedListener =
                            object : AdapterView.OnItemSelectedListener {
                                override fun onNothingSelected(parent: AdapterView<*>?) {

                                }

                                override fun onItemSelected(
                                    parent: AdapterView<*>?,
                                    view: View?,
                                    position: Int,
                                    id: Long
                                ) {

                                    cityListIndex = position

                                }

                            }
                    }
                    is SellerManagerViewModel.SellerManagerEvent.Failure ->
                        Toast.makeText(context, "Server error occured", Toast.LENGTH_SHORT).show()

                    is SellerManagerViewModel.SellerManagerEvent.SuccessManager -> {
                        Snackbar.make(
                            binding.root, resources.getString(R.string.request_submitted),
                            Snackbar.LENGTH_SHORT
                        ).setAction(resources.getString(R.string.ok), {
                        }).show()

                    }
                    is SellerManagerViewModel.SellerManagerEvent.Loading ->
                        Toast.makeText(context, "Loading....", Toast.LENGTH_SHORT).show()

                    else -> SellerManagerViewModel.SellerManagerEvent.Empty

                }


            })
            sellerViewModel.emptyFieldsEvent.observe(viewLifecycleOwner, Observer {

                when (it) {
                    true -> binding.submitBtn.isEnabled = false
                    false -> {
                        binding.submitBtn.isEnabled = true
                        binding.submitBtn.setOnClickListener {
                            sellerViewModel.applyAsManager(name =
                            binding.nameEdit.text.toString().trim(),email = binding.emailEdit.text.toString().trim(),
                            password = binding.passwordEdit.text.toString().trim(),address = binding.addressEdit.text.toString().trim(),
                            pincode = binding.pincodeEdit.text.toString().trim(),mobile = binding.mobileEdit.text.toString(),
                            city = cityList[cityListIndex].city_id)

                        }
                    }
                }

            })


        }













        return binding.root
    }



}