package com.maestros.dresspanda.seller.repository

import android.util.Log
import com.maestros.dresspanda.seller.City
import com.maestros.dresspanda.seller.repository.model.ManagerResponse
import com.maestros.dresspanda.seller.repository.model.SellerResponse
import com.maestros.dresspanda.util.Resource
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import javax.inject.Inject

class SellerImplementation @Inject constructor(
    val sellerAPI: SellerAPI):Seller{
    override suspend fun getCities(action:String?): Resource<List<City>> {
        return try {
            val response = sellerAPI.getCities(action)
            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())
                Resource.Success(result.list)

            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }

    }

    override suspend fun applyAsSeller(
        action: String?,
        image: MultipartBody.Part?,
        name: RequestBody?,
        email: RequestBody?,
        password: RequestBody?,
        address: RequestBody?,
        pincode: RequestBody?,
        Payment_method: RequestBody?,
        category: RequestBody?,
        shop_title: RequestBody?,
        mobile: RequestBody?,
        store_name: RequestBody?,
        brand: RequestBody?,
        city: RequestBody?
    ):Resource<SellerResponse> {
        return try {
            val response = sellerAPI.applyAsSeller(action,image!!,name!!,email!!,password!!,address!!,
            pincode!!, Payment_method!!,category!!,shop_title!!,mobile!!,store_name!!,brand!!,city!!)

            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())
                Resource.Success(result)

            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }


    }

    override suspend fun applyAsManager(
        action: String?,
        body: HashMap<String, String>
    ): Resource<ManagerResponse> {

        return try {
            val response = sellerAPI.applyAsManager(action,body)

            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())
                Resource.Success(result)

            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }




    }


}