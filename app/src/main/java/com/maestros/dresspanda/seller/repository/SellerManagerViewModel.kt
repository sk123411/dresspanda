package com.maestros.dresspanda.seller.repository

import android.content.Context
import android.net.Uri
import android.text.TextUtils
import android.util.Log
import android.widget.EditText
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.maestros.dresspanda.R
import com.maestros.dresspanda.seller.City
import com.maestros.dresspanda.seller.repository.model.ManagerResponse
import com.maestros.dresspanda.seller.repository.model.SellerResponse
import com.maestros.dresspanda.util.Constant
import kotlinx.coroutines.launch
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.Part
import retrofit2.http.Query
import java.io.File
import javax.annotation.Resource

class SellerManagerViewModel @ViewModelInject constructor(
    private val seller: Seller,
    private val context: Context
) : ViewModel() {

    private val sellerManagerEvent_ = MutableLiveData<SellerManagerEvent>()

    val sellerManagerEvent: LiveData<SellerManagerEvent>
        get() = sellerManagerEvent_





    private val emptyFieldsEvent_ = MutableLiveData<Boolean>()

    val emptyFieldsEvent: LiveData<Boolean>
        get() = emptyFieldsEvent_


    private val imageFile_ = MutableLiveData<File>()



    val imageFile: LiveData<File>
        get() = imageFile_

    sealed class SellerManagerEvent {

        class SuccessCity(val cityList: List<City>) : SellerManagerEvent()
        class SuccessSeller(val sellerResponse: SellerResponse) : SellerManagerEvent()
        class SuccessManager(val sellerResponse: ManagerResponse) : SellerManagerEvent()

        class Failure(message: String?) : SellerManagerEvent()
        object Empty : SellerManagerEvent()

        object Loading : SellerManagerEvent()

    }




    fun getCities() {
        sellerManagerEvent_.value = SellerManagerEvent.Loading

        viewModelScope.launch {


            val response = seller.getCities("show_city")

            when (response) {

                is com.maestros.dresspanda.util.Resource.Success ->
                    sellerManagerEvent_.value = SellerManagerEvent.SuccessCity(response.data!!)

                is com.maestros.dresspanda.util.Resource.Error ->
                    sellerManagerEvent_.value = SellerManagerEvent.Failure(response.message)


            }
        }



    }


    fun applyAsSeller(
        image: File,
        name: String?,
        email: String?,
        password: String?,
        address: String?,
        pincode: String?,
        Payment_method: String?,
        category: String?,
        shop_title: String?,
        mobile: String?,
        store_name: String?,
        brand: String?,
        city: String?
    ) {

        sellerManagerEvent_.value = SellerManagerViewModel.SellerManagerEvent.Loading

        val storeImage:RequestBody =  RequestBody.create(
            "image/jpg".toMediaTypeOrNull(),
            image)

        val storeImageBody:MultipartBody.Part =
                MultipartBody.Part.createFormData(Constant.STORE_IMAGE,
                    image.getName(), storeImage)



        Log.d("IMAGEEEEEEE", "SSS" +storeImageBody.body.toString())

        Log.d("IMAGEEEEEEE", image.name + "::PATH" +image.absoluteFile.toURI().toString())



        val nameBody = getMultiPartFormRequestBody(name)
        val emailBody = getMultiPartFormRequestBody(email)
        val passwordBody = getMultiPartFormRequestBody(password)
        val addressBody = getMultiPartFormRequestBody(address)
        val pincodeBody = getMultiPartFormRequestBody(pincode)
        val Payment_methodBody = getMultiPartFormRequestBody(Payment_method)
        val categoryBody = getMultiPartFormRequestBody(category)
        val shop_titleBody = getMultiPartFormRequestBody(shop_title)
        val mobileBody = getMultiPartFormRequestBody(mobile)
        val brandBody = getMultiPartFormRequestBody(brand)
        val cityBody = getMultiPartFormRequestBody(city)
        val store_nameBody = getMultiPartFormRequestBody(store_name)


        viewModelScope.launch {
            val response = seller.applyAsSeller(
                action = "apply_as_seller",
                image = storeImageBody,
                name = nameBody,
                email = emailBody,
                password = passwordBody,
                address = addressBody,
                pincode = pincodeBody,
                Payment_method = Payment_methodBody,
                category = categoryBody,
                shop_title = shop_titleBody,
                mobile = mobileBody,
                brand = brandBody,
                city = cityBody,
                store_name = store_nameBody)

            when(response){

                is com.maestros.dresspanda.util.Resource.Success ->
                    sellerManagerEvent_.value = SellerManagerEvent.SuccessSeller(response.data!!)

                is com.maestros.dresspanda.util.Resource.Error ->
                    sellerManagerEvent_.value = SellerManagerEvent.Failure("response.data!!")

            }

        }


    }


    fun applyAsManager(   name: String?,
                          email: String?,
                          password: String?,
                          address: String?,
                          pincode: String?,
                          mobile: String?,
                          city: String?
                          ){

        sellerManagerEvent_.value = SellerManagerViewModel.SellerManagerEvent.Loading

        viewModelScope.launch{

            val data = HashMap<String,String>()
            data.put("name", name!!)
            data.put("email", email!!)
            data.put("password", password!!)
            data.put("address", address!!)
            data.put("pincode", pincode!!)
            data.put("mobile", mobile!!)
            data.put("city", city!!)


            val response = seller.applyAsManager("apply_as_manager",data)


            when(response){

                is com.maestros.dresspanda.util.Resource.Success ->
                    sellerManagerEvent_.value = SellerManagerEvent.SuccessManager(response.data!!)

                is com.maestros.dresspanda.util.Resource.Error ->
                    sellerManagerEvent_.value = SellerManagerEvent.Failure("response.data!!")

            }

        }


    }





    fun checkInputEmptyFields(name:EditText?,email:EditText?,password: EditText?, address:EditText?,
    pincode:EditText?,paymentMethod:EditText?,shop_title:EditText?,mobile: EditText?,
    store_name: EditText?):Boolean{

     if (TextUtils.isEmpty(name?.text.toString().trim())){
         name?.setError(context.resources.getString(R.string.emptyName))
         emptyFieldsEvent_.value = true
         return false
     }else if (TextUtils.isEmpty(email?.text.toString().trim())){
         email?.setError(context.resources.getString(R.string.emptyName))
         emptyFieldsEvent_.value = true

         return false
     }else if (TextUtils.isEmpty(password?.text.toString().trim())){
         password?.setError(context.resources.getString(R.string.emptyName))
         emptyFieldsEvent_.value = true

         return false
     }else if (TextUtils.isEmpty(address?.text.toString().trim())){
         address?.setError(context.resources.getString(R.string.emptyName))
         emptyFieldsEvent_.value = true

         return false
     }else if (TextUtils.isEmpty(pincode?.text.toString().trim())){
         pincode?.setError(context.resources.getString(R.string.emptyName))
         emptyFieldsEvent_.value = true

         return false
     }else if (TextUtils.isEmpty(paymentMethod?.text.toString().trim())){
         paymentMethod?.setError(context.resources.getString(R.string.emptyName))
         emptyFieldsEvent_.value = true

         return false
     }else if (TextUtils.isEmpty(shop_title?.text.toString().trim())){
         shop_title?.setError(context.resources.getString(R.string.emptyName))
         emptyFieldsEvent_.value = true

         return false
     }else if (TextUtils.isEmpty(mobile?.text.toString().trim())){
         mobile?.setError(context.resources.getString(R.string.emptyName))
         emptyFieldsEvent_.value = true

         return false
     }else if (TextUtils.isEmpty(store_name?.text.toString().trim())){
         store_name?.setError(context.resources.getString(R.string.emptyName))
         emptyFieldsEvent_.value = true
         return false
     }
        emptyFieldsEvent_.value = false

        return true
    }

    fun getMultiPartFormRequestBody(tag:String?):RequestBody{
        return RequestBody.create(MultipartBody.FORM, tag!!)

    }

    fun sendImageFile(file: File) {

        imageFile_.value = file

    }



}