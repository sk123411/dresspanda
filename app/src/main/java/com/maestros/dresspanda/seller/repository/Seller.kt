package com.maestros.dresspanda.seller.repository

import com.maestros.dresspanda.seller.City
import com.maestros.dresspanda.seller.repository.model.ManagerResponse
import com.maestros.dresspanda.seller.repository.model.SellerResponse
import com.maestros.dresspanda.util.Resource
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*
import java.io.File

interface Seller {


    suspend fun getCities(action: String?): Resource<List<City>>


    suspend fun applyAsSeller(
        action: String?,
        image: MultipartBody.Part?,
        name: RequestBody?,
        email: RequestBody?,
        password: RequestBody?,
        address: RequestBody?,
        pincode: RequestBody?,
        Payment_method: RequestBody?,
        category: RequestBody?,
        shop_title: RequestBody?,
        mobile: RequestBody?,
        store_name: RequestBody?,
        brand: RequestBody?,
        city: RequestBody?
    ): Resource<SellerResponse>


    suspend fun applyAsManager(@Query("action") action:String?,
                               @FieldMap body: HashMap<String,String>)
            : Resource<ManagerResponse>




}