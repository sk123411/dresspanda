package com.maestros.dresspanda.data.shop

import android.util.Log
import androidx.room.*
import com.maestros.dresspanda.home.ui.brand.HomeResponse
import com.maestros.dresspanda.seller.repository.model.SellerResponse

@Dao
interface ShopsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertBrands(brands: List<SellerResponse>)



    @Query("SELECT * FROM sellerresponse")
    suspend fun getAllShops():List<SellerResponse>


    @Update
    suspend fun updateBrands(brands: List<SellerResponse>)



    @Query("DELETE FROM sellerresponse")
    suspend fun deleteAll()




}