package com.maestros.dresspanda.data.product

import androidx.room.*
import com.maestros.dresspanda.home.ui.product.model.productdetail.ProductResponse

@Dao
interface ProductDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertProducts(brands: List<ProductResponse>)


    @Query("SELECT * FROM product")
    suspend fun getAllProducts():List<ProductResponse>


    @Update
    suspend fun updateBrands(brands: List<ProductResponse>)


    @Query("DELETE FROM product")
    suspend fun deleteAll()




}