package com.maestros.dresspanda.data.wishlist

import androidx.room.*
import com.maestros.dresspanda.home.ui.wishlist.model.WishlistShop

@Dao
interface ShopWishlistDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertProduct(wishlistShop: WishlistShop):Long

    @Delete
    suspend fun deleteShop(wishlistShop: WishlistShop)


    @Query("SELECT * FROM wishlistShop")
    suspend fun getAllShops():List<WishlistShop>

    @Query("SELECT * FROM wishlistShop where id=:id")
    suspend fun getItemById(id:String):List<WishlistShop>

    @Update
    suspend fun updateShop(product: WishlistShop)


    @Query("DELETE FROM wishlistShop")
    suspend fun deleteAll()


    suspend fun insertOrUpdate(product: WishlistShop):Long{
        val item = getItemById(product.id)
        var successEvent:Long=0

        if(item.isEmpty()){
          successEvent = insertProduct(product)

        }else {
            updateShop(product)
        }
        return successEvent
    }

    suspend fun isShopOnDB(product: WishlistShop):Boolean {
        val item = getItemById(product.id)

        if (!item.isEmpty()) {
            return true
        }
        return false
    }




}