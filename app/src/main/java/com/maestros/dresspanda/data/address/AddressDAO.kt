package com.maestros.dresspanda.data.address

import androidx.room.*
import com.maestros.dresspanda.home.ui.address.Address
import com.maestros.dresspanda.home.ui.product.model.productdetail.ProductResponse

@Dao
interface AddressDAO {


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAddress(address:Address)


    @Query("SELECT * FROM address")
    suspend fun getAllAddresses():List<Address>


    @Update
    suspend fun updateAddress(address:Address)



    @Delete
    suspend fun deleteAddress(address: Address)



    @Query("DELETE FROM product")
    suspend fun deleteAll()






}