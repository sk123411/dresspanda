package com.maestros.dresspanda.data

import androidx.room.Database
import androidx.room.RoomDatabase
import com.maestros.dresspanda.data.address.AddressDAO
import com.maestros.dresspanda.data.home.HomeBrandsDao
import com.maestros.dresspanda.data.product.ProductDAO
import com.maestros.dresspanda.data.wishlist.ProductWishlistDao
import com.maestros.dresspanda.data.shop.ShopsDao
import com.maestros.dresspanda.data.wishlist.ShopWishlistDao
import com.maestros.dresspanda.home.ui.address.Address
import com.maestros.dresspanda.home.ui.brand.HomeResponse
import com.maestros.dresspanda.home.ui.product.model.productdetail.ProductResponse
import com.maestros.dresspanda.home.ui.wishlist.model.WishlistProduct
import com.maestros.dresspanda.home.ui.wishlist.model.WishlistShop
import com.maestros.dresspanda.seller.repository.model.SellerResponse

@Database(
    entities = [
        ProductResponse::class,
        HomeResponse::class,
        SellerResponse::class,
        WishlistProduct::class,
        WishlistShop::class,
        Address::class
    ],
    version = 1
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun productWishlistDao(): ProductWishlistDao
    abstract fun homeBrands(): HomeBrandsDao
    abstract fun shops(): ShopsDao
    abstract fun productDao(): ProductDAO
    abstract fun wishlistShops(): ShopWishlistDao
    abstract fun addressDAO(): AddressDAO


}