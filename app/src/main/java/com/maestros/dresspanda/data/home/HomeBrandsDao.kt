package com.maestros.dresspanda.data.home

import android.util.Log
import androidx.room.*
import com.maestros.dresspanda.home.ui.brand.HomeResponse

@Dao
interface HomeBrandsDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertBrands(brands: List<HomeResponse>)


    @Query("SELECT * FROM home")
    suspend fun getAllBrands():List<HomeResponse>


    @Update
    suspend fun updateBrands(brands: List<HomeResponse>)


    @Query("DELETE FROM home")
    suspend fun deleteAll()



    suspend fun insertOrUpdate(brands: List<HomeResponse>){
//        if (getAllBrands().equals(brands)){
        if (getAllBrands().containsAll(brands)&&brands.containsAll(getAllBrands())){
            Log.d("XXXXXUPDATE", brands.get(0).title)
            updateBrands(brands)
        }else {
            Log.d("XXXXXINSERT", brands.get(0).title)

            insertBrands(brands)
        }

    }



}