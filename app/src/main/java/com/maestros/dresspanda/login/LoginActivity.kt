package com.maestros.dresspanda.login

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.akexorcist.localizationactivity.ui.LocalizationActivity
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.snackbar.Snackbar
import com.maestros.dresspanda.R
import com.maestros.dresspanda.databinding.ActivityLoginBinding
import com.maestros.dresspanda.databinding.ForgetPasswordLytBinding
import com.maestros.dresspanda.home.HomeActivity
import com.maestros.dresspanda.signup.SignUpActivity
import com.maestros.dresspanda.util.Constant
import dagger.hilt.android.AndroidEntryPoint
import dmax.dialog.SpotsDialog
import io.paperdb.Paper
import kotlinx.coroutines.flow.collect
import java.util.*
import kotlin.collections.HashMap

@AndroidEntryPoint
class LoginActivity : LocalizationActivity() {


    private val viewModel: LoginViewModel by viewModels()

    lateinit var binding: ActivityLoginBinding
    lateinit var progressDialog: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        setLanguage(Constant.getLanguage(context = applicationContext))

        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)



        val fromDetail = intent.getBooleanExtra("from_detail",false)
        val fromNews = intent.getBooleanExtra("from_news",false)
        val fromOrders= intent.getBooleanExtra("from_orders",false)





        progressDialog = ProgressDialog(this)
        binding.laGoogleLogin.setOnClickListener {
            val intent = viewModel.googleSignInClientIntent
            startActivityForResult(intent, 101)

        }







//        binding.hidePasswordImage.setOnClickListener {
//
//
//            if (binding.etPassword.inputType==InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD){
//                binding.hidePasswordImage.setImageResource(R.drawable.hide)
//                binding.etPassword.inputType==InputType.TYPE_TEXT_VARIATION_PASSWORD
//
//            }else {
//
//                binding.hidePasswordImage.setImageResource(R.drawable.visible)
//                binding.etPassword.inputType==InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
//
//            }
//
//
//        }


        binding.laFbLogin.setOnClickListener {
            viewModel.handleFbLogin(this)
        }


        viewModel.fbuser.observe(this, Observer {

            if (it != null) {

                viewModel.login(it)
            }

        })

        progressDialog.setTitle("Loading.....")
        binding.spSignButton.setOnClickListener {

            navigateToSignUp(it)

        }

        lifecycleScope.launchWhenStarted {

            //Collecting login event
            viewModel.login.collect {
                when (it) {

                    is LoginViewModel.LoginEvent.Loading -> {
                        progressDialog.show()

                    }
                    is LoginViewModel.LoginEvent.Success -> {


                        Log.d("DDDDDDEEEEEEEEEEEEE","::"+fromDetail)
                        if (fromDetail){


                            startActivity(Intent(this@LoginActivity, HomeActivity::class.java).putExtra("from_detail",true))




                        }else if (fromOrders) {

                            startActivity(Intent(this@LoginActivity, HomeActivity::class.java).putExtra("from_orders",true))
                            progressDialog.dismiss()

                        }else if (fromNews){
                            startActivity(Intent(this@LoginActivity, HomeActivity::class.java).putExtra("from_news",true))
                            progressDialog.dismiss()


                        }else {

                            startActivity(Intent(this@LoginActivity, HomeActivity::class.java))
                        }
                    }

                    is LoginViewModel.LoginEvent.Failure -> {
                        progressDialog.dismiss()
                    }

                    else -> Unit

                }
            }

            viewModel.forgetPassword.collect {
                when (it) {
                    is LoginViewModel.LoginEvent.Loading -> {
                        progressDialog.show()


                    }
                    is LoginViewModel.LoginEvent.ForgetPassword -> {
                        progressDialog.dismiss()


                    }

                    is LoginViewModel.LoginEvent.Failure -> {
                        progressDialog.dismiss()
                    }

                    else -> Unit

                }
            }
        }
    }


    fun proceedToLogin(view: View) {

        if (binding.etPassword.text.toString().isNotEmpty() && binding.etEmail.text.toString()
                .isNotEmpty()
        ) {
            viewModel.handleSignIN(
                binding.etEmail.text.toString().trim(),
                binding.etPassword.text.toString().trim()
            )

        } else Toast.makeText(applicationContext, "Email or Password is empty", Toast.LENGTH_SHORT)
            .show()


    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d("DATAAAA", "::" + data)
        if (requestCode == 101) {
            viewModel.handleSignInGoogle(data!!)
        } else {
            viewModel.fbCallbackManager.onActivityResult(requestCode, resultCode, data!!);

        }

    }

    fun navigateToSignUp(view: View) {

        startActivity(Intent(view.context, SignUpActivity::class.java))
    }


    fun navigateToHome() {

        startActivity(Intent(applicationContext, HomeActivity::class.java))
    }

    fun processForgetPassRequest(email: String?) {

        viewModel.processForgetPassword(email!!)

        Toast.makeText(
            applicationContext, "Email has been sent to your registered email address",
            Toast.LENGTH_SHORT
        ).show()

    }

    fun showForgetPasswordDialog(view: View) {

        val dialog = BottomSheetDialog(view.context)
        val binding = ForgetPasswordLytBinding.inflate(dialog.layoutInflater)
        dialog.apply {
            setContentView(binding.root)

        }

        binding.fpSendRequest.setOnClickListener {

            if (binding.spForgetPasswordEdit.text!!.isNotEmpty()) {
                dialog.dismiss()
                processForgetPassRequest(binding.spForgetPasswordEdit.text.toString())


            } else {
                Toast.makeText(applicationContext, "Please check input", Toast.LENGTH_SHORT).show()
                dialog.show()

            }
        }

        dialog.show()

    }

    override fun onBackPressed() {
        super.onBackPressed()

        finishAffinity()

    }



}