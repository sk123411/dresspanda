package com.maestros.dresspanda.login.repository

import com.maestros.dresspanda.login.model.ForgetPassword
import com.maestros.dresspanda.signup.model.Register
import com.maestros.dresspanda.util.Resource
import retrofit2.Response
import retrofit2.http.FieldMap
import retrofit2.http.POST
import retrofit2.http.Query

interface Login {
    suspend fun login(action: String?,data:Map<String,String>?): Resource<Register>

    suspend fun socialLogin(action: String?, @FieldMap data:Map<String,String>?): Resource<Register>


    suspend fun forgetPassword(action: String?, @FieldMap data:Map<String,String>?): Resource<ForgetPassword>

}