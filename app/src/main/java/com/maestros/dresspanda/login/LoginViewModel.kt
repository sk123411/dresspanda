package com.maestros.dresspanda.login

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.maestros.dresspanda.login.model.ForgetPassword
import com.maestros.dresspanda.login.model.SocialUser
import com.maestros.dresspanda.login.repository.Login
import com.maestros.dresspanda.signup.model.Register
import com.maestros.dresspanda.util.Constant
import com.maestros.dresspanda.util.Resource
import dmax.dialog.SpotsDialog
import io.paperdb.Paper
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import java.util.*
import kotlin.collections.HashMap


class LoginViewModel @ViewModelInject constructor(
    val googleSignInClient: GoogleSignInClient,
    val fbCallbackManager: CallbackManager,
    val context: Context,
    val loginRepo:Login
) : ViewModel(){


    val googleSignInClientIntent = googleSignInClient.signInIntent

    val fbuser_ = MutableLiveData<SocialUser>()

    val fbuser:LiveData<SocialUser>
        get() = fbuser_




    sealed class LoginEvent {
        class Success(val resultText: Register): LoginEvent()
        class Failure(val errorText: String): LoginEvent()
        object Loading : LoginEvent()
        object Empty : LoginEvent()
        class ForgetPassword(val resultText: com.maestros.dresspanda.login.model.ForgetPassword): LoginEvent()
        class UserLoggedIn(val status: Int):LoginEvent()
        class UserDetails(val userDetails: Register):LoginEvent()



    }


    private val _login = MutableStateFlow<LoginEvent>(LoginEvent.Empty)
    val login: StateFlow<LoginEvent> = _login


    private val _forgetPassword = MutableStateFlow<LoginEvent>(LoginEvent.Empty)
    val forgetPassword: StateFlow<LoginEvent> = _forgetPassword


    init {

        Paper.init(context)

    }





    fun handleSignIN( email:String?,  password:String?) {
        _login.value = LoginEvent.Loading

        viewModelScope.launch {

            if (email!!.isNotEmpty() && password!!.isNotEmpty()) {
                val map = HashMap<String, String>()
                map.put("email", email!!)
                map.put("password", password!!)
                val response = loginRepo.login("login", map)

                when(response){
                    is Resource.Success -> {
                        if ( response.data?.id!=null){
                            _login.value = LoginEvent.Success(response.data)
                            saveUserDetails(response.data)
                            saveUserLogin(1)


                        }else {
                            _login.value = LoginEvent.Failure(response.data?.result!!)
                            _login.value = LoginEvent.UserLoggedIn(0)


                        }
                        }
                    is Resource.Error ->  _login.value =  LoginEvent.Empty
                }

            }

        }
    }



    fun handleSignInGoogle(data:Intent){
        val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
        handleSignInResult(task)
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account: GoogleSignInAccount? = completedTask.getResult(ApiException::class.java)
            val fbUser = SocialUser()
            fbUser.aouth_id = account?.id
            fbUser.aouth_provider = "facebook"
            fbUser.firstName = account!!.givenName
            fbUser.email = account!!.email
            fbuser_.value = fbUser


        } catch (e: ApiException) {
            Log.e("XXXXXXX", "::"+e.message)
            Log.e("XXXXXXX", "::"+e.statusCode)
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
        }
    }


    fun handleFbLogin(activity: Activity) {
        LoginManager.getInstance().logInWithReadPermissions(activity, Arrays.asList("email"));
        LoginManager.getInstance().registerCallback(fbCallbackManager, object :
            FacebookCallback<LoginResult?> {
            override fun onSuccess(loginResult: LoginResult?) {
                Log.d("XXXXXXX", "Success Login")

                getUserProfile(loginResult?.accessToken, loginResult?.accessToken?.userId)

            }

            override fun onCancel() {

            }

            override fun onError(exception: FacebookException) {
                Log.e("Kdfhkjishf", "::"+exception.localizedMessage)
            }
        })
    }

    fun getUserProfile(token: AccessToken?, userId: String?) {

        val parameters = Bundle()
        val fbUser = SocialUser()

        parameters.putString(
            "fields",
            "id, first_name, middle_name, last_name, name, picture, email"
        )
        GraphRequest(token,
            "/$userId/",
            parameters,
            HttpMethod.GET,
            GraphRequest.Callback { response ->
                val jsonObject = response.jsonObject

                if (BuildConfig.DEBUG) {
                    FacebookSdk.setIsDebugEnabled(true)
                    FacebookSdk.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS)
                }
                // Facebook Id
                if (jsonObject.has("id")) {
                    val facebookId = jsonObject.getString("id")
                    Log.i("Facebook Id: ", facebookId.toString())
                    fbUser.aouth_id = facebookId
                    fbUser.aouth_provider = "Facebook"


                    //  id = facebookId.toString()
                } else {
                    Log.i("Facebook Id: ", "Not exists")
                    //  id = "Not exists"
                }

                // Facebook First Name
                if (jsonObject.has("first_name")) {
                    val facebookFirstName = jsonObject.getString("first_name")
                    Log.i("Facebook First Name: ", facebookFirstName)
                    fbUser.firstName = facebookFirstName
                } else {
                    Log.i("Facebook First Name: ", "Not exists")
                }

                // Facebook Middle Name
                if (jsonObject.has("middle_name")) {
                    val facebookMiddleName = jsonObject.getString("middle_name")
                    Log.i("Facebook Middle Name: ", facebookMiddleName)
                    //  middleName = facebookMiddleName
                } else {
                    Log.i("Facebook Middle Name: ", "Not exists")
                    // middleName = "Not exists"
                }

                // Facebook Last Name
                if (jsonObject.has("last_name")) {
                    val facebookLastName = jsonObject.getString("last_name")
                    Log.i("Facebook Last Name: ", facebookLastName)
                    fbUser.lastName = facebookLastName
                } else {
                    Log.i("Facebook Last Name: ", "Not exists")

                }
                // Facebook Name
                if (jsonObject.has("name")) {
                    val facebookName = jsonObject.getString("name")

                } else {
                    Log.i("Facebook Name: ", "Not exists")
                }
                // Facebook Profile Pic URL
                if (jsonObject.has("picture")) {
                    val facebookPictureObject = jsonObject.getJSONObject("picture")
                    if (facebookPictureObject.has("data")) {
                        val facebookDataObject = facebookPictureObject.getJSONObject("data")
                        if (facebookDataObject.has("url")) {
                            val facebookProfilePicURL = facebookDataObject.getString("url")
                            fbUser.profilePicture = facebookProfilePicURL
                            //   Log.i("Facebook Profile Pic URL: ", facebookProfilePicURL)
                            // picture = facebookProfilePicURL
                        }
                    }
                } else {
                    //  Log.i("Facebook Profile Pic URL: ", "Not exists")
                    //  picture = "Not exists"
                }

                // Facebook Email
                if (jsonObject.has("email")) {
                    val facebookEmail = jsonObject.getString("email")
                    Log.i("Facebook Email: ", facebookEmail)
                    fbUser.email = facebookEmail
                } else {
                    Log.i("Facebook Email: ", "Not exists")
                }
                fbuser_.value = fbUser

            }).executeAsync()

    }


    fun login(socialUser: SocialUser){
        _login.value = LoginEvent.Loading
        val map = HashMap<String,String>()
        map.put("name", socialUser.firstName!!)
        map.put("email", socialUser.email!!)
        map.put("aouth_id", socialUser.aouth_id!!)
        map.put("aouth_provider", socialUser.aouth_provider!!)

        viewModelScope.launch {
            val response = loginRepo.socialLogin("social_sign_up", map)

            when(response){

                is Resource.Success->{
                    if ( response.data?.id!=null){
                        _login.value = LoginEvent.Success(response.data!!)
                        saveUserLogin(1)
                        saveUserDetails(response.data)
                    }else {
                        _login.value = LoginEvent.Failure(response.data?.result!!)
                        _login.value = LoginEvent.UserLoggedIn(0)

                    }
                    }
                is Resource.Error->  {
                    _login.value=LoginEvent.Failure(response.message!!)
                    _login.value = LoginEvent.UserLoggedIn(0)

                }
            }

        }
    }


    fun processForgetPassword(email:String){

        _login.value = LoginEvent.Loading
        val map = HashMap<String,String>()
        map.put("email", email!!)

        viewModelScope.launch {

            val response = loginRepo.forgetPassword("forget_password",map)
            when(response){

                is Resource.Success->{
                    _login.value = LoginEvent.ForgetPassword(response.data!!)
                }
                is Resource.Error->  _login.value =LoginEvent.Failure(response.message!!)
            }
        }

    }



    fun saveUserLogin(value:Int?){
        Paper.book().write(Constant.USER_LOGGED_IN,value)
    }


    fun saveUserDetails(register:Register?){
        Paper.book().write(Constant.USER_DETAILS,register)
    }


    fun getUserLogin(){
        _login.value = LoginEvent.UserLoggedIn(Paper.book().read(Constant.USER_LOGGED_IN, 0))

    }


    fun getUserDetails(){
        _login.value = LoginEvent.UserDetails(Paper.book().read(Constant.USER_DETAILS))

    }






}


