package com.maestros.dresspanda.login.model

data class ForgetPassword(
    val email: String,
    val result: String
)