package com.maestros.dresspanda

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.maestros.dresspanda.databinding.FragmentContactUsBinding
import com.maestros.dresspanda.util.Constant


class ContactUsFragment : Fragment() {

    lateinit var binding:FragmentContactUsBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentContactUsBinding.inflate(layoutInflater)
        // Inflate the layout for this fragment

        binding.callRoot.setOnClickListener {

            Constant.showMessage("+919258956524", it.context)
        }

        binding.suppo.setOnClickListener {

            Constant.showMessage("+919258956524", it.context)
        }

        binding.gLocation.setOnClickListener {

            Constant.showMessage(resources.getString(R.string.addressDescription), it.context)
        }


        binding.fbb.setOnClickListener {

            Constant.showMessage("https://facebook.com/pages/dresspanda3455", it.context)
        }




        return binding.root
    }


}