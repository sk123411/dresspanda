package com.maestros.dresspanda.payment

import android.content.Context
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.sslwireless.sslcommerzlibrary.model.initializer.SSLCommerzInitialization
import com.sslwireless.sslcommerzlibrary.model.response.SSLCTransactionInfoModel
import com.sslwireless.sslcommerzlibrary.view.singleton.IntegrateSSLCommerz
import com.sslwireless.sslcommerzlibrary.viewmodel.listener.SSLCTransactionResponseListener


class SSlCommerz {


    companion object {

        const val TAG = "SSLCOMMERZ"


//        fun initializePayment(
//            context: Context?,
//            mandatoryFieldModel: MandatoryFieldModel,
//            sslCommerz: SSLCommerz
//        ) {
//
//            PayUsingSSLCommerz.getInstance()
//                .setData(context, mandatoryFieldModel, object : OnPaymentResultListener {
//                    override fun transactionSuccess(transactionInfo: TransactionInfo) {
//                        // If payment is success and risk label is 0 get payment details from here
//                        if (transactionInfo.riskLevel == "0") {
//                            Log.e(TAG, transactionInfo.valId)
//                            /* After successful transaction send this val id to your server and from
//                         your server you can call this api
//                         https://sandbox.sslcommerz.com/validator/api/validationserverAPI.php?val_id=yourvalid&store_id=yourstoreid&store_passwd=yourpassword
//                         if you call this api from your server side you will get all the details of the transaction.
//                         for more details visit:   www.tashfik.me
//            */
//                            sslCommerz.succcess(transactionInfo)
//
//
//                        } else {
//                            Log.e(
//                                TAG,
//                                "Transaction in risk. Risk Title : " + transactionInfo.riskTitle
//                            )
//                        }
//                    }
//
//                    override fun transactionFail(s: String) {
//                        Log.e(TAG, s)
//
//                        sslCommerz.errorState(s)
//                    }
//
//                    override fun error(errorCode: Int) {
//                        when (errorCode) {
//                            ErrorKeys.USER_INPUT_ERROR -> Log.e(TAG, "User Input Error")
//                            ErrorKeys.INTERNET_CONNECTION_ERROR -> Log.e(
//                                TAG,
//                                "Internet Connection Error"
//                            )
//                            ErrorKeys.DATA_PARSING_ERROR -> Log.e(TAG, "Data Parsing Error")
//                            ErrorKeys.CANCEL_TRANSACTION_ERROR -> Log.e(
//                                TAG,
//                                "User Cancel The Transaction"
//                            )
//                            ErrorKeys.SERVER_ERROR -> Log.e(TAG, "Server Error")
//                            ErrorKeys.NETWORK_ERROR -> Log.e(TAG, "Network Error")
//                        }
//                    }
//
//
//                })
//        }


        fun processPayment(
            context: AppCompatActivity?,
            sslCommerzInitialization: SSLCommerzInitialization,
            sslCommerz: SSLCommerzLatest
        ) {


            IntegrateSSLCommerz
                .getInstance(context)
                .addSSLCommerzInitialization(sslCommerzInitialization)
                .buildApiCall(object : SSLCTransactionResponseListener {
                    override fun transactionFail(p0: String?) {
                        sslCommerz.errorState(p0)
                    }

                    override fun merchantValidationError(p0: String?) {
                        sslCommerz.errorState(p0)
                    }

                    override fun transactionSuccess(p0: SSLCTransactionInfoModel?) {

                        sslCommerz.succcess(p0)
                    }

                })


        }

    }
}