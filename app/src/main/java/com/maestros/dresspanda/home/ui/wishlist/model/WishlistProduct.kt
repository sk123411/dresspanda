package com.maestros.dresspanda.home.ui.wishlist.model

import androidx.annotation.Nullable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(tableName = "wishlist")
data class WishlistProduct(
    @PrimaryKey(autoGenerate = true) val pid: Int,
    @SerializedName("brand")
    @Expose
    val MRP: String,
    @SerializedName("brand")
    @Expose
    val brand: String,
    @SerializedName("category_id")
    @Expose
    val category_id: String,
    @SerializedName("color")
    @Expose
    val color: String,

    @SerializedName("id")
    @Expose
    val id: String,


    @SerializedName("image")
    @Nullable
    @Expose
    val image: String,
    @SerializedName("path")
    @Nullable
    @Expose
    val path: String,
    @SerializedName("product_description")
    @Nullable
    @Expose
    val product_description: String,
    @SerializedName("product_title")
    @Nullable
    @Expose
    val product_title: String,
    @SerializedName("seller_id")
    @Nullable
    @Expose
    val seller_id: String,
    @SerializedName("selling_price")
    @Nullable
    @Expose
    val selling_price: String,
    @SerializedName("size")
    @Nullable
    @Expose
    val size: String,
    @SerializedName("stock")
    @Nullable
    @Expose
    val stock: String,
    @SerializedName("sub_category_id")
    @Nullable
    @Expose
    val sub_category_id: String
)