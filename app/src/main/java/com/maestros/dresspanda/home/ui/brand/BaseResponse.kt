package com.maestros.dresspanda.home.ui.brand

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class BaseResponse<T>(
    @SerializedName("data")
    @Expose
    val list: List<T>,

    @SerializedName("status")
    @Expose
    val status:Boolean?
)