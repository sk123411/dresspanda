package com.maestros.dresspanda.home.ui.shop.fragment

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.maestros.dresspanda.R
import com.maestros.dresspanda.databinding.FragmentShoplistBinding
import com.maestros.dresspanda.home.ui.home.HomeAdapter
import com.maestros.dresspanda.home.ui.shop.ShopViewModel
import com.maestros.dresspanda.seller.repository.model.SellerResponse
import com.maestros.dresspanda.util.Constant
import dagger.hilt.android.AndroidEntryPoint
import io.paperdb.Paper
import kotlinx.coroutines.launch


@AndroidEntryPoint
class ShoplistFragment : Fragment() {

    lateinit var binding: FragmentShoplistBinding
    val shopViewModel: ShopViewModel by viewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentShoplistBinding.inflate(inflater)
        Paper.init(activity)
        val fromBrandStatus = Paper.book().read<Int>(Constant.FROM_BRANDS, 0)

        if (fromBrandStatus==1){
            val id = arguments?.getString("brand_id")
            Log.d("TITLE", ":"+arguments?.getString("title") )
            if (id!=null){
                shopViewModel.getShopsByBrandId(id)
            }
        }else {
            shopViewModel.getShops()

        }




        lifecycleScope.launch {

            shopViewModel.shops.observe(viewLifecycleOwner, Observer {
                when (it) {

                    is ShopViewModel.ShopEvent.SuccessShops -> {

                        activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.GONE


                        binding.shopList.apply {
                            layoutManager =
                                GridLayoutManager(context, 2)
                            adapter =
                                HomeAdapter<SellerResponse>(
                                    it.resultText,
                                    R.layout.brand_item_list
                                )
                        }
                    }

                    is ShopViewModel.ShopEvent.Failure -> {
                        Toast.makeText(context, "Error occured", Toast.LENGTH_SHORT).show()
                    }

                    is ShopViewModel.ShopEvent.Loading -> {

                        activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.VISIBLE

                    }
                }
            })
        }
        // Inflate the layout for this fragment
        return binding.root
    }


}