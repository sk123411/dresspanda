package com.maestros.dresspanda.home.ui.cart.address

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.maestros.dresspanda.R
import com.maestros.dresspanda.databinding.AddressItemListBinding
import com.maestros.dresspanda.databinding.SelectAddressItemLytBinding
import com.maestros.dresspanda.home.ui.address.Address
import com.maestros.dresspanda.home.ui.address.repository.allAddressModel.Data
import com.maestros.dresspanda.home.ui.cart.repository.CartX
import com.maestros.dresspanda.util.Constant
import io.paperdb.Paper

class SelectAddressAdapter(val address:List<Data>):RecyclerView.Adapter<SelectAddressAdapter.MyViewHolder>(){
    class MyViewHolder(view:View):RecyclerView.ViewHolder(view) {
        val addaressItemBinding = SelectAddressItemLytBinding.bind(view)


        fun bindData(address: Data, addressAdapter: SelectAddressAdapter) {

            addaressItemBinding.addressTitle.setText(address.name)
            addaressItemBinding.addressContent.setText(addaressItemBinding.root.resources.getString(R.string.address)+": "+address.address)
            addaressItemBinding.addressBuilding.setText(addaressItemBinding.root.resources.getString(R.string.buildingNumber)+": "+address.address)
            addaressItemBinding.addressStreet.setText(addaressItemBinding.root.resources.getString(R.string.streetNumber)+": "+address.address)
            addaressItemBinding.addressPincode.setText(addaressItemBinding.root.resources.getString(R.string.pincode)+": "+address.address)
            addaressItemBinding.addressCity.setText(addaressItemBinding.root.resources.getString(R.string.city)+": "+address.address)



            addaressItemBinding.root.setOnClickListener {
                Constant.SelectedAddressID = address.id
                Paper.init(it.context)
                Paper.book().write(Constant.ADDRESS,address)

                addressAdapter.notifyDataSetChanged()

            }

        }

    }



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val binding = SelectAddressItemLytBinding.inflate(LayoutInflater.from(parent.context))

        return MyViewHolder(binding.root)
    }

    override fun getItemCount(): Int {
        return address.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        val data = address.get(position)

        if (Constant.SelectedAddressID.equals(data.id)){

            holder.addaressItemBinding.tickImage.setImageResource(R.drawable.right_gr)
        }else {
            holder.addaressItemBinding.tickImage.setImageResource(R.drawable.right_g)

        }



        holder.bindData(address.get(position),this)


    }

}