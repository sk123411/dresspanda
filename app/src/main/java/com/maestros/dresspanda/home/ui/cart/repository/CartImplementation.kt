package com.maestros.dresspanda.home.ui.cart.repository

import android.util.Log
import com.maestros.dresspanda.home.ui.product.model.productdetail.ProductResponse
import com.maestros.dresspanda.util.Resource
import org.json.JSONObject
import javax.inject.Inject

class CartImplementation @Inject constructor(val cartApi:CartAPI):Cart{
    override suspend fun showCartItems(
        show_cart: String?,
        map: HashMap<String, String>
    ): Resource<CartX> {
        return try {
            val response = cartApi.showCartItems(show_cart,map)
            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())
                Resource.Success(result)

            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }

    override suspend fun updateCartItem(
        update_cart: String?,
        map: HashMap<String, String>
    ): Resource<CartX> {
        return try {
            val response = cartApi.updateCartItem(update_cart,map)
            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())
                Resource.Success(result)

            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }

    override suspend fun deleteCartItem(
        delete_cart: String?,
        map: HashMap<String, String>
    ): Resource<JSONObject> {
        return try {
            val response = cartApi.deleteCartItem(delete_cart,map)
            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())
                Resource.Success(result)

            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }


}