package com.maestros.dresspanda.home.ui.select_address

import android.os.Bundle
import android.transition.PatternPathMotion
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.maestros.dresspanda.R
import com.maestros.dresspanda.databinding.AddAddressLayoutBinding
import com.maestros.dresspanda.databinding.SelectAddressLytBinding
import com.maestros.dresspanda.home.ui.address.AddressListFragment
import com.maestros.dresspanda.home.ui.address.AddressViewModel
import com.maestros.dresspanda.home.ui.address.repository.allAddressModel.Data
import com.maestros.dresspanda.home.ui.cart.address.SelectAddressAdapter
import com.maestros.dresspanda.home.ui.cart.repository.CartX
import com.maestros.dresspanda.signup.model.Register
import com.maestros.dresspanda.util.Constant
import com.maestros.dresspanda.util.Resource
import com.maestros.dresspanda.util.TextChangeListernerImple
import dagger.hilt.android.AndroidEntryPoint
import io.paperdb.Paper
import kotlinx.coroutines.launch

@AndroidEntryPoint
class SelectAddressFragment : Fragment() {


    val addressViewModel:AddressViewModel by viewModels()
    lateinit var binding:SelectAddressLytBinding
    var userID:String?=""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Paper.init(context)
        val register:Register = Paper.book().read(Constant.USER_DETAILS)
        userID = register.id
        addressViewModel.getAddress(userID)
        binding = SelectAddressLytBinding.inflate(layoutInflater)
        val map:CartX = Paper.book().read<CartX>(Constant.CART_MAP)

        lifecycleScope.launch {

            addressViewModel.addressEvent.observe(viewLifecycleOwner, Observer {

                when(it){

                    is AddressViewModel.AddressEvent.AddressFetched -> {
                        activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility =
                            View.GONE

                        binding.totalItems.setText("Total Items ${map.qntity}")
                        binding.totalAmount.setText("${Constant.CURRENCYSYMBOL} ${map.totalAmount}")



                        if (it.address.isNotEmpty()) {
                            binding.addAddressButton.visibility = View.GONE

                            binding.addressList.apply {

                                layoutManager = LinearLayoutManager(context)
                                adapter = SelectAddressAdapter(it.address)

                            }

                        }else {

                            binding.addAddressButton.visibility = View.VISIBLE

                            binding.addAddressButton.setOnClickListener {


                               openAddressDialog(data = Data(
                                    name = "",
                                    address ="",
                                    buldingNo = "",
                                    city = "",
                                    state = "",
                                    pincode = "binding.pincodeEdit.text.toString().trim()",
                                    landmark = "",
                                    streetNo ="",
                                    userId = "",
                                    id = ""

                                ),isUpdate = false)

                            }
                        }

                    }

                    is AddressViewModel.AddressEvent.Loading -> {

                        activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility =
                            View.VISIBLE

                    }
                }


            })

        }


        binding.continueButton.setOnClickListener {


            if(!Constant.SelectedAddressID.equals("")) {
                val bundle = Bundle();
                bundle.putString("id", Constant.SelectedAddressID)
                Navigation.findNavController(it).navigate(R.id.paymentFragment, bundle)

            }else {

                Toast.makeText(context, "Please selected the delivery address",Toast.LENGTH_LONG).show()
            }


        }


        return binding.root
    }

    fun openAddressDialog(data: Data,isUpdate:Boolean) {

        val bottomSheetDialog = BottomSheetDialog(requireActivity())
        val binding = AddAddressLayoutBinding.inflate(layoutInflater)
        bottomSheetDialog.setContentView(binding.root)
        bottomSheetDialog.show()

        if (isUpdate){

            binding.nameEdit.setText(data.name)
            binding.LandMartEdit.setText(data.landmark)
            binding.buildingNumberEdit.setText(data.buldingNo)
            binding.cityEdit.setText(data.city)
            binding.stateEdit.setText(data.state)
            binding.pincodeEdit.setText(data.pincode)
            binding.streetNumberEdit.setText(data.streetNo)
            binding.addressEdit.setText(data.address)
            binding.addButton.setText("Update")
        }

        binding.nameEdit.addTextChangedListener(TextChangeListernerImple(object :
            TextChangeListernerImple.TextChangeListerner {
            override fun onTextChange(text: String?) {
                addressViewModel.checkInputEmptyFields(
                    binding.nameEdit,
                    binding.buildingNumberEdit, binding.streetNumberEdit, binding.addressEdit,
                    binding.LandMartEdit, binding.pincodeEdit, binding.cityEdit, binding.stateEdit
                )


            }

        }))


        binding.buildingNumberEdit.addTextChangedListener(TextChangeListernerImple(object :
            TextChangeListernerImple.TextChangeListerner {
            override fun onTextChange(text: String?) {
                addressViewModel.checkInputEmptyFields(
                    binding.nameEdit,
                    binding.buildingNumberEdit, binding.streetNumberEdit, binding.addressEdit,
                    binding.LandMartEdit, binding.pincodeEdit, binding.cityEdit, binding.stateEdit
                )


            }

        }))


        binding.streetNumberEdit.addTextChangedListener(TextChangeListernerImple(object :
            TextChangeListernerImple.TextChangeListerner {
            override fun onTextChange(text: String?) {
                addressViewModel.checkInputEmptyFields(
                    binding.nameEdit,
                    binding.buildingNumberEdit, binding.streetNumberEdit, binding.addressEdit,
                    binding.LandMartEdit, binding.pincodeEdit, binding.cityEdit, binding.stateEdit
                )


            }

        }))

        binding.addressEdit.addTextChangedListener(TextChangeListernerImple(object :
            TextChangeListernerImple.TextChangeListerner {
            override fun onTextChange(text: String?) {
                addressViewModel.checkInputEmptyFields(
                    binding.nameEdit,
                    binding.buildingNumberEdit, binding.streetNumberEdit, binding.addressEdit,
                    binding.LandMartEdit, binding.pincodeEdit, binding.cityEdit, binding.stateEdit
                )


            }

        }))

        binding.LandMartEdit.addTextChangedListener(TextChangeListernerImple(object :
            TextChangeListernerImple.TextChangeListerner {
            override fun onTextChange(text: String?) {
                addressViewModel.checkInputEmptyFields(
                    binding.nameEdit,
                    binding.buildingNumberEdit, binding.streetNumberEdit, binding.addressEdit,
                    binding.LandMartEdit, binding.pincodeEdit, binding.cityEdit, binding.stateEdit
                )


            }

        }))

        binding.pincodeEdit.addTextChangedListener(TextChangeListernerImple(object :
            TextChangeListernerImple.TextChangeListerner {
            override fun onTextChange(text: String?) {
                addressViewModel.checkInputEmptyFields(
                    binding.nameEdit,
                    binding.buildingNumberEdit, binding.streetNumberEdit, binding.addressEdit,
                    binding.LandMartEdit, binding.pincodeEdit, binding.cityEdit, binding.stateEdit
                )


            }

        }))

        binding.cityEdit.addTextChangedListener(TextChangeListernerImple(object :
            TextChangeListernerImple.TextChangeListerner {
            override fun onTextChange(text: String?) {
                addressViewModel.checkInputEmptyFields(
                    binding.nameEdit,
                    binding.buildingNumberEdit, binding.streetNumberEdit, binding.addressEdit,
                    binding.LandMartEdit, binding.pincodeEdit, binding.cityEdit, binding.stateEdit
                )

            }

        }))



        binding.stateEdit.addTextChangedListener(TextChangeListernerImple(object :
            TextChangeListernerImple.TextChangeListerner {
            override fun onTextChange(text: String?) {
                addressViewModel.checkInputEmptyFields(
                    binding.nameEdit,
                    binding.buildingNumberEdit, binding.streetNumberEdit, binding.addressEdit,
                    binding.LandMartEdit, binding.pincodeEdit, binding.cityEdit, binding.stateEdit
                )

            }

        }))







        lifecycleScope.launch {

            addressViewModel.emptyFieldsEvent.observe(viewLifecycleOwner, Observer {

                when(it){

                    false -> {
                        binding.addButton.isEnabled = true

                        if(isUpdate){




                            binding.addButton.setOnClickListener {


                                addressViewModel.updateAddress(
                                    addressID = data.id,
                                    address = Data(
                                        name = binding.nameEdit.text.toString().trim(),
                                        address = binding.addressEdit.text.toString().trim(),
                                        buldingNo = binding.buildingNumberEdit.text.toString().trim(),
                                        city = binding.cityEdit.text.toString().trim(),
                                        state = binding.stateEdit.text.toString().trim(),
                                        pincode = binding.pincodeEdit.text.toString().trim(),
                                        landmark = binding.LandMartEdit.text.toString().trim(),
                                        streetNo = binding.streetNumberEdit.text.toString().trim(),
                                        userId = Constant.getUserID(requireContext())!!,
                                        id = Constant.getUserID(requireContext())!!

                                    )

                                )

                                bottomSheetDialog.dismiss()
                            }

                        }else {
                            binding.addButton.setOnClickListener {


                                addressViewModel.insertAddress(
                                    Data(
                                        name = binding.nameEdit.text.toString().trim(),
                                        address = binding.addressEdit.text.toString().trim(),
                                        buldingNo = binding.buildingNumberEdit.text.toString().trim(),
                                        city = binding.cityEdit.text.toString().trim(),
                                        state = binding.stateEdit.text.toString().trim(),
                                        pincode = binding.pincodeEdit.text.toString().trim(),
                                        landmark = binding.LandMartEdit.text.toString().trim(),
                                        streetNo = binding.streetNumberEdit.text.toString().trim(),
                                        userId =Constant.getUserID(requireContext())!!,
                                        id = Constant.getUserID(requireContext())!!

                                    )
                                    , user_id = Constant.getUserID(requireContext())!!
                                )

                                bottomSheetDialog.dismiss()
                            }
                        }


                    }
                    true -> binding.addButton.isEnabled = false

                }


            })

        }


    }

}