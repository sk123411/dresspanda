package com.maestros.dresspanda.home.ui.wishlist.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.maestros.dresspanda.R
import com.maestros.dresspanda.databinding.WishItemListBinding
import com.maestros.dresspanda.home.ui.wishlist.WishlistViewModel
import com.maestros.dresspanda.home.ui.wishlist.model.WishlistProduct
import com.maestros.dresspanda.util.Constant
import com.squareup.picasso.Picasso
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class WishlistAdapter(val list:MutableList<WishlistProduct>, val wishlistViewModel: WishlistViewModel,val lifeCycleScope: CoroutineScope) :
    RecyclerView.Adapter<WishlistAdapter.ViewHolder>() {



    class ViewHolder(view:View): RecyclerView.ViewHolder(view) {
        val binding = WishItemListBinding.bind(view)




        fun bindData(
            wish: WishlistProduct,
            wishlistViewModel: WishlistViewModel,
            lifeCycleScope: CoroutineScope,
            wishlistAdapter: WishlistAdapter
        ){


            binding.wiTitle.text = wish.product_title
            binding.wiPrice.text = wish.selling_price.toString()
            binding.wiOriginalPrice.text = wish.MRP
            Picasso.get().load(Constant.BASE_URL+wish.image)
                .placeholder(R.mipmap.ic_launcher_round).into(binding.wiImage)


            binding.deleteProductButton.setOnClickListener {
                deleteProduct(
                    wishlistViewModel,
                    wish,
                    lifeCycleScope,
                    wishlistAdapter)

            }


        }

        private fun deleteProduct(wishlistViewModel: WishlistViewModel,
                                  wish: WishlistProduct,
                                  lifeCycleScope: CoroutineScope,
                                  adapter: WishlistAdapter
                                  ) {
                lifeCycleScope.launch {
                    wishlistViewModel.productWishlistDao.deleteProduct(wish)
                    adapter.list.remove(wish)
                    adapter.notifyItemRemoved(adapterPosition)
                    adapter.notifyItemChanged(adapterPosition, adapter.list)

                }


        }


    }



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(R.layout.wish_item_list,
        parent,false)

        return ViewHolder(view)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.bindData(
            list.get(position),
            wishlistViewModel,
            lifeCycleScope,
            this)


    }
}