package com.maestros.dresspanda.home.ui.orders.repository.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.maestros.dresspanda.home.ui.orders.repository.model.CheckoutResponse
import com.maestros.dresspanda.home.ui.product.model.productdetail.ProductResponse

data class OrderResponse(
    @SerializedName("data")
    @Expose
    val data: List<CheckoutResponse>,


    @SerializedName("result")
    @Expose
    val result: String
) {
}