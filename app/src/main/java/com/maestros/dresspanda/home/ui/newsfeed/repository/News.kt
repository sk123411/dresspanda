package com.maestros.dresspanda.home.ui.newsfeed.repository

import com.maestros.dresspanda.home.ui.newsfeed.adapter.model.postlist.BasePost
import com.maestros.dresspanda.home.ui.newsfeed.adapter.model.LikeCommentResponse
import com.maestros.dresspanda.home.ui.newsfeed.adapter.model.postcomment.PostComment
import com.maestros.dresspanda.home.ui.newsfeed.adapter.model.postlist.Post
import com.maestros.dresspanda.util.Resource
import okhttp3.MultipartBody
import okhttp3.RequestBody

interface News {

    suspend fun insertPost(
        action: String?,
        image: MultipartBody.Part?,
        title: RequestBody?,
         userID: RequestBody?
    ): Resource<Post>

    suspend fun getAllPosts(action: String?,data:HashMap<String,String>): Resource<BasePost<Post>>


    suspend fun likePost(action:String?,data:HashMap<String,String>):Resource<LikeCommentResponse>

    suspend fun commentOnPost(action:String?,data:HashMap<String,String>):Resource<LikeCommentResponse>

    suspend fun showPostComments(action:String?,data:HashMap<String,String>):Resource<PostComment>


}