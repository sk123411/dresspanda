package com.maestros.dresspanda.home.ui.orders

import android.os.Build
import android.util.Log
import android.widget.EditText
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.maestros.dresspanda.home.ui.notification.Notification
import com.maestros.dresspanda.home.ui.orders.repository.Orders
import com.maestros.dresspanda.home.ui.orders.repository.model.CheckoutResponse
import com.maestros.dresspanda.util.Resource
import com.maestros.dresspanda.util.TextChangeListernerImple
import kotlinx.coroutines.launch

class OrderViewModel @ViewModelInject constructor(val orders: Orders):ViewModel() {

    private val _ordersEvent = MutableLiveData<OrdersEvent>()
     val ordersEvent:LiveData<OrdersEvent>
        get() = _ordersEvent

    sealed class OrdersEvent {
        class AddOrder(val resultText: CheckoutResponse) : OrdersEvent()
        class SuccessGetAllOrders(val resultText: List<CheckoutResponse>) : OrdersEvent()
        class Failure(val errorText: String) : OrdersEvent()
        class AddNotification(val resultText: Notification) : OrdersEvent()
        class ShowNotification(val resultText: List<Notification>) : OrdersEvent()

        object Loading : OrdersEvent()

    }

    fun addOrder(userID: String?, addressId:String?,totalAmount: Double,paymentType:String?,deliveryFee:Int){

        val map = HashMap<String,String>()

        map.put("user_id",userID!!)
        map.put("address_id",addressId!!)
        map.put("total_amount",totalAmount.toString())
        map.put("payment_type",paymentType!!)
        map.put("delivery_fee",deliveryFee.toString())

        Log.d("CARTCCCCCC" , "::"+map.toString())

        viewModelScope.launch {

            val response = orders.addOrder("checkout", map!!)


            when(response){

                is Resource.Success -> {

                if (response.data!!.result.equals("successfully")) {
                    _ordersEvent.value = OrdersEvent.AddOrder(response.data!!)
                }else{
                    _ordersEvent.value = OrdersEvent.Failure(response.data.result)
                }
                }
                is Resource.Error -> {

                    _ordersEvent.value = OrdersEvent.Failure("cvcvc")

                }
            }

        }


    }


    fun showOrders(userID: String?){
        _ordersEvent.value = OrdersEvent.Loading

        val map = HashMap<String,String>()
        map.put("user_id",userID!!)



        viewModelScope.launch {

            val response = orders.showOrders("show_orders", map!!)

            when(response){

                is Resource.Success -> {

                    if (response.data!!.result.equals("successfully")) {
                        _ordersEvent.value = OrdersEvent.SuccessGetAllOrders(response.data.data)
                    }else{
                        _ordersEvent.value = OrdersEvent.Failure(response.data.result)

                    }
                }

                is Resource.Error -> {
                    Log.d("CARTCCCCCC" , "::"+response.message)

                    _ordersEvent.value = OrdersEvent.Failure("response.data!!.result")

                }
            }

        }


    }

    fun showSearchOrders(userID: String?,searchText: String?){
        _ordersEvent.value = OrdersEvent.Loading

        val map = HashMap<String,String>()
        map.put("user_id",userID!!)

        val searchList = mutableListOf<CheckoutResponse>()


        viewModelScope.launch {

            val response = orders.showOrders("show_orders", map!!)

            when(response){

                is Resource.Success -> {

                    if (response.data!!.result.equals("successfully")) {

                        for (res in response.data.data){
                            if (res.orderId.toLowerCase().contains(searchText!!.toLowerCase())){
                                searchList.add(res)

                            }

                        }

                        _ordersEvent.value = OrdersEvent.SuccessGetAllOrders(searchList)
                    }else{
                        _ordersEvent.value = OrdersEvent.Failure(response.data.result)
                    }
                }

                is Resource.Error -> {
                    Log.d("CARTCCCCCC" , "::"+response.message)

                    _ordersEvent.value = OrdersEvent.Failure("response.data!!.result")

                }
            }

        }


    }


    fun addNotification(userID: String?, message:String?){

        val map = HashMap<String,String>()
        map.put("user_id",userID!!)
        map.put("text",message!!)

        viewModelScope.launch {

            val response = orders.addNotification("add_notification", map!!)


            when(response){

                is Resource.Success -> {

                    if (response.data!!.result.contains("successfully")) {
                        _ordersEvent.value = OrdersEvent.AddNotification(response.data)
                    }else{
                        _ordersEvent.value = OrdersEvent.Failure(response.data.result)
                    }
                }
                is Resource.Error -> {

                    _ordersEvent.value = OrdersEvent.Failure("cvcvc")

                }
            }

        }


    }
    fun showNotification(userID: String?){

        val map = HashMap<String,String>()
        map.put("user_id",userID!!)
        viewModelScope.launch {

            val response = orders.getNotification("show_notification", map!!)


            when(response){

                is Resource.Success -> {

                    if (response.data!!.status!!) {
                        _ordersEvent.value = OrdersEvent.ShowNotification(response.data.list)
                    }else{
                        _ordersEvent.value = OrdersEvent.Failure("Server error occured")

                    }
                }
                is Resource.Error -> {

                    _ordersEvent.value = OrdersEvent.Failure("cvcvc")

                }
            }

        }


    }

}