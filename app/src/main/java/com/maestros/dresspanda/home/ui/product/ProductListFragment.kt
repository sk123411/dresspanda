package com.maestros.dresspanda.home.ui.product

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.maestros.dresspanda.R
import com.maestros.dresspanda.databinding.FragmentProductListBinding
import com.maestros.dresspanda.home.ui.home.HomeViewModel
import com.maestros.dresspanda.home.ui.product.adapter.ProductAdapter
import com.maestros.dresspanda.util.Constant
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProductListFragment : Fragment() {

    lateinit var binding:FragmentProductListBinding
    private val homeViewModel: HomeViewModel by viewModels()
    private val productViewModel: ProductViewModel by viewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentProductListBinding.inflate(layoutInflater)

        val isFromHome:Boolean = productViewModel.getPaperDatabase<Boolean>(Constant.IS_FROM_HOME!!)

        if (isFromHome)
            homeViewModel.getHomeProducts()
        else
        {
            var id:String? = "4"
            if ( arguments?.getString("id")!=null){
                id = arguments?.getString("id")
            }

            productViewModel.getProductsByCategoryId(id)

        }



        lifecycleScope.launchWhenStarted {
            homeViewModel.homeEvent.observe(viewLifecycleOwner, Observer {

                when (it) {

                    is HomeViewModel.HomeEvent.SuccessProduct -> {
                        activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.GONE

                        binding.productsList.apply {
                            layoutManager = GridLayoutManager(context, 2)
                            adapter = ProductAdapter(it.resultText)
                        }

                    }

                    is HomeViewModel.HomeEvent.Failure -> {
                        Toast.makeText(context, "Error occured", Toast.LENGTH_SHORT).show()
                    }
                    is HomeViewModel.HomeEvent.Loading -> {
                        activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.VISIBLE

                    }

                }
            })


            productViewModel.productEvent.observe(viewLifecycleOwner, Observer {

                when (it) {
                    is ProductViewModel.ProductEvent.ProductsByCategory -> {
                        activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.GONE



                        binding.productsList.apply {
                            layoutManager = GridLayoutManager(context, 2)
                            adapter = ProductAdapter(it.productResponse)
                        }

                    }

                    is ProductViewModel.ProductEvent.Loading -> {
                        activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.VISIBLE

                    }


                }
            })

        }







        return binding.root

    }



}