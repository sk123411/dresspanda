package com.maestros.dresspanda.home.ui.cart.repository


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Product(
    @SerializedName("brand")
    @Expose
    val brand: String,
    @SerializedName("category_id")
    @Expose
    val categoryId: String,
    @SerializedName("color")
    @Expose
    val color: String,
    @SerializedName("discount")
    @Expose
    val discount: String,
    @SerializedName("id")
    @Expose
    val id: String,
    @SerializedName("image")
    @Expose
    val image: String,
    @SerializedName("MRP")
    @Expose
    val mRP: String,
    @SerializedName("path")
    @Expose
    val path: String,
    @SerializedName("product_description")
    @Expose
    val productDescription: String,
    @SerializedName("product_title")
    @Expose
    val productTitle: String,
    @SerializedName("seller_id")
    @Expose
    val sellerId: String,
    @SerializedName("selling_price")
    @Expose
    val sellingPrice: String,
    @SerializedName("size")
    @Expose
    val size: String,
    @SerializedName("status")
    @Expose
    val status: String,
    @SerializedName("stock")
    @Expose
    val stock: String,
    @SerializedName("sub_category_id")
    @Expose
    val subCategoryId: String,
    @SerializedName("sub_total")
    @Expose
    val subTotal: String,
    @SerializedName("qntity")
    @Expose
    val qunatity: String
)