package com.maestros.dresspanda.home.ui.product

import android.content.Context
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.maestros.dresspanda.home.ui.product.model.productdetail.ProductResponse
import com.maestros.dresspanda.home.ui.product.repository.Product
import com.maestros.dresspanda.data.wishlist.ProductWishlistDao
import com.maestros.dresspanda.home.ui.category.CateogoryViewModel
import com.maestros.dresspanda.home.ui.product.model.productdetail.ProductDetailResponse
import com.maestros.dresspanda.home.ui.wishlist.model.WishlistProduct
import com.maestros.dresspanda.util.Resource
import io.paperdb.Paper
import kotlinx.coroutines.launch
import org.json.JSONObject

class ProductViewModel  @ViewModelInject constructor(
    val context: Context,
    val product:Product,
    val productWishlistDao: ProductWishlistDao
):ViewModel(){

    val productEvent_ = MutableLiveData<ProductEvent>()

    val productEvent:LiveData<ProductEvent>
        get() = productEvent_


    sealed class ProductEvent{
        class ProductAddedSuccess(productResponse: WishlistProduct):ProductEvent()
        class Failure(message:String):ProductEvent()
        class ProductOnDb(val boolean: Boolean, productResponse: WishlistProduct):ProductEvent()
        class ProductsByCategory(val productResponse: List<ProductResponse>):ProductEvent()
        object Loading:ProductEvent()
        class ProductDetailsByID(val productResponse: ProductDetailResponse):ProductEvent()
        class AddProductToCartSuccess(productResponse: ProductResponse):ProductEvent()
        class AddProductViewCountSuccess(productResponse: JSONObject):ProductEvent()


    }




    fun insertProduct(productResponse: WishlistProduct){
        viewModelScope.launch {
            val itemResult = productWishlistDao.insertOrUpdate(productResponse)


            if (itemResult<-1)
                productEvent_.value = ProductEvent.Failure("Some error occured , please try again")
            else
            productEvent_.value=ProductEvent.ProductAddedSuccess(productResponse)

        }
    }


    fun isProductOnDB(productResponse: WishlistProduct){
        viewModelScope.launch {
            val isProductOnDatabase = productWishlistDao.isProductOnDB(productResponse)
            productEvent_.value = ProductEvent.ProductOnDb(isProductOnDatabase, productResponse)

        }
    }


    fun <T> getPaperDatabase(string: String):T{
        var data:T
        Paper.init(context)
        data = Paper.book().read(string)
        return data

    }



    fun getProductsByCategoryId(categoryID:String?){

        productEvent_.value = ProductEvent.Loading

        val map = HashMap<String,String>()
        map.put("subcategory_id", categoryID!!)

        viewModelScope.launch {
            val response = product.getProductsByCategory("show_products_bysubcat", map)



            when(response){

                is Resource.Success->{


                    if(response.data?.status!!){
                        productEvent_.value = ProductEvent.ProductsByCategory(response.data.list)
                    }else {
                        productEvent_.value = ProductEvent.Failure(response.message!!)
                    }

                }
                is Resource.Error-> productEvent_.value = ProductEvent.Failure(response.message!!)
            }


        }

    }
    fun getProductDetailsByID(productID:String?){

        productEvent_.value = ProductEvent.Loading
        val map = HashMap<String,String>()
        map.put("product_id", productID!!)

        viewModelScope.launch {
            val response = product.getProductDetailsByID("get_shop_by_productid", map)



            when(response){

                is Resource.Success->{
                    productEvent_.value = ProductEvent.ProductDetailsByID(response.data!!)
                }
                is Resource.Error-> productEvent_.value = ProductEvent.Failure(response.message!!)
            }


        }

    }

    fun addToCart(userId:String?,productID: String?,quantity:String?){
        productEvent_.value = ProductEvent.Loading

        val map = HashMap<String,String>()
        map.put("user_id", userId!!)
        map.put("product_id", productID!!)
        map.put("product_quantity", quantity!!)


        viewModelScope.launch {


            val response = product.addProductToCart("add_to_cart",map)

            when(response){

                is Resource.Success -> {
                    productEvent_.value = ProductEvent.AddProductToCartSuccess(response.data!!)

                }
                is Resource.Error -> {
                    productEvent_.value = ProductEvent.Failure(response.message!!)
                }
            }

        }


    }


    fun addProductViewCount(userId: String?, productID: String?){
        productEvent_.value = ProductEvent.Loading

        val map = HashMap<String,String>()
        map.put("user_id", userId!!)
        map.put("product_id", productID!!)


        viewModelScope.launch {
            val response = product.addCountProductSeen("count_view_product", map)

            when(response){

                is Resource.Success -> {
                    productEvent_.value = ProductEvent.AddProductViewCountSuccess(response.data!!)

                }
                is Resource.Error -> {
                    productEvent_.value = ProductEvent.Failure(response.message!!)
                }
            }
        }


    }


}