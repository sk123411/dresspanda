package com.maestros.dresspanda.home.ui.product.repository

import com.maestros.dresspanda.home.ui.brand.BaseResponse
import com.maestros.dresspanda.home.ui.product.model.productdetail.ProductDetailResponse
import com.maestros.dresspanda.home.ui.product.model.productdetail.ProductResponse
import com.maestros.dresspanda.util.Resource
import org.json.JSONObject
import retrofit2.Response
import retrofit2.http.FieldMap
import retrofit2.http.Query

interface Product {

    suspend fun getProductsByCategory(action:String?, category_id:HashMap<String,String>):Resource<BaseResponse<ProductResponse>>
    suspend fun getProductDetailsByID(action:String?, product_id:HashMap<String,String>):Resource<ProductDetailResponse>
    suspend fun addProductToCart(action:String?, data:HashMap<String,String>):Resource<ProductResponse>
    suspend fun addCountProductSeen(count_view_product:String?, map:HashMap<String,String>): Resource<JSONObject>

}