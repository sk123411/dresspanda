package com.maestros.dresspanda.home.ui.home.repository

import com.maestros.dresspanda.home.ui.brand.BaseResponse
import com.maestros.dresspanda.home.ui.brand.HomeResponse
import com.maestros.dresspanda.home.ui.home.model.Slider
import com.maestros.dresspanda.home.ui.notification.Notification
import com.maestros.dresspanda.home.ui.product.model.productdetail.ProductResponse
import retrofit2.Response
import retrofit2.http.*

interface HomeAPI {

    @GET("dresspanda/api/process.php")
    suspend fun getBrands(@Query("action") banner:String?): Response<BaseResponse<HomeResponse>>

    @GET("dresspanda/api/process.php")
    suspend fun getProducts(@Query("action") banner:String?): Response<BaseResponse<ProductResponse>>

    @GET("dresspanda/api/process.php")
    suspend fun getSliderDataHome(@Query("action") banner:String?): Response<BaseResponse<Slider>>





}