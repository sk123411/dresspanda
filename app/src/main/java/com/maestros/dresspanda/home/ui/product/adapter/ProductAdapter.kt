package com.maestros.dresspanda.home.ui.product.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.maestros.dresspanda.R
import com.maestros.dresspanda.databinding.LytProductItemBinding
import com.maestros.dresspanda.home.ui.product.model.productdetail.ProductResponse
import com.maestros.dresspanda.util.Constant
import com.squareup.picasso.Picasso
import io.paperdb.Paper

class ProductAdapter(val list:List<ProductResponse>) :RecyclerView.Adapter<ProductAdapter.MyViewHolder>(){
    class MyViewHolder(view:View):RecyclerView.ViewHolder(view) {

        val binding = LytProductItemBinding.bind(view)




         fun bindProduct(productResponse: ProductResponse) {

             val langCode = Paper.book().read<String>(Constant.LANGUAGE)

             if(langCode.equals("bn")){

                 if(productResponse.product_title_bangla.isEmpty()){

                     binding.ppTitle.text = productResponse.product_title

                 }else {

                     binding.ppTitle.text = productResponse.product_title_bangla


                 }

             }else {


                 binding.ppPrice.text = binding.root.resources.getString(R.string.currency_symbol) + " "+productResponse.selling_price
                 Picasso.get().load(Constant.BASE_URL + productResponse.image)
                     .placeholder(R.mipmap.ic_launcher_round).into(binding.ppImage)

             }



                 binding.root.setOnClickListener {
                     Paper.init(it.context)
                     Paper.book().write(Constant.PRODUCT_DETAIL, productResponse)
                     Navigation.findNavController(it).navigate(
                         R.id.productDetailFragment
                     )

                 }


        }

    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ProductAdapter.MyViewHolder {

        val binding = LayoutInflater.from(parent.context).inflate(R.layout.lyt_product_item,
                parent,false)
        Paper.init(binding.context)

        return MyViewHolder(view = binding.rootView)


    }

    override fun getItemCount(): Int {

        return list.size
    }

    override fun onBindViewHolder(holder: ProductAdapter.MyViewHolder, position: Int) {

        holder.bindProduct(list.get(position))

    }

}