package com.maestros.dresspanda.home.ui.gift

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.maestros.dresspanda.R
import com.maestros.dresspanda.home.ui.wishlist.adapter.model.Wish

class GiftViewModel : ViewModel() {

    val giftlist_ = MutableLiveData<List<Gift>>()

    val giftlist:LiveData<List<Gift>>
        get() = giftlist_



    fun generateTempList(){

        val temp = arrayListOf<Gift>()
        for (l in 1..20){
            temp.add(
                Gift(R.drawable.gift_image, "Coupon won", "You will get 100% Off"
              ))

        }
        giftlist_.value =temp;
    }










}