package com.maestros.dresspanda.home.ui.category.adapter.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class SubCategoryResponse(
    @SerializedName("id")
    @Expose
    val id: String,
    @SerializedName("image")
    @Expose
    val image: String,

    @SerializedName("category_id")
    @Expose
    val categoryID: String,
    @SerializedName("path")
    @Expose
    val path: String,
    @SerializedName("name")
    @Expose
    val title: String,
    @SerializedName("bangla_name")
    @Expose
    val bangla_name: String

)