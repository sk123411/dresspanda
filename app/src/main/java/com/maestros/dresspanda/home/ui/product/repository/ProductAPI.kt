package com.maestros.dresspanda.home.ui.product.repository

import com.maestros.dresspanda.home.ui.brand.BaseResponse
import com.maestros.dresspanda.home.ui.product.model.productdetail.ProductDetailResponse
import com.maestros.dresspanda.home.ui.product.model.productdetail.ProductResponse
import org.json.JSONObject
import retrofit2.Response
import retrofit2.http.*

interface ProductAPI {

    @POST("dresspanda/api/process.php")
    @FormUrlEncoded
    suspend fun getProductsByCategoryID(@Query("action") show_product:String?, @FieldMap map:HashMap<String,String>): Response<BaseResponse<ProductResponse>>


    @POST("dresspanda/api/process.php")
    @FormUrlEncoded
    suspend fun getProductDetailsByID(@Query("action") show_product:String?,
                                      @FieldMap map:HashMap<String,String>): Response<ProductDetailResponse>


    @POST("dresspanda/api/process.php")
    @FormUrlEncoded
    suspend fun addProductToCart(@Query("action") add_cart:String?,
                                 @FieldMap map:HashMap<String,String>): Response<ProductResponse>



    @POST("dresspanda/api/process.php")
    @FormUrlEncoded
    suspend fun addCountProductSeen(@Query("action") count_view_product:String?, @FieldMap map:HashMap<String,String>): Response<JSONObject>


}