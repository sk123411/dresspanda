package com.maestros.dresspanda.home.ui.message.fragment

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.dhaval2404.imagepicker.ImagePicker
import com.maestros.dresspanda.R
import com.maestros.dresspanda.databinding.FragmentSendChatMessageBinding
import com.maestros.dresspanda.home.ui.message.ChatViewModel
import com.maestros.dresspanda.home.ui.message.adapter.ChatAdapter
import com.maestros.dresspanda.login.LoginViewModel
import com.maestros.dresspanda.util.Constant
import dagger.hilt.android.AndroidEntryPoint
import io.paperdb.Paper
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.io.File

@AndroidEntryPoint
class SendChatMessageFragment : Fragment() {

    var senderID:String?=""
    var userID:String?=""
    lateinit var binding:FragmentSendChatMessageBinding
    val loginViewModel:LoginViewModel by viewModels()
    val chatViewModel: ChatViewModel by viewModels()



    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSendChatMessageBinding.inflate(inflater)
        loginViewModel.getUserDetails()


        Paper.init(activity)
        senderID = Paper.book().read("SENDER_ID")
        chatViewModel.getAllChats(senderID,Constant.getUserID(context))

        lifecycleScope.launch {

            loginViewModel.login.collect {user->
                when(user) {
                    is LoginViewModel.LoginEvent.UserDetails -> {

                        chatViewModel.getAllChats(senderId = user.userDetails.id,
                        receiverId = senderID)

                        userID = user.userDetails.id

                        chatViewModel.imageFile.observe(viewLifecycleOwner , Observer { file ->


                            binding.sendMessageButtton.setOnClickListener {view


                                chatViewModel.sendMessageImage(senderId = user.userDetails.id,
                                receiverId = senderID,message = binding.messageEdit.text.toString(),
                                imageFile = file!!, userId = user.userDetails.id)
                            }

                        })

                        binding.sendMessageButtton.setOnClickListener {


                            chatViewModel.sendMessage(senderId = user.userDetails.id,
                            receiverId = senderID,userId = user.userDetails.id, message = binding.messageEdit.text.toString())


                        }


                    }



                }






            }



        }

        lifecycleScope.launch {

            chatViewModel.chatEvent.observe(viewLifecycleOwner, Observer {

                when(it){

                    is ChatViewModel.ChatEvent.GetAllChats -> {
                        binding.noMessageText.visibility = View.GONE

                        activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.GONE

                        binding.chatList.apply {
                            adapter = ChatAdapter(it.messages)
                            layoutManager = LinearLayoutManager(context)
                        }
                    }

                    is ChatViewModel.ChatEvent.SuccessMessageSent -> {
                        binding.noMessageText.visibility = View.GONE

                        activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.GONE

                        Toast.makeText(context, "Message sent", Toast.LENGTH_SHORT).show()

                        chatViewModel.getAllChats(senderId = senderID, receiverId = userID)
                    }

                    is ChatViewModel.ChatEvent.Loading -> {
                        binding.noMessageText.visibility = View.GONE


                        activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.VISIBLE

                    }

                    is ChatViewModel.ChatEvent.empty -> {

                        binding.noMessageText.visibility = View.VISIBLE
                    }


                }

            })


        }


        binding.attachFileButton.setOnClickListener {

            ImagePicker.with(this)
                .galleryOnly()
                .compress(1024)
                .start()

        }


        return binding.root

}


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            val fileUri = data?.data
            binding.attachedImage.visibility = View.VISIBLE
            binding.attachedImage.setImageURI(fileUri)
            //You can get File object from intent
            val file: File = ImagePicker.getFile(data)!!

            //You can also get File Path from intent
            chatViewModel.sendImageFile(file)


            val filePath: String = ImagePicker.getFilePath(data)!!
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(context, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(context, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }
}