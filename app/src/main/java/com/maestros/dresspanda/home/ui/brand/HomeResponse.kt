package com.maestros.dresspanda.home.ui.brand

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(tableName = "home")
data class HomeResponse(
    @PrimaryKey(autoGenerate = true) val pid: Int,
    @SerializedName("id")
    @Expose
    val id: String,
    @SerializedName("image")
    @Expose
    val image: String,
    @SerializedName("path")
    @Expose
    val path: String,
    @SerializedName("name")
    @Expose
    val title: String,
    @SerializedName("name_bangla")
    @Expose
    val name_bangla: String


)