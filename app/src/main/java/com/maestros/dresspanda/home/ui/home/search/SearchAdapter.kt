package com.maestros.dresspanda.home.ui.home.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.maestros.dresspanda.R
import com.maestros.dresspanda.databinding.BrandItemListBinding
import com.maestros.dresspanda.databinding.LytProductItemBinding
import com.maestros.dresspanda.home.ui.brand.HomeResponse
import com.maestros.dresspanda.home.ui.product.model.productdetail.ProductResponse
import com.maestros.dresspanda.seller.repository.model.SellerResponse
import com.maestros.dresspanda.util.Constant
import com.squareup.picasso.Picasso
import io.paperdb.Paper

class  SearchAdapter<T:Any?> constructor(val filter: FILTER, val list:MutableList<T>):RecyclerView.Adapter<SearchAdapter.ViewHolder>(){
    class ViewHolder (val view:View):RecyclerView.ViewHolder(view){



        fun setLayoutData(filter: FILTER, data:Any?){


            when(filter)
            {

                FILTER.PRODUCTS -> {



                    val productListBinding = LytProductItemBinding.bind(view)
                    val category = data as ProductResponse
                    productListBinding.ppTitle.text = category.product_title
                    Picasso.get().load(Constant.BASE_URL + category.image)
                        .placeholder(R.mipmap.ic_launcher_round).into(productListBinding.ppImage)
                    productListBinding.root.setOnClickListener {

                        Paper.init(it.context)
                        Paper.book().write(Constant.PRODUCT_DETAIL, category)
                        Navigation.findNavController(productListBinding.root)
                            .navigate(R.id.productDetailFragment)

                    }



                }

                FILTER.SHOP -> {
                    val brandItemListBinding = BrandItemListBinding.bind(view)
                    val category = data as SellerResponse


                    Picasso.get().load(Constant.BASE_URL + category.store_image)
                        .placeholder(R.mipmap.ic_launcher_round)
                        .into(brandItemListBinding.bbImage)
                    brandItemListBinding.bbTitle.visibility = View.VISIBLE
                    brandItemListBinding.bbTitle.text = category.store_name

                    brandItemListBinding.root.setOnClickListener {
                        Paper.init(it.context)
                        Paper.book().write(Constant.SHOP_DETAILS, category)
                        Paper.book().write(Constant.SHOP_WISHLIST_DETAILS, category)

                        val data = Bundle()
                        data.putBoolean(Constant.FROM_FOLLOWED_SHOPS, false)
                        Navigation.findNavController(brandItemListBinding.root)
                            .navigate(R.id.shopDetailsFragment, data)
                    }

                }


                FILTER.BRANDS -> {


                    val brandItemListBinding = BrandItemListBinding.bind(view)
                    val category = data as HomeResponse

                    Picasso.get().load(Constant.BASE_URL + category.image)
                        .placeholder(R.mipmap.ic_launcher_round)
                        .into(brandItemListBinding.bbImage)
                    brandItemListBinding.bbTitle.visibility = View.VISIBLE
                    brandItemListBinding.bbTitle.text = category.title

                    brandItemListBinding.root.setOnClickListener {

                        Paper.init(it.context)
                        Paper.book().write(Constant.FROM_BRANDS, 1)
                        val bundle = Bundle()
                        bundle.putString("brand_id", category.id)
                        bundle.putString("title", category.title)
                        Navigation.findNavController(brandItemListBinding.root)
                            .navigate(R.id.shoplistFragment,bundle)


                    }


                }


            }


        }




    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

            when(filter){


                FILTER.PRODUCTS -> return ViewHolder(LytProductItemBinding.inflate(LayoutInflater.from(parent.context)).root)

                FILTER.SHOP -> return ViewHolder(BrandItemListBinding.inflate(LayoutInflater.from(parent.context)).root)

                FILTER.BRANDS -> return ViewHolder(BrandItemListBinding.inflate(LayoutInflater.from(parent.context)).root)

            else -> return ViewHolder(BrandItemListBinding.inflate(LayoutInflater.from(parent.context)).root)
            }


         }

    override fun getItemCount(): Int {
       return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       holder.setLayoutData(filter,list.get(position))
    }

}












enum class FILTER{

    PRODUCTS,
    SHOP,
    BRANDS

}