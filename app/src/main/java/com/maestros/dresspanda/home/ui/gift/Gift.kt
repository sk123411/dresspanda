package com.maestros.dresspanda.home.ui.gift

data class Gift(

    val image:Int?,
    val title:String?,
    val subtitle:String?


)
