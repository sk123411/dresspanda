package com.maestros.dresspanda.home.ui.message.repository

import android.util.Log
import com.maestros.dresspanda.home.ui.brand.BaseResponse
import com.maestros.dresspanda.home.ui.message.Message
import com.maestros.dresspanda.home.ui.message.MessagesList
import com.maestros.dresspanda.home.ui.message.model.BaseMessage
import com.maestros.dresspanda.util.Resource
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import javax.inject.Inject

class ChatImplementation @Inject constructor(val chatapi:ChatAPI):Chat{





    override suspend fun sendMessage(
        action: String?,
        data: HashMap<String, String>
    ): Resource<Message> {
        return try {
            val response = chatapi.sendMessage(action, data)
            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())
                Resource.Success(result)

            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }

    }

    override suspend fun showMessage(
        action: String?,
        data: HashMap<String, String>
    ):Resource<BaseMessage<MessagesList>> {

        return try {
            val response = chatapi.showMessage(action, data)
            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())
                Resource.Success(result)

            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }


    }

    override suspend fun showAllMessages(
        action: String?,
        data: HashMap<String, String>
    ):  Resource<BaseMessage<MessagesList>> {
        return try {
            val response = chatapi.showAllMessages(action, data)
            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())
                Resource.Success(result)

            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }

    override suspend fun sendMessageWithImage(
        action: String?,
        image: MultipartBody.Part?,
        userID:RequestBody?,
        message: RequestBody?,
        senderID: RequestBody?,
        receiverID: RequestBody?
    ): Resource<Message> {
        return try {
            val response = chatapi.sendMessageWithImage(action, image,userID,message,senderID,receiverID)
            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())
                Resource.Success(result)

            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }


}