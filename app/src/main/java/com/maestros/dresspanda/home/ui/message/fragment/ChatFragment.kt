package com.maestros.dresspanda.home.ui.message.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.maestros.dresspanda.R
import com.maestros.dresspanda.databinding.FragmentChatBinding
import com.maestros.dresspanda.home.ui.home.HomeAdapter
import com.maestros.dresspanda.home.ui.message.ChatViewModel
import com.maestros.dresspanda.home.ui.message.MessagesList
import com.maestros.dresspanda.login.LoginViewModel
import com.maestros.dresspanda.util.Constant
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@AndroidEntryPoint
class ChatFragment : Fragment() {


    val chatViewModel: ChatViewModel by viewModels()


    lateinit var binding:FragmentChatBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentChatBinding.inflate(inflater)
        chatViewModel.getAllMessages(Constant.getUserID(context))









        lifecycleScope.launch {


            chatViewModel.chatEvent.observe(viewLifecycleOwner, Observer {

                when(it){


                    is ChatViewModel.ChatEvent.GetAllChats -> {
                        binding.noMessageText.visibility = View.GONE

                        activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.GONE


                        binding.messageList.apply {

                            layoutManager = LinearLayoutManager(context)
                            adapter =
                                HomeAdapter<MessagesList>(
                                    it.messages,
                                    R.layout.message_item
                                )

                        }

                    }

                    is ChatViewModel.ChatEvent.Loading -> {
                        binding.noMessageText.visibility = View.GONE

                        activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.VISIBLE

                    }
                    is ChatViewModel.ChatEvent.Failure -> {
                        binding.noMessageText.visibility = View.GONE

                        activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.GONE

                    }
                    is ChatViewModel.ChatEvent.empty -> {

                        binding.noMessageText.visibility = View.VISIBLE
                    }

                }


            })
        }


        // Inflate the layout for this fragment




        return binding.root
    }



}