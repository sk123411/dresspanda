package com.maestros.dresspanda.home.ui.newsfeed.adapter

import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.recyclerview.widget.RecyclerView
import com.maestros.dresspanda.databinding.CommentItemLytBinding
import com.maestros.dresspanda.home.ui.newsfeed.adapter.model.postcomment.Data
import com.maestros.dresspanda.util.Constant

class PostCommentAdapter(val list: List<Data>) :
    RecyclerView.Adapter<PostCommentAdapter.MyViewholder>() {
    class MyViewholder(view: View) : RecyclerView.ViewHolder(view) {

        val commentItemLytBinding = CommentItemLytBinding.bind(view)
        fun bindData(data: Data) {

            if (data.userId.equals(Constant.getUserID(context = commentItemLytBinding.root.context))){
                val lp =
                    LinearLayoutCompat.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT)
                lp.weight = 1.0f;
                lp.gravity = Gravity.TOP;

                commentItemLytBinding.commentRootLayout.layoutParams = lp

            }else {


            }
            commentItemLytBinding.userName.text = data.id
            commentItemLytBinding.userComment.text = data.comment



        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewholder {

        val binding = CommentItemLytBinding.inflate(LayoutInflater.from(parent.context))


        return MyViewholder(binding.root)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewholder, position: Int) {

        holder.bindData(list.get(position))
    }
}