package com.maestros.dresspanda.home.ui.newsfeed

import android.content.Context
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.maestros.dresspanda.home.ui.newsfeed.adapter.model.LikeCommentResponse
import com.maestros.dresspanda.home.ui.newsfeed.adapter.model.postcomment.Data
import com.maestros.dresspanda.home.ui.newsfeed.adapter.model.postlist.Post
import com.maestros.dresspanda.home.ui.newsfeed.repository.News
import com.maestros.dresspanda.util.Constant
import kotlinx.coroutines.launch
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

class NewsViewModel @ViewModelInject constructor(private val news: News, private val context: Context) : ViewModel() {

    val _newsEvent = MutableLiveData<NewsEvent>()

    val newsEvent:LiveData<NewsEvent>
        get() = _newsEvent



    private val imageFile_ = MutableLiveData<File>()



    val imageFile: LiveData<File>
        get() = imageFile_



    sealed class NewsEvent {
        class SuccessAddPost(val resultText: Post) :NewsEvent()
        class SuccessGetAllPost(val resultText:List<Post>) :NewsEvent()
        class SuccessGetAllPostUserID(val resultText:List<Post>) :NewsEvent()

        class SuccessPostLike(val resultText:LikeCommentResponse, val isComment:Boolean) :NewsEvent()
        class SuccessGetPostComments(val resultText:List<Data>) :NewsEvent()

        class Failure(val errorText: String) : NewsEvent()
        object Loading : NewsEvent()
        object Empty : NewsEvent()
    }



    fun getAllPost(userId:String?){
        _newsEvent.value = NewsEvent.Loading
        val map = HashMap<String,String>()
        map.put("user_id", userId!!)
        viewModelScope.launch {

            val response = news.getAllPosts("show_post", map )

            when(response){

                is com.maestros.dresspanda.util.Resource.Success -> {

                    if(response.data?.result!!) {
                        _newsEvent.value = NewsEvent.SuccessGetAllPost(response.data?.postList!!)
                    }else {
                        _newsEvent.value = NewsEvent.Failure("Error occured")
                    }

                }

                is com.maestros.dresspanda.util.Resource.Error -> {
                    _newsEvent.value = NewsEvent.Failure(response.message!!)

                }
            }

        }

    }




    fun addLikeOnPost(userId:String?, postId:String?){
        _newsEvent.value = NewsEvent.Loading
        val map = HashMap<String,String>()
        map.put("user_id", userId!!)
        map.put("post_id", postId!!)

        viewModelScope.launch {

            val response = news.likePost("like_post", map)

            when(response){

                is com.maestros.dresspanda.util.Resource.Success -> {
                        _newsEvent.value = NewsEvent.SuccessPostLike(response.data!!,false)
                    getAllPost(userId)

                }

                is com.maestros.dresspanda.util.Resource.Error -> {
                    _newsEvent.value = NewsEvent.Failure(response.message!!)

                }
            }

        }

    }


    fun addCommentOnPost(userId:String?, postId:String?, comment:String?){
        _newsEvent.value = NewsEvent.Loading
        val map = HashMap<String,String>()
        map.put("user_id", userId!!)
        map.put("post_id", postId!!)
        map.put("comment", comment!!)

        viewModelScope.launch {

            val response = news.commentOnPost("add_comment_onpost", map)

            when(response){

                is com.maestros.dresspanda.util.Resource.Success -> {
                    _newsEvent.value = NewsEvent.SuccessPostLike(response.data!!,true)
                    showCommentOnPost(postId)

                }

                is com.maestros.dresspanda.util.Resource.Error -> {
                    _newsEvent.value = NewsEvent.Failure(response.message!!)

                }
            }

        }

    }


    fun showCommentOnPost(postId:String?){
        _newsEvent.value = NewsEvent.Loading
        val map = HashMap<String,String>()
        map.put("post_id", postId!!)

        viewModelScope.launch {

            val response = news.showPostComments("show_comment_onpost", map)

            when(response){

                is com.maestros.dresspanda.util.Resource.Success -> {

                    if (response.data!!.status.equals("successfully")){
                        _newsEvent.value = NewsEvent.SuccessGetPostComments(response.data!!.data)

                    }else {

                        _newsEvent.value = NewsEvent.Failure("Server error occured")

                    }


                }

                is com.maestros.dresspanda.util.Resource.Error -> {
                    _newsEvent.value = NewsEvent.Failure("Server error occured")
                }


            }

        }

    }






    fun insertPost(image:File?,userId:String?,title:String?){
        _newsEvent.value = NewsEvent.Loading
        val map = HashMap<String,String>()


        val imageBody: RequestBody =  RequestBody.create(
            "image/jpg".toMediaTypeOrNull(),
            image!!)

        val storeImageBody: MultipartBody.Part =
            MultipartBody.Part.createFormData(
                Constant.STORE_IMAGE,
                image.name, imageBody)


        val titleBody = getMultiPartFormRequestBody(title)
        val userBody = getMultiPartFormRequestBody(userId)



        viewModelScope.launch {

            val response = news.insertPost(action = "add_post", image = storeImageBody,
            userID = userBody, title = titleBody)

            when(response){

                is com.maestros.dresspanda.util.Resource.Success -> {

                    _newsEvent.value = NewsEvent.SuccessAddPost(response.data!!)
                }

                is com.maestros.dresspanda.util.Resource.Error -> {
                    _newsEvent.value = NewsEvent.Failure(response.message!!)

                }
            }

        }

    }

    fun getMultiPartFormRequestBody(tag:String?): RequestBody {
        return RequestBody.create(MultipartBody.FORM, tag!!)

    }




    fun sendImageFile(file: File) {

        imageFile_.value = file

    }


}