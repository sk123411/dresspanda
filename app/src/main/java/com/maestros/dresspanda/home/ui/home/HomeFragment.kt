package com.maestros.dresspanda.home.ui.home

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.maestros.dresspanda.R
import com.maestros.dresspanda.databinding.FragmentHomeBinding
import com.maestros.dresspanda.databinding.SelectFilterLayoutBinding
import com.maestros.dresspanda.home.HomeActivity
import com.maestros.dresspanda.home.ui.brand.HomeResponse
import com.maestros.dresspanda.home.ui.cart.CartFragmentViewModel
import com.maestros.dresspanda.home.ui.home.model.Slider
import com.maestros.dresspanda.home.ui.home.search.FILTER
import com.maestros.dresspanda.home.ui.home.search.SearchAdapter
import com.maestros.dresspanda.home.ui.product.model.productdetail.ProductResponse
import com.maestros.dresspanda.login.LoginActivity
import com.maestros.dresspanda.seller.repository.model.SellerResponse
import com.maestros.dresspanda.util.Constant
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType
import com.smarteist.autoimageslider.SliderAnimations
import dagger.hilt.android.AndroidEntryPoint
import io.paperdb.Paper

@AndroidEntryPoint
class HomeFragment : Fragment() {

    private val homeViewModel: HomeViewModel by viewModels()
    private val cartViewModel: CartFragmentViewModel by viewModels()

    lateinit var binding: FragmentHomeBinding
    lateinit var customToolbarRoot: RelativeLayout
    lateinit var sliderAdapter: SliderAdapter

    lateinit var tempProducts: List<ProductResponse>
    lateinit var shops: List<SellerResponse>
    lateinit var homeProducts: List<HomeResponse>


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeBinding.inflate(inflater)

        homeViewModel.getHomeBrands()
        homeViewModel.getHomeProducts()
        homeViewModel.getSliderData()
        homeViewModel.getShops()

        tempProducts = ArrayList<ProductResponse>()
        shops = ArrayList<SellerResponse>()
        homeProducts = ArrayList<HomeResponse>()








        if (Constant.isUserLogged(requireActivity())!!) {
            cartViewModel.getCartItems(Constant.getUserID(context))
        }



        Paper.init(context)


        val savedProductList = Paper.book().read<List<ProductResponse>>(FILTER.PRODUCTS.name)
        val savedShopsList = Paper.book().read<List<SellerResponse>>(FILTER.SHOP.name)
        val savedBrandsList = Paper.book().read<List<HomeResponse>>(FILTER.BRANDS.name)

        binding.fhSearchEdit.addTextChangedListener(object : TextWatcher {


            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {


                if (!s.toString().equals("") || !s.isNullOrEmpty()) {

                    val filter = Paper.book().read<FILTER>(Constant.FILTER,FILTER.PRODUCTS)


                    when (filter) {

                        FILTER.PRODUCTS -> {


                            val filterList = mutableListOf<ProductResponse>()
                            binding.searchList.isVisible = true


                            if (savedProductList != null) {

                                for (response in savedProductList) {


                                    if (response.product_title.toLowerCase()
                                            .contains(s.toString().toLowerCase())
                                    ) {


                                        filterList.add(response)

                                    }
                                }
                            }else {


                                for (response in tempProducts) {


                                    if (response.product_title.toLowerCase()
                                            .contains(s.toString().toLowerCase())
                                    ) {


                                        filterList.add(response)

                                    }
                                }
                            }


                            binding.searchList.apply {

                                layoutManager = GridLayoutManager(context, 2)
                                adapter =
                                    SearchAdapter<ProductResponse>(FILTER.PRODUCTS, filterList)
                            }


                        }

                        FILTER.SHOP -> {


                            val filterList = mutableListOf<SellerResponse>()
                            binding.searchList.isVisible = true


                            if (savedShopsList != null) {

                                for (response in savedShopsList) {


                                    if (response.shop_title.toLowerCase()
                                            .contains(s.toString().toLowerCase())
                                    ) {


                                        filterList.add(response)

                                    }
                                }
                            }else {

                                for (response in shops) {


                                    if (response.shop_title.toLowerCase()
                                            .contains(s.toString().toLowerCase())
                                    ) {


                                        filterList.add(response)

                                    }
                                }
                            }

                            binding.searchList.apply {

                                layoutManager = GridLayoutManager(context, 2)
                                adapter = SearchAdapter<SellerResponse>(FILTER.SHOP, filterList)
                            }


                        }


                        FILTER.BRANDS -> {


                            val filterList = mutableListOf<HomeResponse>()
                            binding.searchList.isVisible = true


                            if (savedBrandsList != null) {

                                for (response in savedBrandsList) {


                                    if (response.title.toLowerCase()
                                            .contains(s.toString().toLowerCase())
                                    ) {


                                        filterList.add(response)

                                    }
                                }
                            }else {

                                for (response in homeProducts) {


                                    if (response.title.toLowerCase()
                                            .contains(s.toString().toLowerCase())
                                    ) {


                                        filterList.add(response)

                                    }
                                }
                            }

                            binding.searchList.apply {

                                layoutManager = GridLayoutManager(context, 2)
                                adapter = SearchAdapter<HomeResponse>(FILTER.BRANDS, filterList)
                            }


                        }
                        else -> {

                            val filterList = mutableListOf<ProductResponse>()
                            binding.searchList.isVisible = true


                            if (savedProductList != null) {

                                for (response in savedProductList) {


                                    if (response.product_title.toLowerCase()
                                            .contains(s.toString().toLowerCase())
                                    ) {


                                        filterList.add(response)

                                    }
                                }
                            }

                            binding.searchList.apply {

                                layoutManager = GridLayoutManager(context, 2)
                                adapter =
                                    SearchAdapter<ProductResponse>(FILTER.PRODUCTS, filterList)
                            }


                        }


                    }


                } else {


                    binding.searchList.isVisible = false
                }


            }


        })



        binding.fhSearchEdit.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                if (requireActivity().currentFocus != null) {
                    val inputManager: InputMethodManager =
                        requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    inputManager.hideSoftInputFromWindow(
                        requireActivity().currentFocus!!.getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS
                    )


                }

                return@OnEditorActionListener false
            }
            false
        })




        customToolbarRoot = requireActivity().findViewById<RelativeLayout>(R.id.toolbarParent)



        lifecycleScope.launchWhenStarted {
            homeViewModel.homeEvent.observe(viewLifecycleOwner, Observer { it ->
                when (it) {

                    is HomeViewModel.HomeEvent.SuccessBrands -> {
                        activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility =
                            View.GONE

                        binding.brandlist.apply {
                            layoutManager =
                                LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                            adapter =
                                HomeAdapter<HomeResponse>(
                                    it.resultText,
                                    R.layout.brand_item_list
                                )
                        }
                        homeProducts = it.resultText
                        Paper.init(context)
                        Paper.book().write(FILTER.BRANDS.name, it.resultText)

                    }

                    is HomeViewModel.HomeEvent.SuccessProduct -> {
                        activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility =
                            View.GONE

                        binding.productsList.apply {
                            layoutManager =
                                LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                            adapter =
                                HomeAdapter<ProductResponse>(
                                    it.resultText,
                                    R.layout.lyt_product_item
                                )
                        }

                        tempProducts = it.resultText

                        Paper.init(context)
                        Paper.book().write(FILTER.PRODUCTS.name, it.resultText)


                    }


                    is HomeViewModel.HomeEvent.SuccessShops -> {

                        shops = it.resultText

                        Paper.init(context)
                        Paper.book().write(FILTER.SHOP.name, it.resultText)


                    }


                    is HomeViewModel.HomeEvent.SliderData -> {
                        activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility =
                            View.GONE

                        sliderAdapter = SliderAdapter(requireContext())
                        for (data in it.data) {
                            sliderAdapter.addItem(
                                Slider(
                                    data.title,
                                    "${Constant.BASE_URL}${data.image}"
                                )
                            )

                        }

                        binding.imageSlider.setSliderAdapter(sliderAdapter)
                        binding.imageSlider.scrollTimeInMillis = 3000
                        binding.imageSlider.setIndicatorAnimation(IndicatorAnimationType.WORM);
                        binding.imageSlider.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
                        binding.imageSlider.startAutoCycle()


                    }

                    is HomeViewModel.HomeEvent.Failure -> {
                        activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility =
                            View.GONE

                        Toast.makeText(context, "Servor error occured", Toast.LENGTH_SHORT).show()
                    }

                    is HomeViewModel.HomeEvent.Loading -> {
                        activity?.findViewById<ProgressBar>(R.id.progressBar)?.visibility =
                            View.VISIBLE

                    }


                }
            })


        }

        lifecycleScope.launchWhenStarted {

            cartViewModel.cartItems.observe(viewLifecycleOwner, Observer {

                when (it) {
                    is CartFragmentViewModel.CartEvent.Loading -> {


                    }
                    is CartFragmentViewModel.CartEvent.GetCartItemSuccess -> {
//                        val bottomNavigationView = activity?.findViewById<BottomNavigationView>(R.id.bottomNavView)
//
//                        val actionLayout:MenuItem =  bottomNavigationView?.menu!!.findItem(R.id.bottom_cart)
//
//                        val rootLayout:FrameLayout = actionLayout.actionView.findViewById(R.id.rootLayout)
//
//                        val counterLayout = rootLayout.findViewById<FrameLayout>(R.id.cartRootLayout)
//                        counterLayout.visibility = View.VISIBLE


                    }
                }


            })
        }


        binding.fhNewsFeed.setOnClickListener {


            if (Constant.isUserLogged(requireActivity())!!) {

                navigateTo(it, R.id.newsFeedFragment)

            } else {

                startActivity(
                    Intent(
                        requireContext(),
                        LoginActivity::class.java
                    ).putExtra("from_news", true)
                )
            }
        }



        binding.fhCategories.setOnClickListener {

            navigateTo(it, R.id.categoryFragment)
        }


        binding.showAllProducts.setOnClickListener {


            Paper.init(context)
            Paper.book().write(Constant.IS_FROM_HOME, true)
            navigateTo(it, R.id.productListFragment)
        }

        binding.showAllBrands.setOnClickListener {

            navigateTo(it, R.id.brandsFragment)

        }

//        binding.fhConpaigns.setOnClickListener {
//            navigateTo(it, R.id.compaignsFragment)
//        }
//
//        binding.fhGifts.setOnClickListener {
//            navigateTo(it, R.id.giftFragment)
//        }

        binding.fhOrders.setOnClickListener {


            if (Constant.isUserLogged(requireActivity())!!) {

                navigateTo(it, R.id.ordersFragment)

            } else {

                startActivity(
                    Intent(
                        requireContext(),
                        LoginActivity::class.java
                    ).putExtra("from_orders", true)
                )
            }
        }











        binding.filterButton.setOnClickListener {

            var selectedFilter: String? = ""

            val dialog = Dialog(it.context)
            val dialogBinding = SelectFilterLayoutBinding.inflate(layoutInflater)
            dialog.setContentView(dialogBinding.root)
            dialog.show()

            Paper.init(context)
            val filter = Paper.book().read<FILTER>(Constant.FILTER)

            when (filter) {

                FILTER.PRODUCTS -> {

                    dialogBinding.productsRadio.isChecked = true
                    selectedFilter = "products"
                }

                FILTER.SHOP -> {

                    dialogBinding.shopsRadio.isChecked = true
                    selectedFilter = "shop"
                }

                FILTER.BRANDS -> {

                    dialogBinding.brandsRadio.isChecked = true
                    selectedFilter = "brands"
                }

            }



            dialogBinding.languageGroup.setOnCheckedChangeListener(object :
                RadioGroup.OnCheckedChangeListener {
                override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {

                    when (checkedId) {


                        R.id.shopsRadio -> {

                            selectedFilter = "shop"


                        }


                        R.id.productsRadio -> {

                            selectedFilter = "products"


                        }


                        R.id.brandsRadio -> {

                            selectedFilter = "brands"


                        }
                    }


                }

            })


            dialogBinding.applyBtn.setOnClickListener {


                Log.d("FILTERRRRR", "::" + selectedFilter)

                when (selectedFilter) {


                    "shop" -> {

                        Paper.init(context)
                        Paper.book().write(Constant.FILTER, FILTER.SHOP)

                    }

                    "products" -> {

                        Paper.init(context)
                        Paper.book().write(Constant.FILTER, FILTER.PRODUCTS)

                    }

                    "brands" -> {

                        Paper.init(context)
                        Paper.book().write(Constant.FILTER, FILTER.BRANDS)

                    }

                }



                dialog.dismiss()


            }

        }










        return binding.root

    }

    private fun navigateTo(view: View?, destinationId: Int?) {
        activity?.findNavController(R.id.nav_host_fragment)!!.navigate(destinationId!!)

    }

    override fun onPause() {
        super.onPause()

        customToolbarRoot.visibility = View.GONE
    }

    override fun onResume() {
        super.onResume()


        customToolbarRoot.visibility = View.VISIBLE
        HomeActivity.bottomNavigationView.menu.findItem(R.id.bottom_home).setChecked(true)


    }

}