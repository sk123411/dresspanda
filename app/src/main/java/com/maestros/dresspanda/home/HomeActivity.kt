package com.maestros.dresspanda.home

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.StrictMode
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.RadioGroup
import android.widget.Toast
import androidx.activity.viewModels
import com.google.android.material.navigation.NavigationView
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import com.akexorcist.localizationactivity.ui.LocalizationActivity
import com.facebook.login.LoginManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.maestros.dresspanda.R
import com.maestros.dresspanda.databinding.ActivityHomeBinding
import com.maestros.dresspanda.databinding.SelectThemeLayoutBinding
import com.maestros.dresspanda.helper.HelperViewModel
import com.maestros.dresspanda.home.ui.cart.CartFragmentViewModel
import com.maestros.dresspanda.home.ui.home.HomeViewModel
import com.maestros.dresspanda.login.LoginActivity
import com.maestros.dresspanda.login.LoginViewModel
import com.maestros.dresspanda.util.Constant
import com.maestros.dresspanda.util.Resource
import dagger.hilt.android.AndroidEntryPoint
import io.paperdb.Paper
import kotlinx.coroutines.launch

@AndroidEntryPoint
class HomeActivity : LocalizationActivity() {


    lateinit var binding: ActivityHomeBinding

    private lateinit var appBarConfiguration: AppBarConfiguration
    private val loginViewModel: LoginViewModel by viewModels()

    private val cartViewModel: CartFragmentViewModel by viewModels()

    companion object {
        lateinit var bottomNavigationView: BottomNavigationView
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setLanguage(Constant.getLanguage(context = applicationContext))
        super.onCreate(savedInstanceState)

        val builder: StrictMode.VmPolicy.Builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
        Paper.init(this)






        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        bottomNavigationView = binding.barMain.contentMain.bottomNavView



        if (Constant.getUserID(applicationContext)!=null) {
            cartViewModel.getCartItems(Constant.getUserID(applicationContext))
        }
        setSupportActionBar(binding.barMain.toolbarMain.toolbar)

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)





       val fromDetail =  intent.getBooleanExtra("from_detail",false)
        val fromNews = intent.getBooleanExtra("from_news",false)
        val fromOrders= intent.getBooleanExtra("from_orders",false)







        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home

            ), drawerLayout
        )

        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)



        if (Constant.isUserLogged(this)!!) {

            if (fromDetail){

                navController.navigate(R.id.shopList)

            }else if (fromNews){
                navController.navigate(R.id.newsFeedFragment)

            }else if (fromOrders){
                navController.navigate(R.id.ordersList)


            }


        } else {

            val wishlistMenu = navView.menu.findItem(R.id.nav_wishlist)
            val dashboardMenu = navView.menu.findItem(R.id.nav_dashboard)
            val shopsMenu = navView.menu.findItem(R.id.nav_shop)
            val ordersMenu = navView.menu.findItem(R.id.nav_orders)
            val messagesMenu = navView.menu.findItem(R.id.nav_messages)
            val loginMenu = navView.menu.findItem(R.id.nav_sign_out)
            val sellerMenu = navView.menu.findItem(R.id.nav_apply_as_seller)
            val managerMenu = navView.menu.findItem(R.id.nav_apply_as_manager)


            wishlistMenu.isVisible = false

            dashboardMenu.isVisible = false
            shopsMenu.isVisible = false
            ordersMenu.isVisible = false
            messagesMenu.isVisible = false
            sellerMenu.isVisible = false
            managerMenu.isVisible = false

            loginMenu.setTitle("Log in")


        }

        navView.setNavigationItemSelectedListener { it ->


            if (it.itemId == R.id.nav_wishlist) {

                navController.navigate(R.id.wishlistFragment)

                binding.drawerLayout.closeDrawer(GravityCompat.START)

                false
            } else if (it.itemId == R.id.nav_contact) {

                navController.navigate(R.id.contactUsFragment)

                binding.drawerLayout.closeDrawer(GravityCompat.START)

                false
            } else if (it.itemId == R.id.nav_dashboard) {

                navController.navigate(R.id.myAccountFragment)

                binding.drawerLayout.closeDrawer(GravityCompat.START)

                false
            } else if (it.itemId == R.id.nav_shop) {


                navController.navigate(R.id.followedShopFragment)

                binding.drawerLayout.closeDrawer(GravityCompat.START)

                false
            } else if (it.itemId == R.id.nav_orders) {

                navController.navigate(R.id.ordersFragment)

                binding.drawerLayout.closeDrawer(GravityCompat.START)

                false
            } else if (it.itemId == R.id.nav_dashboard) {


                navController.navigate(R.id.myAccountFragment)

                binding.drawerLayout.closeDrawer(GravityCompat.START)

                false
            } else if (it.itemId == R.id.nav_messages) {

                navController.navigate(R.id.chatFragment)

                binding.drawerLayout.closeDrawer(GravityCompat.START)

                false
            } else if (it.itemId == R.id.nav_apply_as_seller) {
                navController.navigate(R.id.applySellerFragment)
                binding.drawerLayout.closeDrawer(GravityCompat.START)

                false
            } else if (it.itemId == R.id.nav_apply_as_manager) {
                navController.navigate(R.id.applyManagerFragment)
                binding.drawerLayout.closeDrawer(GravityCompat.START)

                false

            } else if (it.itemId == R.id.all_shop) {
                Paper.init(applicationContext)
                Paper.book().write(Constant.FROM_BRANDS, 0)
                navController.navigate(R.id.shoplistFragment)
                binding.drawerLayout.closeDrawer(GravityCompat.START)

                false

            } else if (it.itemId == R.id.nav_sign_out) {
                loginViewModel.saveUserLogin(0)
                LoginManager.getInstance().logOut()
                startActivity(
                    Intent(this@HomeActivity, LoginActivity::class.java).addFlags(
                        Intent.FLAG_ACTIVITY_CLEAR_TASK
                    ).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                )

                false

            } else if (it.itemId == R.id.nav_dark_mode) {
                openDialog()
                false
            } else if (it.itemId == R.id.nav_cart) {

                navController.navigate(R.id.cartFragment)
                false
            }

            false

        }


        navController.addOnDestinationChangedListener(object :
            NavController.OnDestinationChangedListener {
            override fun onDestinationChanged(
                controller: NavController,
                destination: NavDestination,
                arguments: Bundle?
            ) {

                when (destination.id) {
//
//                    R.id.nav_home -> bottomNavigationView.selectedItemId = R.id.bottom_home
//                    R.id.wishlistFragment -> bottomNavigationView.selectedItemId = R.id.bottom_wishlist
//                    R.id.cartFragment -> bottomNavigationView.selectedItemId = R.id.bottom_cart
//                    R.id.nav_dashboard -> bottomNavigationView.selectedItemId = R.id.bottom_profile

                    R.id.productDetailFragment -> {

                        binding.barMain.toolbarMain.toolbar.visibility = View.GONE
                    }
                    else -> {
                        binding.barMain.toolbarMain.toolbar.visibility = View.VISIBLE

                    }
                }


            }

        })


        binding.barMain.contentMain.bottomNavView.setOnNavigationItemSelectedListener { item ->

            val root = binding.barMain.contentMain.bottomNavView
            when (item.itemId) {
                R.id.bottom_home -> {

//                    val menuItem:MenuItem = root.menu.findItem(R.id.bottom_home)
//                    menuItem.
                    navController.navigate(R.id.nav_home)
                    true
                }
                R.id.bottom_wishlist -> {


                    if (Constant.isUserLogged(this)!!) {
                        navController.navigate(R.id.wishlistFragment)


                    } else {

                        startActivity(
                            Intent(this@HomeActivity, LoginActivity::class.java).addFlags(
                                Intent.FLAG_ACTIVITY_CLEAR_TASK
                            ).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        )


                    }


                    // Respond to navigation item 2 click
                    true
                }
                R.id.bottom_profile -> {

                    if (Constant.isUserLogged(this)!!) {
                        navController.navigate(R.id.myAccountFragment)


                    } else {

                        startActivity(
                            Intent(this@HomeActivity, LoginActivity::class.java).addFlags(
                                Intent.FLAG_ACTIVITY_CLEAR_TASK
                            ).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        )


                    }

                    // Respond to navigation item 2 click
                    true
                }

                R.id.bottom_cart -> {


                    if (Constant.isUserLogged(this)!!) {
                        navController.navigate(R.id.cartFragment)


                    } else {

                        startActivity(
                            Intent(this@HomeActivity, LoginActivity::class.java).addFlags(
                                Intent.FLAG_ACTIVITY_CLEAR_TASK
                            ).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        )


                    }

                    true
                }
                else -> false
            }


        }





        lifecycleScope.launchWhenStarted {

            cartViewModel.cartItems.observe(this@HomeActivity, Observer {

                when (it) {


                    is CartFragmentViewModel.CartEvent.GetCartItemSuccess -> {

                        Constant.setCartCount(
                            bottomNavigationView,
                            it.cartItems.qntity
                        )
                    }
                    is CartFragmentViewModel.CartEvent.Failure -> {


                    }
                }


            })
        }

    }


    private fun openDialog() {
        val dialog = Dialog(this)
        val binding = SelectThemeLayoutBinding.inflate(layoutInflater)
        dialog.setContentView(binding.root)
        dialog.show()
        var radioStatusText: Int? = 0


        when (getThemeStatus()) {
            0 -> binding.lightRadio.isChecked = true
            1 -> binding.darkRadio.isChecked = true
        }


        binding.radioGrounpTheme.setOnCheckedChangeListener(object :
            RadioGroup.OnCheckedChangeListener {
            override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {

                if (checkedId == R.id.lightRadio) {
                    radioStatusText = 0
                    saveThemeOption(radioStatusText)

                } else {

                    radioStatusText = 1
                    saveThemeOption(radioStatusText)
                }


            }

        })

        binding.changeBtn.setOnClickListener {
            if (radioStatusText == 0) {

                dialog.dismiss()

                startActivity(
                    Intent(this, HomeActivity::class.java).addFlags(
                        Intent.FLAG_ACTIVITY_CLEAR_TASK
                    ).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                )

            } else {

                dialog.dismiss()
                startActivity(
                    Intent(this, HomeActivity::class.java).addFlags(
                        Intent.FLAG_ACTIVITY_CLEAR_TASK
                    ).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                )

            }
        }

    }


    private fun saveThemeOption(themeStatus: Int?) {

        Paper.init(this)
        Paper.book().write(Constant.THEME_OPTION, themeStatus)

    }

    private fun getThemeStatus(): Int? {
        Paper.init(this)
        return Paper.book().read<Int>(Constant.THEME_OPTION, 0)
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        //    menuInflater.inflate(R.menu.home, menu)
        return true
    }


    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) ||
                super.onSupportNavigateUp()
    }


    override fun onStart() {
        super.onStart()
        when (getThemeStatus()) {
            0 -> {

                AppCompatDelegate
                    .setDefaultNightMode(
                        AppCompatDelegate
                            .MODE_NIGHT_NO
                    );

            }
            1 -> {
                AppCompatDelegate
                    .setDefaultNightMode(
                        AppCompatDelegate
                            .MODE_NIGHT_YES
                    )

            }
        }
    }


}


