package com.maestros.dresspanda.home.ui.newsfeed.adapter.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class LikeCommentData (
    @SerializedName("id")
    @Expose
    val id:String?,
    @SerializedName("user_id")
    @Expose
    val user_id:String?,
    @SerializedName("post_id")
    @Expose
    val post_id:String?,
    @SerializedName("comment")
    @Expose
    val comment:String?
    ) {
}