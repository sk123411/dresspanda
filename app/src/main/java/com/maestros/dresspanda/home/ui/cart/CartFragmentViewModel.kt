package com.maestros.dresspanda.home.ui.cart

import android.content.Context
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.maestros.dresspanda.home.ui.cart.repository.Cart
import com.maestros.dresspanda.home.ui.cart.repository.CartX
import com.maestros.dresspanda.home.ui.product.ProductViewModel
import com.maestros.dresspanda.home.ui.wishlist.model.WishlistProduct
import com.maestros.dresspanda.util.Resource
import kotlinx.coroutines.launch
import org.json.JSONObject


class CartFragmentViewModel @ViewModelInject constructor(
    val cart: Cart, val context: Context
) : ViewModel() {


    private val _cartItems = MutableLiveData<CartEvent>()

     val cartItems: LiveData<CartEvent>
        get() = _cartItems




    sealed class CartEvent {
        class GetCartItemSuccess(val cartItems: CartX) : CartEvent()
        class UpdateCartItemSuccess(val cartItems: CartX) : CartEvent()
        class DeleteCartItemSuccess(val jsonObject: JSONObject) : CartEvent()
        class Failure(message: String) : CartEvent()
        object Loading:CartEvent()
        object Empty:CartEvent()
    }




    fun getCartItems(userId:String?){

        val map = HashMap<String,String>()
        map.put("user_id", userId!!)

        viewModelScope.launch {


            val response = cart.showCartItems("show_cart", map)

            when(response){

                is Resource.Success->{
                    _cartItems.value = CartEvent.GetCartItemSuccess(response.data!!)
                }
                is Resource.Error-> _cartItems.value = CartEvent.Failure(response.message!!)
            }


        }

    }

    fun updateCartItem(userId:String?,productId:String?,quantity:String?){

        val map = HashMap<String,String>()
        map.put("user_id", userId!!)
        map.put("quantity", quantity!!)
        map.put("id", productId!!)

        viewModelScope.launch {


            val response = cart.updateCartItem("update_cart", map)

            when(response){

                is Resource.Success->{
                    _cartItems.value = CartEvent.UpdateCartItemSuccess(response.data!!)
                }
                is Resource.Error-> _cartItems.value = CartEvent.Failure(response.message!!)
            }


        }




    }



    fun deleteCartItem(userId:String?,productId:String?){

        val map = HashMap<String,String>()
        map.put("user_id", userId!!)
        map.put("id", productId!!)

        viewModelScope.launch {


            val response = cart.deleteCartItem("delete_cart", map)

            when(response){

                is Resource.Success->{
                    _cartItems.value = CartEvent.DeleteCartItemSuccess(response.data!!)
                }
                is Resource.Error-> _cartItems.value = CartEvent.Failure(response.message!!)
            }


        }

    }

}