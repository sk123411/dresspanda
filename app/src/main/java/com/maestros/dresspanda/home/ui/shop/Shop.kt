package com.maestros.dresspanda.home.ui.shop

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Shop(
    @SerializedName("Payment_method")
    @Expose
    val Payment_method: String,
    @SerializedName("address")
    @Expose
    val address: String,
    @SerializedName("aprove_status")
    @Expose
    val aprove_status: String,
    @SerializedName("brand")
    @Expose
    val brand: String,
    @SerializedName("category")
    @Expose
    val category: String,
    @SerializedName("city_id")
    @Expose
    val city_id: String,
    @SerializedName("email")
    @Expose
    val email: String,
    @SerializedName("id")
    @Expose
    val id: String,
    @SerializedName("mobile2")
    @Expose
    val mobile2: String,
    @SerializedName("name")
    @Expose
    val name: String,
    @SerializedName("password")
    @Expose
    val password: String,
    @SerializedName("path")
    @Expose
    val path: String,
    @SerializedName("pincode")
    @Expose
    val pincode: String,
    @SerializedName("result")
    @Expose
    val result: String,
    @SerializedName("role")
    @Expose
    val role: String,
    @SerializedName("shop_title")
    @Expose
    val shop_title: String,
    @SerializedName("store_image")
    @Expose
    val store_image: String,
    @SerializedName("store_name")
    @Expose
    val store_name: String
)