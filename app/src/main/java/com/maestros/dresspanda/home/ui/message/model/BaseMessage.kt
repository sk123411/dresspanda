package com.maestros.dresspanda.home.ui.message.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class BaseMessage<T>(
    @SerializedName("data")
    @Expose
    val list: List<T>,

    @SerializedName("result")
    @Expose
    val status:Boolean?
)