package com.maestros.dresspanda.login.repository

import com.maestros.dresspanda.home.ui.brand.BaseResponse
import com.maestros.dresspanda.home.ui.brand.HomeResponse
import com.maestros.dresspanda.home.ui.category.adapter.model.CategoryResponse
import com.maestros.dresspanda.home.ui.category.adapter.model.SubCategoryResponse
import com.maestros.dresspanda.login.model.ForgetPassword
import com.maestros.dresspanda.signup.model.Register
import com.maestros.dresspanda.util.Resource
import retrofit2.Response
import retrofit2.http.FieldMap
import retrofit2.http.POST
import retrofit2.http.Query

interface Category {
    suspend fun getCategories(category:String?):Resource<BaseResponse<CategoryResponse>>
    suspend fun getSubCategoriesByCategory(category:String?, map:HashMap<String,String>):
            Resource<BaseResponse<SubCategoryResponse>>

}