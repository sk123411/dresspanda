package com.maestros.dresspanda.home.ui.address.repository.allAddressModel


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class DataX(
    @SerializedName("all_address")
    @Expose
    val allAddress: List<Data>,
    @SerializedName("aouth_id")
    @Expose
    val aouthId: String,
    @SerializedName("aouth_provider")
    @Expose
    val aouthProvider: String,
    @SerializedName("DOB")
    @Expose
    val dOB: String,
    @SerializedName("email")
    @Expose
    val email: String,
    @SerializedName("gender")
    @Expose
    val gender: String,
    @SerializedName("id")
    @Expose
    val id: String,
    @SerializedName("image")
    @Expose
    val image: String,
    @SerializedName("name")
    @Expose
    val name: String,
    @SerializedName("password")
    @Expose
    val password: String,
    @SerializedName("phone_number")
    @Expose
    val phoneNumber: String,
    @SerializedName("regid")
    @Expose
    val regid: String
)