package com.maestros.dresspanda.home.ui.home

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.maestros.dresspanda.R
import com.maestros.dresspanda.data.address.AddressDAO
import com.maestros.dresspanda.databinding.*
import com.maestros.dresspanda.home.ui.address.Address
import com.maestros.dresspanda.home.ui.brand.HomeResponse
import com.maestros.dresspanda.home.ui.category.adapter.model.CategoryResponse
import com.maestros.dresspanda.home.ui.category.adapter.model.SubCategoryResponse
import com.maestros.dresspanda.home.ui.message.Message
import com.maestros.dresspanda.home.ui.message.MessagesList
import com.maestros.dresspanda.home.ui.product.model.productdetail.ProductResponse
import com.maestros.dresspanda.home.ui.shop.Shop
import com.maestros.dresspanda.home.ui.wishlist.model.WishlistShop
import com.maestros.dresspanda.paper.PaperImplementation
import com.maestros.dresspanda.seller.repository.model.SellerResponse
import com.maestros.dresspanda.util.Constant
import com.squareup.picasso.Picasso
import io.paperdb.Paper
import javax.inject.Inject

class HomeAdapter<T>(val list: List<T>, val layout: Int) :


    RecyclerView.Adapter<HomeAdapter.MyViewHolder>() {

    class MyViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

        val langCode = Paper.book().read<String>(Constant.LANGUAGE)

        @Inject
        lateinit var address: AddressDAO



        fun bindCategory(category: Any?) {

            if (category is CategoryResponse) {
                val binding = LytCategoryItemBinding.bind(view)

                Picasso.get().load(Constant.BASE_URL + category.image)
                    .placeholder(R.mipmap.ic_launcher_round).into(binding.lytCatImage)



                if (langCode.equals("bn")) {


                    if (category.bangla_name.isEmpty()) {
                        binding.lytCatTitle.text = category.title

                    } else {
                        binding.lytCatTitle.text = category.bangla_name

                    }
                } else {
                    binding.lytCatTitle.text = category.title


                }



                binding.root.setOnClickListener {
                    val bundle = Bundle()
                    val id = category.id
                    bundle.putString("id", id)
                    bundle.putString("title", category.title)
                    Paper.init(it.context)
                    Paper.book().write(Constant.IS_FROM_HOME, false)
                    // Send category id here
                    Navigation.findNavController(binding.root)
                        .navigate(R.id.subCategoryFragment, bundle)

                }


            } else if (category is ProductResponse) {

                val productListBinding = LytProductItemBinding.bind(view)




                if (langCode.equals("bn")) {


                    if (category.product_title_bangla.isEmpty()) {
                        productListBinding.ppTitle.text = category.product_title

                    } else {
                        productListBinding.ppTitle.text = category.product_title_bangla

                    }
                } else {
                    productListBinding.ppTitle.text = category.product_title


                }







                productListBinding.ppPrice.text = Constant.CURRENCYSYMBOL + category.selling_price
                Picasso.get().load(Constant.BASE_URL + category.image)
                    .placeholder(R.mipmap.ic_launcher_round).into(productListBinding.ppImage)
                productListBinding.root.setOnClickListener {

                    Paper.init(it.context)
                    Paper.book().write(Constant.PRODUCT_DETAIL, category)
                    Navigation.findNavController(productListBinding.root)
                        .navigate(R.id.productDetailFragment)

                }

            } else if (category is HomeResponse) {
                val brandItemListBinding = BrandItemListBinding.bind(view)
                Picasso.get().load(Constant.BASE_URL + category.image)
                    .placeholder(R.mipmap.ic_launcher_round)
                    .into(brandItemListBinding.bbImage)
                brandItemListBinding.bbTitle.visibility = View.VISIBLE



                if (langCode.equals("bn")) {


                    if (category.name_bangla.isEmpty()) {
                        brandItemListBinding.bbTitle.text = category.title

                    } else {
                        brandItemListBinding.bbTitle.text = category.name_bangla

                    }
                } else {
                    brandItemListBinding.bbTitle.text = category.title


                }








                brandItemListBinding.root.setOnClickListener {

                    Paper.init(it.context)
                    Paper.book().write(Constant.FROM_BRANDS, 1)
                    val bundle = Bundle()
                    bundle.putString("brand_id", category.id)
                    bundle.putString("title", category.title)
                    Navigation.findNavController(brandItemListBinding.root)
                        .navigate(R.id.shoplistFragment, bundle)


                }

            } else if (category is Shop) {

                val shopItemListBinding = FollowShopItemBinding.bind(view)

                Picasso.get().load(Constant.BASE_URL + category.store_image)
                    .placeholder(R.mipmap.ic_launcher_round)
                    .into(shopItemListBinding.imageView2)
                shopItemListBinding.shopTitle.text = category.store_name

            } else if (category is MessagesList) {

                val messageItemBinding = MessageItemBinding.bind(view)
                messageItemBinding.miName.text = category.name
                messageItemBinding.miMessage.text = category.message
                messageItemBinding.miTimeAgo.text = category.dates
                //   messageItemBinding.miTimeAgoDet.text = category.strtotime

                messageItemBinding.miThumbName.text = category.name.substring(0, 1)


                messageItemBinding.root.setOnClickListener {
                    Paper.init(it.context)
                    Paper.book().write("SENDER_ID", category.reciverId)
                    Navigation.findNavController(it).navigate(R.id.sendChatMessageFragment)

                }


            } else if (category is SubCategoryResponse) {

                val brandItemListBinding = BrandItemListBinding.bind(view)
                Picasso.get().load(Constant.BASE_URL + category.image)
                    .placeholder(R.mipmap.ic_launcher_round)
                    .into(brandItemListBinding.bbImage)
                brandItemListBinding.bbTitle.visibility = View.VISIBLE

                if (langCode.equals("bn")) {


                    if (category.bangla_name.isEmpty()) {
                        brandItemListBinding.bbTitle.text = category.title

                    } else {
                        brandItemListBinding.bbTitle.text = category.bangla_name

                    }
                } else {
                    brandItemListBinding.bbTitle.text = category.title


                }




                brandItemListBinding.root.setOnClickListener {
                    val bundle = Bundle()
                    val id = category.id
                    bundle.putString("id", id)
                    Paper.init(it.context)

                    Paper.book().write(Constant.IS_FROM_HOME, false)
                    // Send subcategory id here
                    Navigation.findNavController(brandItemListBinding.root)
                        .navigate(R.id.productListFragment, bundle)


                }

            } else if (category is SellerResponse) {
                val brandItemListBinding = BrandItemListBinding.bind(view)
                Picasso.get().load(Constant.BASE_URL + category.store_image)
                    .placeholder(R.mipmap.ic_launcher_round)
                    .into(brandItemListBinding.bbImage)
                brandItemListBinding.bbTitle.visibility = View.VISIBLE
                brandItemListBinding.bbTitle.text = category.store_name

                brandItemListBinding.root.setOnClickListener {
                    Paper.init(it.context)
                    Paper.book().write(Constant.SHOP_DETAILS, category)
                    Paper.book().write(Constant.SHOP_WISHLIST_DETAILS, category)

                    val data = Bundle()
                    data.putBoolean(Constant.FROM_FOLLOWED_SHOPS, false)
                    Navigation.findNavController(brandItemListBinding.root)
                        .navigate(R.id.shopDetailsFragment, data)
                }


            } else if (category is WishlistShop) {
                val brandItemListBinding = BrandItemListBinding.bind(view)
                Picasso.get().load(Constant.BASE_URL + category.store_image)
                    .placeholder(R.mipmap.ic_launcher_round)
                    .into(brandItemListBinding.bbImage)
                brandItemListBinding.bbTitle.visibility = View.VISIBLE
                brandItemListBinding.bbTitle.text = category.store_name

                brandItemListBinding.root.setOnClickListener {
                    Paper.init(it.context)
                    Paper.book().write(Constant.SHOP_WISHLIST_DETAILS, category)
                    val data = Bundle()

                    Log.d("WISHSHHHHHHH", "::" + category.toString());


                    data.putBoolean(Constant.FROM_FOLLOWED_SHOPS, true)
                    Navigation.findNavController(brandItemListBinding.root)
                        .navigate(R.id.shopDetailsFragment, data)


                }

            }
        }

    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {

        val binding = LayoutInflater.from(parent.context).inflate(
            layout,
            parent, false
        )

        Paper.init(binding.context)

        return MyViewHolder(view = binding.rootView)


    }

    override fun getItemCount(): Int {

        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.bindCategory(list.get(position))


    }

}