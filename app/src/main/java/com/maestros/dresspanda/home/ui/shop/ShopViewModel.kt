package com.maestros.dresspanda.home.ui.shop

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.maestros.dresspanda.data.shop.ShopsDao
import com.maestros.dresspanda.data.wishlist.ShopWishlistDao
import com.maestros.dresspanda.home.ui.category.adapter.model.CategoryResponse
import com.maestros.dresspanda.home.ui.home.HomeViewModel
import com.maestros.dresspanda.home.ui.product.model.productdetail.ProductResponse
import com.maestros.dresspanda.home.ui.shop.repository.Shop
import com.maestros.dresspanda.home.ui.wishlist.model.WishlistShop
import com.maestros.dresspanda.seller.repository.model.SellerResponse
import com.maestros.dresspanda.util.Resource
import kotlinx.coroutines.launch

class ShopViewModel @ViewModelInject constructor(
    private val shopsDao: ShopsDao,
    private val shop:Shop,
    private val wishlistDao: ShopWishlistDao
):ViewModel(){

    val _shops = MutableLiveData<ShopEvent>()
    val shops:LiveData<ShopEvent>
        get() = _shops

    val shopsWishList_ = MutableLiveData<MutableList<WishlistShop>>()

    val shopsWishList:LiveData<MutableList<WishlistShop>>
        get() = shopsWishList_




    sealed class ShopEvent {
        class Failure(val errorText: String): ShopEvent()
        object Loading : ShopEvent()
        object Empty : ShopEvent()
        class SuccessShops(val resultText: List<SellerResponse>): ShopEvent()
        class SuccessProducts(val resultText: List<ProductResponse>): ShopEvent()
        class SuccessCategories(val resultText: List<CategoryResponse>): ShopEvent()
        class ProductOnDb(val boolean: Boolean, productResponse: WishlistShop): ShopEvent()


    }



    fun getShops():ShopViewModel.ShopEvent{

        _shops.value = ShopViewModel.ShopEvent.Loading
        viewModelScope.launch {
            if (shopsDao.getAllShops().isNotEmpty()){
                _shops.value = ShopEvent.SuccessShops(shopsDao.getAllShops())
                return@launch

            }
            val response = shop.getShops(action = "get_all_shops")
            when(response){

                is Resource.Success->{



                    _shops.value = ShopViewModel.ShopEvent.SuccessShops(response.data!!)

                }
                is Resource.Error-> _shops.value = ShopViewModel.ShopEvent.Failure(response.message!!)
            }

        }
        return _shops.value!!

    }



    fun getShopsByBrandId(id:String?){
        val map = HashMap<String,String>()
        map.put("brand_id", id!!)

        _shops.value = ShopViewModel.ShopEvent.Loading


        viewModelScope.launch {


            val response = shop.getShopsByBrandID(action = "get_shop_by_brand",map = map)
            when(response){

                is Resource.Success->{

                    if (response.data!!.status!!){

                        _shops.value = ShopViewModel.ShopEvent.SuccessShops(response.data.list)

                    }else {
                        _shops.value = ShopViewModel.ShopEvent.Failure("Server error occured")
                    }


                }
                is Resource.Error-> _shops.value = ShopViewModel.ShopEvent.Failure(response.message!!)
            }

        }

    }


    fun getProductsByShopId(id:String?):ShopViewModel.ShopEvent{

        val map = HashMap<String,String>()
        map.put("id", id!!)

        _shops.value = ShopViewModel.ShopEvent.Loading
        viewModelScope.launch {

            val response = shop.getProductsByShopID(action = "get_productby_shopid",data = map)

            when(response){

                is Resource.Success->{


                    if(response.data!!.status!!) {
                        _shops.value = ShopViewModel.ShopEvent.SuccessProducts(response.data.list)
                    }else {

                        _shops.value = ShopViewModel.ShopEvent.Failure("Some error occured")
                    }

                }
                is Resource.Error-> _shops.value = ShopViewModel.ShopEvent.Failure(response.message!!)
            }

        }
        return _shops.value!!

    }


    fun getCategoriesByShopId(id:String?):ShopViewModel.ShopEvent{
        val map = HashMap<String,String>()
        map.put("id", id!!)
        _shops.value = ShopViewModel.ShopEvent.Loading
        viewModelScope.launch {

            val response = shop.getCategoriesByShopID(action = "get_categoryby_shopid",data = map)

            when(response){

                is Resource.Success->{

                    if(response.data!!.status!!) {
                        _shops.value = ShopViewModel.ShopEvent.SuccessCategories(response.data.list)
                    }else {

                        _shops.value = ShopViewModel.ShopEvent.Failure("Some error occured")
                    }
                }
                is Resource.Error-> _shops.value = ShopViewModel.ShopEvent.Failure("Some error occured")
            }

        }
        return _shops.value!!

    }


    fun saveShop(wishlistShop: WishlistShop){

        viewModelScope.launch {

            wishlistDao.insertOrUpdate(wishlistShop)
            val isProductOnDatabase = wishlistDao.isShopOnDB(wishlistShop)
            _shops.value = ShopViewModel.ShopEvent.ProductOnDb(isProductOnDatabase, wishlistShop)

        }
    }


    fun getWishlistShops(){

        viewModelScope.launch {

            val myList: MutableList<WishlistShop> = mutableListOf<WishlistShop>()
            myList.addAll(wishlistDao.getAllShops())

            shopsWishList_.value = myList
        }
    }


    fun isProductOnDB(productResponse: WishlistShop){
        viewModelScope.launch {
            val isProductOnDatabase = wishlistDao.isShopOnDB(productResponse)
            _shops.value = ShopViewModel.ShopEvent.ProductOnDb(isProductOnDatabase, productResponse)
        }
    }


    fun unfollowShop(productResponse: WishlistShop){

        viewModelScope.launch {
            wishlistDao.deleteShop(productResponse)
            val isProductOnDatabase = wishlistDao.isShopOnDB(productResponse)
            _shops.value = ShopViewModel.ShopEvent.ProductOnDb(isProductOnDatabase, productResponse)
        }
    }












}