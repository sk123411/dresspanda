package com.maestros.dresspanda.home.ui.newsfeed.adapter.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class LikeCommentResponse(
    @SerializedName("result")
    @Expose
    val result:Boolean?,
    @SerializedName("data")
    @Expose
    val data:LikeCommentData?
    ) {



}