package com.maestros.dresspanda.home.ui.orders.repository

import com.maestros.dresspanda.home.ui.brand.BaseResponse
import com.maestros.dresspanda.home.ui.notification.Notification
import com.maestros.dresspanda.home.ui.orders.repository.model.CheckoutResponse
import com.maestros.dresspanda.home.ui.orders.repository.model.OrderResponse
import com.maestros.dresspanda.util.Resource
import retrofit2.http.FieldMap

interface Orders {

    suspend fun addOrder(action:String?, map:HashMap<String,String>):Resource<CheckoutResponse>


    suspend fun showOrders(action:String?, map:HashMap<String,String>):Resource<OrderResponse>

    suspend fun addNotification(banner:String?, @FieldMap map:HashMap<String,String>): Resource<Notification>


    suspend fun getNotification(banner:String?, map:HashMap<String,String>): Resource<BaseResponse<Notification>>
}