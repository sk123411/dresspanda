package com.maestros.dresspanda.home.ui.message.repository

import com.maestros.dresspanda.home.ui.brand.BaseResponse
import com.maestros.dresspanda.home.ui.message.Message
import com.maestros.dresspanda.home.ui.message.MessagesList
import com.maestros.dresspanda.home.ui.message.model.BaseMessage
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*

interface ChatAPI {

    @POST("dresspanda/api/process.php")
    @FormUrlEncoded
    suspend fun sendMessage(
        @Query("action") action: String?,
        @FieldMap data: HashMap<String, String>
    ): Response<Message>

    @POST("dresspanda/api/process.php")
    @FormUrlEncoded
    suspend fun showMessage(
        @Query("action") action: String?,
        @FieldMap data: HashMap<String, String>
    ):  Response<BaseMessage<MessagesList>>

    @Multipart
    @POST("dresspanda/api/process.php")
    suspend fun sendMessageWithImage(
        @Query("action") action: String?,
        @Part image: MultipartBody.Part?,
        @Part("user_id") userID: RequestBody?,

        @Part("messages") message: RequestBody?,
        @Part("sender_id") senderID: RequestBody?,
        @Part("reciver_id") receiverID: RequestBody?
    ): Response<Message>


    @POST("dresspanda/api/process.php")
    @FormUrlEncoded
    suspend fun showAllMessages(
        @Query("action") action: String?,
        @FieldMap data: HashMap<String, String>
    ): Response<BaseMessage<MessagesList>>


}