package com.maestros.dresspanda.home.ui.wishlist.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.maestros.dresspanda.R
import com.maestros.dresspanda.databinding.AddressItemListBinding
import com.maestros.dresspanda.home.ui.address.AddressListFragment
import com.maestros.dresspanda.home.ui.address.AddressViewModel
import com.maestros.dresspanda.home.ui.address.repository.allAddressModel.Data
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class AddressAdapter(val list:MutableList<Data>,
                     val wishlistViewModel: AddressViewModel,
                     val lifeCycleScope: CoroutineScope,
                        val addressListFragment: AddressListFragment
) :
    RecyclerView.Adapter<AddressAdapter.ViewHolder>() {



    class ViewHolder(view:View): RecyclerView.ViewHolder(view) {
        val binding = AddressItemListBinding.bind(view)




        fun bindData(
            address: Data,
            wishlistViewModel: AddressViewModel,
            lifeCycleScope: CoroutineScope,
            wishlistAdapter: AddressAdapter,
            addressListFragment: AddressListFragment
        ){


            binding.name.setText(address.name)
            binding.addressContent.setText(binding.root.resources.getString(R.string.address)+": "+address.address)
            binding.addressBuilding.setText(binding.root.resources.getString(R.string.buildingNumber)+": "+address.address)
            binding.addressStreet.setText(binding.root.resources.getString(R.string.streetNumber)+": "+address.address)
            binding.addressPincode.setText(binding.root.resources.getString(R.string.pincode)+": "+address.address)
            binding.addressCity.setText(binding.root.resources.getString(R.string.city)+": "+address.address)


            binding.root.setOnClickListener {
                addressListFragment.openAddressDialog(address,true)


            }

            binding.deleteAddressButton.setOnClickListener {
                deleteProduct(
                    wishlistViewModel,
                    address,
                    lifeCycleScope,
                    wishlistAdapter)

            }


        }

        private fun deleteProduct(addressViewModel: AddressViewModel,
                                  wish: Data,
                                  lifeCycleScope: CoroutineScope,
                                  adapter: AddressAdapter
                                  ) {
                lifeCycleScope.launch {

                    addressViewModel.deleteAddress(wish.id)
                    adapter.list.remove(wish)
                    adapter.notifyItemRemoved(adapterPosition)
                    adapter.notifyItemChanged(adapterPosition, adapter.list)

                }


        }


    }



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(R.layout.address_item_list,
        parent,false)

        return ViewHolder(view)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.bindData(
            list.get(position),
            wishlistViewModel,
            lifeCycleScope,
            this,
            addressListFragment)


    }
}