package com.maestros.dresspanda.home.profile.repository

import android.util.Log
import com.maestros.dresspanda.signup.model.Register
import com.maestros.dresspanda.util.Resource
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import javax.inject.Inject

class ProfileImplementation @Inject constructor(val profileAPI: ProfileAPI):Profile{
    override suspend fun changePassword(
        action: String?,
        data: HashMap<String, String>
    ): Resource<Register> {

        return try {
            val response = profileAPI.changePassword(action, data)
            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())
                Resource.Success(result)

            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }

    }

    override suspend fun changeName(updateProfile: String?, name: HashMap<String,String>): Resource<Register> {

        return try {
            val response = profileAPI.changeName(updateProfile, name)
            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())
                Resource.Success(result)

            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }

    }

    override suspend fun changeEmail(updateProfile: String?, name: HashMap<String,String>): Resource<Register> {
        return try {
            val response = profileAPI.changeName(updateProfile, name)
            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())
                Resource.Success(result)

            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }

    override suspend fun changeDOB(updateProfile: String?, name: HashMap<String,String>): Resource<Register> {
        return try {
            val response = profileAPI.changeName(updateProfile, name)
            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())
                Resource.Success(result)

            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }

    override suspend fun changeNumber(updateProfile: String?, data:HashMap<String,String>): Resource<Register> {
        return try {
            val response = profileAPI.changeName(updateProfile, data)
            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())
                Resource.Success(result)

            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }

    override suspend fun getProfile(
        action: String?,
        data : HashMap<String, String>
    ): Resource<Register> {
        return try {
            val response = profileAPI.getProfile(action, data)
            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())
                Resource.Success(result)

            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }

    override suspend fun changeImage(
        action: String?,
        image: MultipartBody.Part?,
        userId: RequestBody?
    ): Resource<Register> {
        return try {
            val response = profileAPI.changeImage(action, image,userId)
            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())
                Resource.Success(result)

            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }


}