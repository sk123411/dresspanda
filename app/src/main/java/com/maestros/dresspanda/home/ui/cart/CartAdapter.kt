package com.maestros.dresspanda.home.ui.cart

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.maestros.dresspanda.databinding.CartItemLytBinding
import com.maestros.dresspanda.home.ui.cart.repository.CartX
import com.maestros.dresspanda.home.ui.cart.repository.Product
import com.maestros.dresspanda.home.ui.product.model.productdetail.ProductResponse
import com.maestros.dresspanda.util.Constant
import com.squareup.picasso.Picasso
import io.paperdb.Paper

class CartAdapter(val cartProduct:MutableList<Product>,val cartFragmentViewModel: CartFragmentViewModel,val userId:String?):RecyclerView.Adapter<CartAdapter.MyViewHolder>(){
    class MyViewHolder(view:View):RecyclerView.ViewHolder(view) {

        val binding = CartItemLytBinding.bind(view)

        fun bindData(cartProuduct: Product,
                     cartlist:MutableList<Product>
                     ,cartFragmentViewModel: CartFragmentViewModel,
                     userId: String?,
                     cartAdapter: CartAdapter
                     ){

            Picasso.get().load(Constant.BASE_URL+cartProuduct.image).into(binding.productImage)
            binding.pTitle.setText(cartProuduct.productTitle)
            binding.quantityText.setText(cartProuduct.qunatity)
            binding.pPrice.setText(Constant.CURRENCYSYMBOL +" "+cartProuduct.sellingPrice)



            binding.addButton.setOnClickListener {

                var count:Int = binding.quantityText.text.toString().toInt()
                if (count>=0) {
                    count = count + 1
                    binding.quantityText.setText(count.toString())
                    setProductCartTotal(count, Integer.parseInt(cartProuduct.sellingPrice))


                }

                cartFragmentViewModel.updateCartItem(userId = userId,productId = cartProuduct.id,quantity = count.toString())

            }

            binding.minusButton.setOnClickListener {


                var count:Int = binding.quantityText.text.toString().toInt()

                if (count>=0) {

                    if(count==1) {
                        return@setOnClickListener
                    }
                    count = count - 1
                    binding.quantityText.setText(count.toString())
                    setProductCartTotal(count,cartProuduct.sellingPrice.toInt())
                }

                cartFragmentViewModel.updateCartItem(userId = userId,productId = cartProuduct.id,quantity = count.toString())


            }



            binding.removeButton.setOnClickListener {
                cartFragmentViewModel.deleteCartItem(userId = userId, productId = cartProuduct.id)
                cartlist.remove(cartProuduct)
                cartAdapter.notifyItemRemoved(adapterPosition)
                cartAdapter.notifyItemChanged(adapterPosition,cartlist.size)
                cartFragmentViewModel.getCartItems(userId)

            }


            setProductCartTotal(cartProuduct.qunatity.toInt(),cartProuduct.sellingPrice.toInt())


        }

        private fun setProductCartTotal(count: Int, totalPriceInt: Int) {
            val totalPrice = count * totalPriceInt
            binding.pPriceTotal.setText("${Constant.CURRENCYSYMBOL} "+totalPrice.toString())

        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val binding = CartItemLytBinding.inflate(LayoutInflater.from(parent.context))
        return MyViewHolder(binding.root)

    }

    override fun getItemCount(): Int {

        return cartProduct.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bindData(cartProuduct = cartProduct.get(position),
            cartlist = cartProduct,
            cartFragmentViewModel = cartFragmentViewModel,userId = userId,cartAdapter = this)
    }

}

