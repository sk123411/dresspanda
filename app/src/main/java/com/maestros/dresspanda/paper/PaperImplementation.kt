package com.maestros.dresspanda.paper

import android.content.Context
import com.maestros.dresspanda.util.Constant
import javax.inject.Inject


class PaperImplementation constructor(
    val context:Context
):Paper{

    init {

        initialize()
    }
    override fun initialize() {

        io.paperdb.Paper.init(context)
        io.paperdb.Paper.book().write(Constant.LANGUAGE,"en")
    }

}
