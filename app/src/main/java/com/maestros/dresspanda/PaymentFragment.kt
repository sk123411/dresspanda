package com.maestros.dresspanda

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.gson.Gson
import com.maestros.dresspanda.databinding.BottomCustomerDetailsBinding
import com.maestros.dresspanda.databinding.FragmentPaymentBinding
import com.maestros.dresspanda.home.HomeActivity
import com.maestros.dresspanda.home.ui.address.repository.allAddressModel.Data
import com.maestros.dresspanda.home.ui.cart.repository.CartX
import com.maestros.dresspanda.home.ui.orders.OrderViewModel
import com.maestros.dresspanda.payment.SSLCommerzLatest
import com.maestros.dresspanda.payment.SSlCommerz
import com.maestros.dresspanda.util.Constant
import com.softbd.aamarpay.PayByAamarPay
import com.softbd.aamarpay.model.OptionalFields
import com.softbd.aamarpay.model.RequiredFields
import com.softbd.aamarpay.utils.Params
import com.sslwireless.sslcommerzlibrary.model.initializer.SSLCommerzInitialization
import com.sslwireless.sslcommerzlibrary.model.response.SSLCTransactionInfoModel
import com.sslwireless.sslcommerzlibrary.model.util.SSLCCurrencyType
import com.sslwireless.sslcommerzlibrary.model.util.SSLCSdkType
import dagger.hilt.android.AndroidEntryPoint
import io.paperdb.Paper
import kotlinx.coroutines.launch

const val test = "56475b6bb0fbff7cbae33ba6cae8f2af"

@AndroidEntryPoint
class PaymentFragment : Fragment() {

    lateinit var binding: FragmentPaymentBinding
    var paymentType: String? = ""
    lateinit var cartX: CartX
    lateinit var map: CartX
    lateinit var address: Data

    var name: String? = ""

    var name2: String? = ""
    var email: String? = ""
    var mobile: String? = ""

    val orderViewModel: OrderViewModel by viewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        binding = FragmentPaymentBinding.inflate(layoutInflater)
        Paper.init(context)
        map = Paper.book().read<CartX>(Constant.CART_MAP)
        address = Paper.book().read<Data>(Constant.ADDRESS)


        binding.totalItems.setText("Total Items ${map.qntity}")
        binding.totalAmount.setText("Rs ${map.totalAmount}")



        lifecycleScope.launch {

            orderViewModel.ordersEvent.observe(viewLifecycleOwner, Observer {

                when (it) {


                    is OrderViewModel.OrdersEvent.AddOrder -> {

                        Constant.showSnackBar(
                            requireView(),
                            "Order successfull",
                            "Ok",
                            object : View.OnClickListener {
                                override fun onClick(v: View?) {
                                }

                            }).show()
                        //  navigateFirstTabWithClearStack(requireView())

                        Navigation.findNavController(requireView()).navigate(R.id.ordersFragment)
                        orderViewModel.addNotification(
                            it.resultText.userId,
                            resources.getString(R.string.order_placed)
                        )
                        Constant.setCartCount(HomeActivity.bottomNavigationView, 0.toString())

                    }

                    is OrderViewModel.OrdersEvent.Failure -> {

                        Constant.showSnackBar(
                            requireView(),
                            "Order failed, please try later",
                            "Ok",
                            object : View.OnClickListener {
                                override fun onClick(v: View?) {
                                }

                            }).show()
                    }
                }

            })
        }





        binding.amaryPaylayout.setOnClickListener {


            binding.rbssl.isChecked = true
            binding.rbcod.isChecked = false
            paymentType = "aamarpay"


        }



        binding.CODLinearLyt.setOnClickListener {


            binding.rbssl.isChecked = false
            binding.rbcod.isChecked = true
            paymentType = "COD"


        }
        binding.rbssl.setOnClickListener {


            binding.rbssl.isChecked = true
            binding.rbcod.isChecked = false
            paymentType = "aamarpay"


        }



        binding.rbcod.setOnClickListener {


            binding.rbssl.isChecked = false
            binding.rbcod.isChecked = true
            paymentType = "COD"


        }


        binding.continueButton.setOnClickListener {


            openAlertDialog()


        }



        return binding.root
    }

    private fun processCOD(amount: String) {

        orderViewModel.addOrder(
            userID = Constant.getUserID(context),
            paymentType = paymentType,
            addressId = arguments?.getString("id"),
            totalAmount = map.totalAmount.toDouble(),
            deliveryFee = 40


        )


    }


    fun navigateFirstTabWithClearStack(view: View) {
//        val navController = Navigation.findNavController(view)
//        val navHostFragment: NavHostFragment = fragmentManager?.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
//        val inflater = navHostFragment.navController.navInflater
//        val graph = inflater.inflate(R.navigation.mobile_navigation)
//        graph.startDestination = R.id.nav_home
//
//        navController.graph = graph


    }

//    private fun processSSLpayment(amount:String?) {
//
//        val mandatoryFieldModel = MandatoryFieldModel(
//            "dress60839e17edd14",
//            "dress60839e17edd14@ssl",
//            amount,
//            "TESTTTTT",
//            CurrencyType.BDT,
//            SdkType.TESTBOX,
//            SdkCategory.BANK_LIST
//        )
//
//        SSlCommerz.initializePayment(context,mandatoryFieldModel,object :SSLCommerz{
//            override fun succcess(transactionInfo: TransactionInfo?) {
//                orderViewModel.addOrder(
//                    userID = Constant.getUserID(context),
//                    paymentType = paymentType,
//                    addressId = arguments?.getString("id"),
//                    totalAmount = map.totalAmount.toDouble(),
//                    deliveryFee = 40
//
//
//                )
//
//
//
//            }
//
//            override fun errorState(error: String?) {
//
//
//                Toast.makeText(context,"Some error occured" + error,Toast.LENGTH_LONG).show()
//
//            }
//
//        })
//
//
//
//    }


    fun openAlertDialog() {


        val dialog = AlertDialog.Builder(requireContext()).setTitle("Comfirm Order")
            .setMessage("Are you want to proceed with the order")
            .setPositiveButton("Yes", object : DialogInterface.OnClickListener {
                override fun onClick(dialog: DialogInterface?, which: Int) {


                    when (paymentType) {


                        "aamarpay" -> {


                            if (!name!!.equals("") && !email!!.equals("") && !mobile.equals("")) {
                                ammarpay()

                            } else {
                                openDetailsDialog()

                            }
                        }


                        "COD" -> {
                            processCOD(map.totalAmount.toString())

                        }


                    }


                }

            }).setNegativeButton("No", object : DialogInterface.OnClickListener {
                override fun onClick(dialog: DialogInterface?, which: Int) {

                    dialog?.dismiss()
                }

            }).show()


    }


    fun processSSLpaymentLatest(amount: String?) {


        val sslCommerzInitialization = SSLCommerzInitialization(
            Constant.STORE_ID,
            Constant.STORE_PASSWORD,
            amount!!.toDouble(),
            SSLCCurrencyType.BDT, //Currency type
            "TEST",
            "dress",
            SSLCSdkType.TESTBOX //SDK type for live payment use SSLCSdkType.TESTBOX for test mode
        )


        SSlCommerz.processPayment(
            requireActivity() as AppCompatActivity,
            sslCommerzInitialization,
            object : SSLCommerzLatest {
                override fun succcess(transactionInfo: SSLCTransactionInfoModel?) {
                    orderViewModel.addOrder(
                        userID = Constant.getUserID(context),
                        paymentType = paymentType,
                        addressId = arguments?.getString("id"),
                        totalAmount = map.totalAmount.toDouble(),
                        deliveryFee = 0


                    )
                }

                override fun errorState(error: String?) {
                    Toast.makeText(context, "Some error occured" + error, Toast.LENGTH_LONG).show()
                }

            })


    }


    private fun ammarpay() {
        val requiredFields = RequiredFields(
            name,
            email,
            address.buldingNo + ", " + address.streetNo + ", " + address.landmark,
            address.city,
            address.state,
            address.pincode,
            "Bangladesh",
            mobile,
            "Pay for product",
            map.totalAmount.toString(),
            Params.CURRENCY_BDT,
            getTransactionID(),
            "dresspanda",
            test,
            "https://secure.aamarpay.com/success.php",
            "https://secure.aamarpay.com/failed.php",
            "https://secure.aamarpay.com/failed.php"
        )

//        val requiredFields = RequiredFields(
//            "Customer Name",
//            "customer_email@email.com",
//            "Address 1",
//            "City",
//            "State",
//            "1234",
//            "Country",
//            "0123456789",
//            "Description",
//            "1",
//            Params.CURRENCY_BDT,
//            "A205220",
//            "dresspanda",
//            test,
//            "https://sandbox.aamarpay.com/success.php",
//            "https://sandbox.aamarpay.com/failed.php",
//            "https://sandbox.aamarpay.com/failed.php"
//        )


        val optionalFields = OptionalFields(

        )





        PayByAamarPay.getInstance(requireContext(), requiredFields, optionalFields)
            .payNow { paymentStatus, paymentResponse ->


                if (paymentResponse.payStatus.equals("Successful")) {
                    orderViewModel.addOrder(
                        userID = Constant.getUserID(context),
                        paymentType = paymentType,
                        addressId = arguments?.getString("id"),
                        totalAmount = map.totalAmount.toDouble(),
                        deliveryFee = 40


                    )

                } else {

                    Toast.makeText(
                        requireContext(),
                        "Transaction failed! Please try again",
                        Toast.LENGTH_LONG
                    ).show()
                }


            }


    }


    fun getTransactionID(): String {

        return System.currentTimeMillis().toString() + "u" + Constant.getUserID(requireContext())

    }


    fun openDetailsDialog() {


        val binding = BottomCustomerDetailsBinding.inflate(layoutInflater)

        val bottomSheetDialog = BottomSheetDialog(requireActivity())

        bottomSheetDialog.setContentView(binding.root)
        bottomSheetDialog.show()



        binding.submitBtn.setOnClickListener {


            if (binding.nameEdit.text.toString().isNotEmpty() && binding.emailEdit.text.toString()
                    .isNotEmpty() && binding.phoneEdit.toString().isNotEmpty()
            ) {


                name = binding.nameEdit.text.toString()
                email = binding.emailEdit.text.toString()
                mobile = binding.phoneEdit.text.toString()




                bottomSheetDialog.dismiss()
            } else {


                Toast.makeText(
                    requireContext(),
                    "Please enter and input all the fields",
                    Toast.LENGTH_LONG
                ).show()


            }


        }


    }
}